VCS=hg #svn
LANG=ja_JP.UTF-8
USER=$(shell whoami)
HOSTNAME_=$(shell hostname)
TODAY=$(shell date)

all:stat

stat:
	LANG=$(LANG) $(VCS) stat
ci: commit
commit:
	LANG=$(LANG) $(VCS) $@ -u $(USER) \
	-m "ci from Makefile by $(USER)@$(HOSTNAME_)"
push: commit
	LANG=$(LANG) $(VCS) $@
diff:
	LANG=$(LANG) $(VCS) $@
#	
clean:
	-rm *~
#	LANG=$(LANG) $(VCS) stat | sed -n 's/^\?\s*//'

