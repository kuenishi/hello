#pragma once

#include "lists_base.hpp"
#include "arrays.hpp"

#include <iostream>
using namespace std;

namespace brake {
  namespace lists {

    static const size_t max_array_size = 32;

    template <class T>
    class ArrayListIterator {
    public:
      bool end() const {
        return i.end();
      }
      T& get() {
        return i.get().get(offset);
      }
      void operator++() {
        if( this->end() ) return;
        if( offset == i.get().size() ){
          offset = 0;
          i++;
        }else{
          offset++;
        }
      };
     
    public:
      LinkedListIterator<arrays::Arrays<T> > i;
      size_t offset;
    };
    
    template <class T>
    class ArrayList : public List<T, ArrayListIterator<T> > {
    public:
      ArrayList(){}
      virtual ~ArrayList(){
        //        cout << __LINE__ << size_ << endl;
      }
      void add(const T& t){
        if(lists_.size() == 0){
          arrays::Arrays<T> a(max_array_size);
          a.set(0, t);
          //cout << __LINE__ << t << endl;
          lists_.add(a); // buggy here
        }
        //cout << __LINE__ << t << endl;
      }
      const T& get(unsigned int n)const{}
      void del(unsigned int n){}
      size_t size() const { return size_; }

      Iterator<T> begin() {
        ArrayListIterator<T> iterator;
        iterator.i = lists_.begin();
        iterator.offset = 0;
        return iterator;
      }
    private:
      size_t size_;
      LinkedList<arrays::Arrays<T> > lists_;
    };
  }
}
