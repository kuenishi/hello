#pragma once

#include "base.hpp"
#include <stdlib.h>

#include <iostream>

namespace brake {

  namespace arrays {
    template <class T>
    class Arrays {
    public:
      Arrays(const Arrays<T>& a):
        size_(a.size()),
        data_((T**)malloc(sizeof(T*)*a.size()))
      {
        for(size_t s=0;s<size_;++s){
          const T* p = a.get_ptr(s);
          if(p != NULL){
            data_[s] = new T(*p);
          }else{
            data_[s] = NULL;
          }
        }
      }

      Arrays(size_t s):
        size_(s),
        data_((T**)malloc(sizeof(T*)*size_))
      {
        for(size_t s=0;s<size_;++s){
          data_[s] = NULL;
        }
      };
      virtual ~Arrays(){
        for(size_t s=0;s<size_;++s){
          if(data_[s] != NULL){
            delete data_[s];
          }
        }
      };
      void set(size_t offset, const T& t){
        set_ptr(offset, new T(t));
      }
      const T& get(size_t offset)const{
        const T* p = get_ptr(offset);
        return *p;
      }
      void del(size_t offset){
        set_ptr(offset, NULL);
      }
      void swap(size_t lhs, size_t rhs) const {
        if(lhs >= size_ || rhs >= size_)
          throw out_of_bound_exception();
        if(lhs == rhs)
          return;
        const T* tmp = data_[lhs];
        data_[lhs] = data_[rhs];
        data_[rhs] = tmp;
      }
      size_t size() const {
        return size_;
      }
    private:
      const T* get_ptr(size_t offset)const{
        if(offset >= size_)
          throw out_of_bound_exception();
        return data_[offset];
      }
      void set_ptr(size_t offset, T* p){
        if(offset >= size_)
          throw out_of_bound_exception();
        if(data_[offset] != NULL)
          delete data_[offset];
        data_[offset] = p;
      }
   
      const size_t size_;
      T** data_;
      Arrays(){};
    };

  }
}
