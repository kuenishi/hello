#pragma once

#include <exception>

namespace brake{
  class out_of_bound_exception : public std::exception {};
}
