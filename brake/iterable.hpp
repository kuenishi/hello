#pragma once

namespace brake {

  template <class T>
  class Iterator {
  public:
    virtual bool end() const = 0;
    virtual T& get() = 0;
    virtual void operator++() = 0;
  private:
    // want to forbid
    //T& operator*() = 0;
  };

  template <class T, template <class> class IteratorT >
  class Iterable {
  public:
    virtual IteratorT<T> begin() = 0;
  private:
  };
}
