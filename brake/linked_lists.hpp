#pragma once

#include <stddef.h>
#include "lists_base.hpp"

#ifdef DEBUG
#include <iostream>
#endif

namespace brake{
  namespace lists{

    template <class T>
    struct box_ {
      struct box_<T>* next;
      T data;
      box_(const T& t):data(t){}
      virtual ~box_(){}
    };

    template <class T>
    struct LinkedListIterator : public Iterator<T> {
      bool end() const {
        return (curr == NULL);
      };
      T& get() {
        if( not this->end() )
          return curr->data;
        else
          throw out_of_bound_exception();
      };
      void operator++() {
        if(not this->end()){
          curr = curr->next;
        }
      };
      struct box_<T> *curr;
    };
    
    template <class T>
    class LinkedList : public List<T, LinkedListIterator<T> > {
    public:
      LinkedList():head_(NULL), size_(0){}
      LinkedList(const LinkedList<T>& l):head_(NULL), size_(0){
        // slow
        for(size_t s=0; s<l.size(); ++s){
          this->add(l.get(s));
        }
      }
      virtual ~LinkedList(){
        while(this->size_ > 0){
          this->del(0);
        }
      }
      
      void add(const T& t){
        box_<T>* new_head = new box_<T>(t);
        new_head->next = head_;
        head_ = new_head;
        size_++;
      }
      
      void del(unsigned int n){
        if(n >= size_){
          throw out_of_bound_exception();
        }
        // size should be more than 1 here
        //        std::cout << __FILE__ << " " << __LINE__ << "<head: " << head_ << "\tsize: "<< size_ << "\tNext: "<< head_->next << std::endl;
        box_<T> **curr = &head_;
        box_<T> *ptr = head_;
        
        //        std::cout << __FILE__ << " " << __LINE__ << "<<<" << head_ << std::endl;
        
        for(size_t i=0; i<n; ++i){
          //std::cout << "p>" << *prev << std::endl;
          //std::cout << "data>" << *((*prev)->data) << std::endl;
          //std::cout << (*prev) -> next << std::endl;
          curr = &((*curr)->next);
          ptr = ptr->next;
        }
        //        std::cout << __FILE__ << " " << __LINE__ << "<<<" << std::endl;
        
        //        std::cout << " deleted: " << *(ptr->data) << std::endl;
        //std::cout << (*curr)->next->next << std::endl;
        *curr = ptr->next;
        size_--;
        delete ptr;
      }
      const T& get(unsigned int n)const{
        if(n >= size_){
          throw out_of_bound_exception();
        }
        const box_<T>* ptr = head_;
        for(size_t i=0; i<n; ++i){
          ptr = head_->next;
        }
        return ptr->data;
      }
      size_t size() const {
        return size_;
      }

      LinkedListIterator<T> begin() {
        LinkedListIterator<T> i;
        i.curr = head_;
        return i;
      }

#ifdef DEBUG
      void print() const {
        struct box_<T>* curr = head_;
        while(curr != NULL){
          std::cout << "curr->next: " << curr->next << "\t";
          std::cout << "curr->data: " << *(curr->data) << std::endl;
          curr = curr->next;
        }
      }
#endif
    private:
        struct box_<T>* head_;
        size_t size_;
    };
  }
}
