#pragma once

#include "base.hpp"
#include "iterable.hpp"

namespace brake{
  namespace lists{

    template <class T, template <class> class IteratorT>
    class List : public Iterable<T, IteratorT<T> >{
    public:
      virtual void add(const T&) = 0;
      virtual void del(unsigned int) = 0;
      virtual const T& get(unsigned int) const = 0;
      virtual size_t size() const = 0;

      // from Iterable<T>
      virtual IteratorT<T> begin() = 0;
    };
  }
}
