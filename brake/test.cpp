#include "brake.hpp"
#include "lists.hpp"
#include <iostream>

#include <sys/time.h>
#include <string>

using namespace std;

#define ITERNUM 65536

class micro_bench {
public:
  micro_bench(const char* s):title_(s){
    gettimeofday(&start_, NULL);
  }
  ~micro_bench(){
    struct timeval e;
    gettimeofday(&e, NULL);
    double duration = (e.tv_sec - start_.tv_sec);
    duration += (e.tv_usec - start_.tv_usec) / 1000000.0;
    std::cout << title_ << " ==\tduration: " << duration << " sec" << std::endl;
  }
private:
  struct timeval start_;
  std::string title_;
};

// run test here
int main(){

  cout << "brake version: " << brake::version() << endl;
  {
    micro_bench b("LinkedList");
    brake::lists::LinkedList<unsigned int> l;
    for(size_t s=0; s<ITERNUM; ++s) {
      l.add(s);
    }
    l.get(ITERNUM / 2);
    l.del(ITERNUM / 2);
  }

  {
    micro_bench b("ArrayList");
    brake::lists::ArrayList<unsigned int> l;
    for(size_t s=0; s<ITERNUM; ++s) {
      l.add(s);
    }
    cout << __LINE__ << l.size() << endl;
    l.get(ITERNUM / 2);
    cout << __LINE__ << l.size() << endl;
    l.del(ITERNUM / 2);
  }
}
