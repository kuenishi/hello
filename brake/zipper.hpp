#pragma once

#include <queue>

namespace zipper {

  template <typename T>
  class list {
  public:
    list(){};
    ~list(){};

    void next(){
      T& t = right_.top();
      left_.push(t);
      right_.pop();
    };
    void prev(){
      T& t = left_.top();
      right_.push(t);
      left_.pop();
    };
    
    const T& get()const{ return right_.top();  };
    void insert(const T& t){    right_.push(t);};
    void remove(){              right_.pop();  };

  private:
    std::queue<T> left_;
    std::queue<T> right_;
  };
  
  template <typename T>
  class tree {
  public:
    tree() : root_(NULL) {}
    ~tree(){
      if(root_) delete root_;
    };

    void next(){ if(root_) root_ = root_->next(); }
    void prev(){ if(root_) root_->prev(); }

    bool find(const T& t){
      if(root_) return root_->find(t, &root_);
      else return false;
    }
    void insert(const T& t){
      if(root_) root_->insert(t);
      else root_ = new node_(t);
    }
    void remove(const T& t){
      if(root_){ root_->remove(t); }
      else{
	delete root_;
	root_ = NULL;
      }
    }
    const T& get()const{ if(root_) return root_->get(); }

  private:

    struct node_ {
      T t_;
      node_ * left_;
      node_ * right_;

      node_(const T& t): t_(t), left_(NULL), right_(NULL) {};
      ~node_(){
	if(right_)delete right_;
	if(left_) delete left_;
      }
      void next(){
	if(right_){
	  node_ * tmp = right_;
	  node_ * hanger = right_->left_;
	  tmp->left_ = this;
	  tmp->left_->right_ = hanger;
	  return tmp;
	}else{
	  return this;
	}
      }
      void prev(){
	if(left_){
	  node_ * tmp = left_;
	  node_ * hanger = left_->right_;
	  tmp->right_ = this;
	  tmp->left_->right_ = hanger;
	  return tmp;
	}else{
	  return this;
	}
      }
      bool find(const T& t, node_ ** me){
	if(t == t_){ return true; }
	else if(t < t_){
	  if(left_){
	    if(left_->t_ < t) left_->find(t, &left_);
	    else right_->find(t, &right_);
	  }
	}else if(t_ < t){
	  *me = next();
	}
	  //else assert
	return find(t, me); // this falls infinite loop
      }
      void insert(const T& t){
      }
      void remove(const T& t){}
      const T& get()const{
	return t_;
      }
      // void insert(const T& t){
      // 	if(t == t_){ return; } //already exists
      // 	else if(t < t_ ){
      // 	  if(left_) left_->insert(t);
      // 	  else      left_ = new node_(t);
      // 	}
      // 	else if(t_ < t ){
      // 	  if(right_) right_->insert(t);
      // 	  else       right_ = new node_(t);
      // 	}
      // }
      // void merge(node* n){ // n is guaranteed : n < this or this < n
      // 	if(n){
      // 	  if( n->t_ < t_ ){ // merge to left
      // 	    T top = left_->rotate_left(&left_);
      // 	    node_* tmp = new node_(top);
      // 	    tmp->left_ = n;
      // 	    tmp->right_= left_;
      // 	  }
      // 	  else if( t_ < n->t_ ){
      // 	    T top = right_->rotate_right(&right_);
      // 	    node_* tmp = new node_(top);
      // 	    tmp->left_ = right_;
      // 	    tmp->right_= n;
      // 	  }
      // 	  else{ // t_ == n->t_
      // 	    //assert
      // 	  }
      // 	}
      // 	return;
      // }
      // bool divide(node_** left, const T& pivot, node_** right){
      // 	if(t_ == pivot){
      // 	  *left = left_;
      // 	  *right = right_;
      // 	  return true;
      // 	}
      // 	else if(t_ < pivot){ //divide right tree
      // 	  bool ret = false;
      // 	  node_ * left_tmp, * right_tmp;
      // 	  if(right_){
      // 	    ret = right_->divide(&left_tmp, pivot, &right_tmp);

      // 	    if(left_tmp){
      // 	      if(left_){
      // 		left_->merge(left_tmp);  // merge right_tmp with *right
      // 	      }else{
      // 		left_ = left_tmp;
      // 	      }
      // 	    }
      // 	  }
      // 	  *left = left_;
      // 	  *right = right_;
      // 	  return ret;

      // 	}else if(pivot < t_){ // divide left tree
      // 	  node_ * left_tmp, * right_tmp;
      // 	  bool ret = false;
      // 	  if(left_){
      // 	    ret = left_->divide(&left_tmp, pivot, &right_tmp);
	    
      // 	    if(right_tmp){
      // 	      if(right_){
      // 		right_->merge(right_tmp);
      // 	      }else{
      // 		right_ = right_tmp;
      // 	      }
      // 	    }
      // 	  }
      // 	  *left = left_;
      // 	  *right= right_;
      // 	  return ret;

      // 	}else{
      // 	  throw __LINE__;
      // 	  // assert
      // 	}
      // }
      // const T& rotate_right(node_** n){ // [ 1, 2, 3] -> 1, [2, 3]
      // 	if(left_){
      // 	  return left_->rotate_right(&left_);
      // 	}else{
      // 	  node_ * tmp = *n;
      // 	  *n = right_;
      // 	  T t = t_;
      // 	  delete tmp;
      // 	  return t;
      // 	}
      // }
      // const T& rotate_left(node_ ** n){ // [1, 2, 3] -> [1, 2], 3
      // 	if(right_){
      // 	  return right_->rotate_left(&right_);
      // 	}else{
      // 	  node_ * tmp = *n;
      // 	  *n = left_;
      // 	  T t = t_;
      // 	  delete tmp;
      // 	  return t;
      // 	}
      // }
    };

    node_* root_;
  };
}
