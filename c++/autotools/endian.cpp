#include <iostream>
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

using namespace std;

int main(){

  cout << "checking endian." << endl;

#ifdef BIG_ENDIAN
  cout << "big endian" << endl;
#endif
#ifdef LITTLE_ENDIAN
  cout << "little endian" << endl;
#endif

#ifdef HOGEHOGEHOGE
  cout << "hogehogehoge" << endl;
#endif

#ifdef WORDS_BIGENDIAN
  cout << "words bigendian" << endl;
#endif

}
