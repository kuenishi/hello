#include <iostream>
using namespace std;

#include <Options.h>
#include <config.h>

int main(int args, char* argv[]){
  Options o(args, argv);
  cout << args << " options." << endl;
  for( int i = 0; i < args; ++i){
    cout << i << "\t" << argv[i] << endl;
  }
  cout << "--------------------------hello options!" << endl;
  o.print();
  cout << o.get("a") << endl;
}
