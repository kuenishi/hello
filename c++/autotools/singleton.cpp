#include <iostream>

using namespace std;

//マルチスレッド非対応：ダブルチェック（一度チェックして、なければMutexをとる）とか
//Phoenix Singletonとか←ほかのSingletonと依存しあったりするかもしれないとき
class singleton{
public:
  static singleton& getInstance();

  ///member functions
  void add(int);
  void add(string);
  void print();

//  friend void destruct(){ delete instance_; };
private:
  static singleton* instance_;

 // constructors must be private
  singleton(const singleton&){};
  singleton():i(0), s("a"){ };
  ~singleton(){ cout << "singleton dtor called." << endl; };

  ///member variables
  int i;
  string s;
};

singleton& singleton::getInstance(){
  if( not instance_ )
    instance_ = new singleton;
  return *instance_;
}

void singleton::add(int a){
  i += a;
}
void singleton::add(string a){
  s += a;
}
void singleton::print(){
  cout << "singleton: i=" << i << ", s=" << s << endl; 
}

void hoge(){
  cout << "function " << __func__ << "() called." << endl;
  singleton::getInstance().add(34);
}
void aho(){
  singleton::getInstance().print();
}

singleton* singleton::instance_ = 0;

int main(){
  atexit(aho);
  atexit(hoge);
 // atexit(destruct);
  cout << "hello singleton." << endl;
  singleton::getInstance().add(56);
  singleton::getInstance().print();
  singleton::getInstance().add(4);
  singleton::getInstance().add(" hogehoge ");
  singleton::getInstance().print();
}

