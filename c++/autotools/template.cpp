
#include <iostream>
#include <vector>

using namespace std;

template <class C = std::vector<int> >
class Typing{
public:
  static const char* getName(){
    return typeid( C ).name();
  };

};


int main(){
  cout << typeid( typeid(vector<double>).name() ).name() << endl;
  cout << typeid(vector<float>).name() << endl;
  cout << typeid(vector<int>).name() << endl;
  cout << typeid(vector<unsigned int>).name() << endl;
  cout << typeid(vector<string>).name() << endl;
  cout << typeid(vector<vector<double> >).name() << endl;

  cout << Typing<>::getName() << " ?= " << Typing<int>::getName() << endl;
}
