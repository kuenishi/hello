
#include <iostream>

/// $Id: virtual.cpp 48 2007-01-27 18:15:26Z kuenishi $

using namespace std;

class vtest{
public:
  vtest(){};
  virtual ~vtest();

  virtual void test() =0;

};

void vtest::test(){
  cout << "I'd be rather virtual." << endl;
} 
vtest::~vtest(){}

class hoge: public vtest{
public:

  hoge(){};
  ~hoge(){};
  void aaaa(){};
  void test(){ 
	  vtest::test();
	  cout << "I'd not be rather virtual." <<endl; 
  };
};

int main(){
  hoge sample;// = new up();
  sample.test();
}
