#include <stdio.h>
#include <string>
#include <stdlib.h>
#include <iostream>

#ifndef _XOPEN_SOURCE
//#define _XOPEN_SOURCE
#endif
#include <unistd.h>

using namespace std;
static const string SALT_BASE("$6$aaaaaa$");

extern "C"{
string get(const string& key){
  struct crypt_data d;
  d.initialized = 0;
  char * h = crypt(key.c_str(), SALT_BASE.c_str(), &d);
  string hash(h+SALT_BASE.size());
  printf("%s\n", hash.c_str());
  free(h);
  return hash;
}
}

int main(){
  string key("hogehoge");
  cout << get(key) << endl;
  return 0;
}
