#include <dlloader.h>
#include <dlfcn.h>
#include <unistd.h>
#include <stdexcept>

using namespace my;

dlloader::dlloader(const string& libfile, const string& func)
{
  handle_ = dlopen( libfile.c_str(), RTLD_LAZY);
  dlerror();
  if( not handle_ ){
    Log::ger::error( dlerror() );
    throw std::exception();
  }
  set_func();
}
void dlloader::set_func(const string& func){
  func_ = (int (*)(char*))dlsym( handle_, func.c_str() );
}

dlloader::~dlloader(){
  dlclose( handle_ );
  // delete handle_;
}

int dlloader::call(char* s){
  return func_( s );
}
int dlloader::call(const string& s){
  return this->call( const_cast<char*>(s.c_str()) );
}
int dlloader::init_module( char* s){
  int (*func)(char * );
  func = (int (*)(char*))dlsym( handle_, "init" );
  return func( s );
}
int dlloader::fini_module(){
  int (*func)();
  func = (int (*)())dlsym( handle_, "fini" );
  return func();
}
