#ifndef DLLOADER_H
#define DLLOADER_H

#include <string>
#include <logger.h>

using std::string;

namespace my{
  static const string DEFAULT_FUNC = "entrypoint";

  class dlloader{
  public:
    dlloader(const string&, const string& = DEFAULT_FUNC);
    ~dlloader();
    int call(char* );
    int call(const string& );
    int init_module( char* = NULL);
    int fini_module();
    void set_func(const string& = DEFAULT_FUNC);
  private:
    void * handle_;
    int (*func_)(char*);
    //const string default_func_;
  };


}

#endif

