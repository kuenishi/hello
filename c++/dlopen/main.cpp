#include <iostream>
#include <dlfcn.h>
#include <unistd.h>
#include <string>

#define MAXPATHLEN 1024

using namespace std;
int main(int args, char **argv){
   cout << "Hello, Batch" << endl;
   char *cwd = (char *)malloc(sizeof(char)*MAXPATHLEN);
   getcwd( cwd, MAXPATHLEN );
   string pwd( cwd );
   pwd += "/libdyntest.so";

   void *handle = dlopen(pwd.c_str(), RTLD_LAZY);
   fputs (dlerror(), stderr);
   cout << endl;
   cout << "path: " << pwd << endl;
   if( not handle ){
     cerr << "handle is NULL" << endl;
     exit(0);
   }else
     cerr << "handle is not NULL." << endl;
   double (*func)(double);
   func = (double (*)(double))dlsym(handle, "get_i");
   printf ("%f\n", (*func));
   dlclose(handle);
}
