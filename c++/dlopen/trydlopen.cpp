#include <iostream>
#include <dlfcn.h>
#include <unistd.h>
#include <string>

#define MAXPATHLEN 1024

using namespace std;
int main(int args, char **argv){
  cout << "Hello, Batch" << endl;
  char *cwd = (char *)malloc(sizeof(char)*MAXPATHLEN);
  getcwd( cwd, MAXPATHLEN );
  string pwd( cwd );
  pwd += "/libdyntest.so";

  void *handle = dlopen(pwd.c_str(), RTLD_LAZY);
  cout << endl;
  cout << "path: " << pwd << endl;
  if( not handle ){
    fputs (dlerror(), stderr);
    cerr << "handle is NULL" << endl;
    exit(0);
  }else
    cerr << "handle is not NULL." << endl;

  int (*func)();
  func = (int (*)())dlsym(handle, "get_i");//"__Z5get_ii");
  cout << "hoge" << endl; //program reaches here
  int i = func(); 
  cout << "got:" <<  i  << endl; //program doesn't reach here

  dlclose(handle);
}
