#include <iostream>
#include <stdlib.h>

using namespace std;

class hoge{
public:
  hoge(){
    cout << __func__ << endl;
  };
  ~hoge(){
    cout << __func__ << endl;
  };
};

int main(){
  hoge h;
  exit(0);
}

