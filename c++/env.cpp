#include <iostream>
#include <string>
#include <cstdlib>
using namespace std;

int main(int args, char** argv){
  const char * env = getenv("PATH");
  cout << getenv("PATH") << endl;
  std::string new_env(env);
  new_env += ":";
  char cwd[1024];
  getcwd(cwd, 1024);

  string p(argv[0]);
  string q = p.substr(0, p.find_last_of('/'));
  cout << q << endl;

  cout << "cwd:" << cwd << endl;

  new_env += cwd;
  new_env += "/";
  new_env += q;

  setenv("PATH", new_env.c_str(), new_env.size());
  
  cout << getenv("PATH") << endl;
}
