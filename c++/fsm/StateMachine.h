#ifndef STATEMACHINE_H
#define STATEMACHINE_H

#include <iostream>
using namespace std;

class BaseMessage{};

class BaseState{
public:
  virtual void status () const = 0;
  //protected:
  virtual BaseState *  press() = 0;
  template <class MessageType>
  BaseState * accept(MessageType * msg){
    //this->accept(msg);
  };
};

 class StateMachine {
 public:
   StateMachine(BaseState * initial): state_(initial){};
   ~StateMachine(){ delete state_; };

   template <class MessageType>
   void recv(MessageType * msg ){
     BaseState * next_state = state_->accept(msg);
     delete state_ ;
     delete msg;
     state_ = next_state;
   };
   
 private:
   StateMachine(){};
   BaseState * state_;
 };
  
#endif

