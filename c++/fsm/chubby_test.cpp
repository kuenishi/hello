//#include "StateMachine.h"
#include <iostream>
using namespace std;

// クライアントAPI上で管理される状態機械
// session states:
//   NoSession, Active, Jeopardy
// events:
//   create_session, close_session, connection_broke,
// callbacks:
//   active(), closed(), jeopardy(), safe(), expired(),
// actions:
//   rebuild_session(rather action than event...)
//
class Event {
public:
  Event(){};
  virtual ~Event(){};
};

class BaseState{
public:
//   virtual void create_session() = 0;
//   virtual void close_session()  = 0;
//   virtual void TCP_conn_broke() = 0;
//   virtual void rebuild_session()= 0;
  virtual void process_event(const Event&) = 0;
  
  BaseState(){};
  virtual ~BaseState(){};
};

class StateManager{
public:
  void process_event(Event* pe){
    state_->process_event( *pe );
    delete pe;
  };
  StateManager(BaseState* initial):state_(initial){};
  ~StateManager(){};
private:
  BaseState * state_ ;
  StateManager(){};
};

class NoSession : public BaseState{
public:
  virtual void process_event(const  Event& e ){};
  NoSession(){};
  virtual ~NoSession(){};
  
};

class Active : public BaseState{
public:
  Active(){};
  virtual ~Active(){};
};

class Jeopardy: public BaseState{
 public:
  Jeopardy(){};
  virtual ~Jeopardy(){};
};

int main(int args, char ** argv){
  StateManager sm( new NoSession );
  //cout << "hello" << endl;
}
