// #include "BaseState.h"
// #include "BaseMessage.h"
#include "StateMachine.h"

class Press : public BaseMessage{};



class PowerOn : public BaseState{
public:
  PowerOn(){};
  ~PowerOn(){};

  void status () const {
    cout << typeid(this).name() << "#" << __func__ << endl;
  };
  BaseState * accept(Press * msg );
  BaseState * press();
};

class PowerOff : protected BaseState{
public:
  PowerOff(){};
  ~PowerOff(){};

  void status () const {
    cout << typeid(this).name() << "#" << __func__ << endl;
  };
  BaseState * accept(Press * msg );
  BaseState * press();
};

BaseState * PowerOn::accept(Press * msg){
  cout << "hello!" << endl;
  return new PowerOff;
}
BaseState * PowerOn::press(){//Press * msg){
  cout << "hello!" << endl;
  return new PowerOff;
}
BaseState * PowerOff::accept(Press * msg){
  cout << "bye." << endl;
  return new PowerOn;
};
BaseState * PowerOff::press(){//Press * msg){
  cout << "bye." << endl;
  return new PowerOn;
};



int main(int args, char ** argv){
  
  //cout << "hello" << endl;
  PowerOn pon;
  BaseState * p = pon.press();
  p=p->press();
  p=p->press();
  p=p->press();
  p=p->press();
  //pon.status();
//   BaseState * p = pon.press( new Press() );
//   p->press( new Press );
//   poff.press( new Press() );
  StateMachine m(p);
  m.recv(new Press);
}
