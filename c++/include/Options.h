#ifndef OPTIONS_H
#define OPTIONS_H


#include <string>
#include <map>
#include <queue>

using std::string;
using std::map;

#include <stdexcept>
using std::runtime_error;

/**
 * class optparse : Command line Option parser
 *
 * $B%*%W%7%g%s(B -hoge, -h, --hoge, --a $B$J$I$r<u$1<h$k$3$H$,$G$-$k!#(B
 * -hoge$B$J$I%3%s%F%/%9%H$KB3$/0z?t$,$"$l$P!"$=$l$b5[<}$9$k$3$H$,$G$-$k$,!"(B
 * -hoge$B$J$I$KB3$+$:!"(B-$B$b$D$+$J$$0z?t$O(Balones$B$K$H$i$l$k!#(B
 * getAlone$B$O!"%3%s%F%/%9%H$J$7(B-$B$J$70z?t$N$&$A:G=i$K8=$l$?$b$N$@$1$rJV$9!#(B 
 *
 * @author : Kota Uenishi <kuenishi@gmail.com>
 * @usage  : just call Options::get("option") and Options::getLatest() after initialization!
 *
 */
class Options{
 public:
  typedef map<string, string> mymap_;  
  static const unsigned int MAX_ARGUMENTS_ = 64;

  Options(int argc, char* argv[]){
    if( argc < 1 or MAX_ARGUMENTS_ < argc ) return;
    else parse( argc, argv);
  };

  ~Options(){};

  bool has(string a){
    tmpp_ = args_.find( a );
    return ( tmpp_ not_eq args_.end() );
  }
  
  ///has option a or b
  bool has(string a, string b){ return (has( a ) or has( b )); }
  
  ///give a cast from string to such known types as int, double.(and char[]?)
  template <typename T>
  T get( const string& t ){
    switch(typeid(T) ){
      //    case typeid(string):
      //      return get_(t);
      //    case typeid(int):
    default:
      string msg = "no conversion from string to ";
      //      msg += typeid(T).name + " found.";
      throw runtime_error(msg);
    }
  }

  string get_(const string& a){
    tmpp_ = args_.find( a );
    return tmpp_->second;
  }
  string getLatest(){ return tmpp_->second; };

  bool empty(){
    return args_.empty();
  }

   void print(){  //debug code: print all args_ and orphants_
     if( empty() ){
       cout << "no options set." << endl;
       return;
     }
     for(tmpp_ = args_.begin();
 	tmpp_ not_eq orphants_.end();  ){
       cout << tmpp_->first << "\t" << tmpp_->second << endl;
       if( ++tmpp_ == args_.end() ) tmpp_ = orphants_.begin();
     }     //     exit(2);
  };

  string getOrphant(){
    return (orphants_.empty() )? "" : orphants_.begin()->first;
  }
 private:
  Options(){};
  Options( const Options& op ){};
  const Options& operator=( const Options& op ){ return *this; };

  mymap_ args_;
  mymap_::iterator tmpp_;
  mymap_ orphants_;

  ///return whether str begins with begin
  bool beginWith(string str, string begin){
    if( str.length() <= begin.length() )return false;
    return ( str.substr( 0, begin.length()) == begin );
  }

  void orphantFound(string orphant){ 
    orphants_.insert( pair<string, string>(orphant, "_orphant_") );
  }

  ///the only one long routine in this class
  void parse( int argc, char* argv[]){
    using std::queue;
    queue<string> qargv;
    for( int i = 1; i < argc; i++){
      qargv.push( string(argv[i]) );
    }

    while( not qargv.empty() ){
      string first = qargv.front();    
      qargv.pop();

      //case for "hoge",  ignored
      if( not beginWith(first, "-") ){
	orphantFound( first );
	continue;
      }

      //case for "--hoge" "--h"
      if( beginWith(first, "--") ){
	first = first.substr(2);

	//case for "---hoge", "----..."
	if( beginWith(first, "-") )continue;//more than 3 '-' is ignored      

      }else  //case for "-hoge" "-h"
	first = first.substr(1);
      
      if( qargv.empty() ){
	args_.insert( pair<string, string>(first, "") );
	break;
      }

      ///option arguments must not begin with "-"
      string second = qargv.front();
      if( beginWith(second, "-") ) second = "";
      else qargv.pop();
	
      args_.insert( pair<string, string>(first, second) );
    }//end while

  }
};


#endif
