#ifndef MESSAGE_H
#define MESSAGE_H

/**
  * $Id: message.h 29 2006-11-07 03:20:17Z kuenishi $
  *
  */



#include <iostream>
#include <string>
using std::string;

/** class message
  * $Id: message.h 29 2006-11-07 03:20:17Z kuenishi $
  * 汎用メッセージの実装を目指す：どんな型を入れても出力するoperator<<のようなものを。
  */

class message{
public:
  message();
  ~message();

  template <typename T>
  static void print(const T&);

  template <typename T>
  string operator+(T&);

  /*template <typename T>
    friend
    std::ostream& operator<<(const T& t){
    return ( std::cout << t );
    }*/

  template <typename T>
    friend std::ostream& 
    operator<<(message& lhs,  T& rhs){
    return ( std::cout << rhs );
  }  
};

message::message(){
   std::cout << typeid(this).name() << ":  hello template! " << std::endl;
}

message::~message(){}

template <typename T>
void message::print(const T& t){
  std::cout << typeid(T).name() <<  ": " << t << std::endl;
}

template <typename T>
string message::operator+(T& t){
  return string( typeid(T).name() );
}

//template <typename T>
//std::ostream& message::operator<<(const T& t){
//  return (std::cout << t);
//}


#endif
