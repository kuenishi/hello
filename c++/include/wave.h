#ifndef WAVE_H
#define WAVE_H

#include <string>
#include <fstream>
#include <exception>
#include <vector>

using std::string;
using std::ifstream;
using std::exception;
using std::vector;

// class mexception: public exception{
//  private:
//   string message;
//  public:
//   mexception(string s):message(s){
//   }
//   ~mexception(){}
//   string what(){
//     return message;
//   }

// };


/**
 * wave 
 * http://billposer.org/Software/waveutils.html
 *
 *$Id: wave.h 20 2006-10-24 09:02:48Z kuenishi $
 */
class wave{
 public:
  // wave();
  wave(string);

  ///waveが開放する必要はない
  wave(char*);
  ~wave();

  vector<short> read(unsigned int) throw(exception&);
 
 private:
  string filename;

  ifstream fin;
  bool good();
  bool bad();
  bool open_header() throw(exception&);

  bool verbose;
  //  void (p_escape_endian)();
  char riff[4];          // 'RIFF'
  unsigned int fileSize; // ファイルサイズ − ８
  char wavefmt[8];       // 'WAVEfmt '
  unsigned int headerSize; // ヘッダのサイズ
  unsigned short category; // ＰＣＭの種類
  unsigned short channel; // PCMチャネル数
  unsigned int rate; // サンプリングレート
  unsigned int bps; // byte per sec
  unsigned short bpb; // byte per block
  unsigned short bit; // サンプリングビット数
  char data[4]; //String_n<4> data; // 'data'
  unsigned int dataSize; // データのサイズ

  void escape_endian( unsigned short* );
  void escape_endian( short* );
  void escape_endian( unsigned int* );
};


#endif
