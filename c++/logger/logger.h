#ifndef LOGGER_H
#define LOGGER_H

#include <pthread.h>
#include <time.h>
#include <string>
using std::string;
#include <queue>
using std::queue;

//copyright kuenishi 2008
//
// $Id: logger.h 46 2008-04-13 12:06:36Z kuenishi $
//
// logger.h - simple multi-level logging module 
//            with single header file
// usage - Log::error("message here.");
//         Log::warning, Log::debug, Log::info
//         Log::set(warning, stdout) print worse than warning to stdout 
//         ** all of there functions are thread-safe.
//
// TODO: - format: [INFO: <time>]: message
//         use queue and thread? use lock?

//see http://www.alles.or.jp/~torutk/oojava/maneuver/2001/logging/logging.html
namespace Log{
  class lqueue{
  public:
    void push(const string& l){
      pthread_mutex_lock(&mutex_);
      q_.push(l);
      pthread_mutex_unlock(&mutex_);
    };
    const string front(const int & i = 1){
      pthread_mutex_lock(&mutex_);
      string ret = q_.front();
      q_.pop();
      pthread_mutex_unlock(&mutex_);
      return ret;
    };
    lqueue(){
      pthread_mutex_init(&mutex_, NULL);
    };
    ~lqueue(){};
  private:
    queue<string> q_;
    pthread_mutex_t mutex_;
  };
  
  void  now(char* buffer, int buffer_size){
    time_t rawtime;
    struct tm * timeinfo;
    //char buffer [80];
    time( &rawtime );
    timeinfo = localtime ( &rawtime );
    strftime (buffer, buffer_size,"%Y %m%d %H:%M:%S",timeinfo);
  }

  typedef int level;
  //errorとかfatalはプログラムを継続できないのでログには…
  static const level FATAL   = -20;
  static const level ERROR   = -10;
  static const level SEVERE  =  0; 
  static const level WARNING =  10; 
  static const level INFO    =  20; //default shown;
  static const level CONFIG  =  30;  //default hidden;
  static const level DEBUG   =  40; //default hidden;
  static const level FINE    =  50;
  class ger{
  public:
    static ger& i(){ return getInstance(); }
    static ger& getInstance(){
      if( not instance_ )
	instance_ = new ger;
      return *instance_;
    };
    //    template <typename 
    static void fatal(const char* s){
      if( i().log_level_ >= FATAL )
	i().push_(s, __func__ );
    };
    static void error(const char* s){
      if( i().log_level_ >= ERROR )
	i().push_(s, __func__ );
    };
    static void severe(const char* s){
      if( i().log_level_ >= SEVERE )
	i().push_(s, __func__ );
    };
    static void warning(const char* s){
      if( i().log_level_ >= WARNING )
	i().push_(s, __func__ );
    };
    static void info(const char* s){
      if( i().log_level_ >= INFO )
	i().push_(s, __func__ );
    };
    static void config(const char* s){
      if( i().log_level_ >= CONFIG )
	i().push_(s, __func__ );
    };
    static void debug(const char* s){
      if( i().log_level_ >= DEBUG )
	i().push_(s, __func__ );
    };
    static void fine(const char* s){
      if( i().log_level_ >= FINE )
	i().push_(s, __func__ );
    };
    static void set_level(const level& l){
      pthread_mutex_lock(&i().mutex_);
      i().log_level_ = l;
      pthread_mutex_unlock(&i().mutex_);
    };
    //    static level get_level()
  private:
    level log_level_;
    pthread_mutex_t mutex_;
    void push_(const char* s, const char* what){
      this->print_(s, what);
    }
    void print_(const char* s, const char* what){
      char buffer[80];
      pthread_mutex_lock(&mutex_);
      now( buffer, 80 );
      printf("%s [%s]:%s\n", buffer, what, s );
      pthread_mutex_unlock(&mutex_);
    };
    
    static ger * instance_;
    ger(const Log::ger& l){};
    ger():log_level_(21){//	  mutex_(PTHREAD_MUTEX_INITIALIZER){
      pthread_mutex_init(&mutex_, NULL);
    };
    ~ger(){};
  };
}
Log::ger * Log::ger::instance_=0;

#endif
