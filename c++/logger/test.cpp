#include <iostream>
#include "logger.h"
#include <sstream>

using namespace std;

int loop(int * n){
  stringstream ss;
  ss << "t_id:" << *n << flush; 
//   cout << "n " << *n << endl;
  const char* msg = ss.str().c_str();
  for( int i = 0; i < 200 ; ++i){
    Log::ger::fatal(msg);
    Log::ger::error(msg);
    Log::ger::severe(msg);
    Log::ger::warning(msg);
    Log::ger::debug(msg);
    Log::ger::info(msg);
    Log::ger::config(msg);
    Log::ger::fine(msg);
  }

}

int main(int args, char** argv){
  if( args >= 2 )
    Log::ger::set_level(atoi(argv[1]) );
  cerr << "hello," << endl;

  int n = 5;
  pthread_t th[n];
  for( int i = 0; i < n; ++i ){
    int* p = (int*)malloc(sizeof( int));
    *p = i;
    pthread_create(&th[i], NULL, (void* (*)(void*))loop, (void*)p);
  }

  for( int i = 0; i < n; ++i ){
    pthread_join(th[i], NULL);
  }
}
