#include "tcp_client.h"
#include "udp_client.h"
#include <stdlib.h>

///似非telnetクライアント
//http://www.ueda.info.waseda.ac.jp/~toyama/network/example2.html

int main(int args, char** argv){
  tcp_client * c = new tcp_client();
  //  c->c(argv[1], atoi(argv[2]));
  c->c("localhost", 5001);
  //  char buf[1024];
  //fgets(buf, 1024, stdin);
  c->r();
  //  for( int i = 0 ; i < 1000; ++i )
  //  c->w( "message message message" );
  c->w( "message message messae end" );
  cout << "end sent." << endl;
  sleep(1);
  c->disconnect();
  cout << "closed." << endl;
}
void udp(int n, char * mesg){
  udp_client * c = new udp_client();
  c->c("localhost", 5001);
  for( int i = 0; i < n ; i++){
    c->w( mesg );
    cout << i+1 << " th message sent: " << mesg << endl;
  }
  delete c;
}
