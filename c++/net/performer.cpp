#include "threadpool.h"
#include <sys/time.h>

#include "performer.h"

int  main(){
  struct timeval start, end;
  int Nthreads, Nrepeat;
  Nthreads = 4;
  Nrepeat  = 32;
  //  tcp_performer tp(Nrepeat, Nthreads);
  threadpool_tobj tp;
  for(int i = 0; i < Nthreads ; ++i){
    tp.add( new tcp_performer( Nrepeat ));
  }
  gettimeofday( &start, NULL );
  tp.start();
  tp.join();
  gettimeofday( &end, NULL );
  double t = (end.tv_sec - start.tv_sec)+ ((double)(end.tv_usec - start.tv_usec))/1000000.0;
  printf("---%f seconds - %f tps - %d datas to process\t--\n",
  	 t,  Nrepeat * Nthreads/t, Nrepeat * Nthreads );

}

