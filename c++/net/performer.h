#ifndef PERFORMER_H
#define PERFORMER_H

#include "threadpool.h"
#include <stdio.h>
#include "tcp_client.h"
#include "common.h"

// class tcp_performer : public threadpool_tobj{
// public:
//   performer(int n, int m) : threadpool_tobj(n),
// 			    Nrepeat(m)
//   { 
//     pthread_mutex_init( &mutex_, NULL );
//   };
//   virtual ~performer(){};
//   virtual void run() =0;
//   virtual void stop(){
//     stop_flag_ = true;
//   };
//   int Nrepeat;
//   pthread_mutex_t mutex_; 


// };

class tcp_performer : public thread{
public:
  tcp_performer(int n): thread(), Nrepeat(n) {};
  ~tcp_performer(){};
  virtual int run(){
    tcp_client * c;
    for( int i = 0; i < Nrepeat ; ++i ){
      c = new tcp_client;
      c->c("localhost", 5001) ;
      //c->r();
      //      for( int i = 0 ; i < 1024; ++i )
      c->w( FIXED_MSG );
      //c->r();
      //cout << "hoge." << endl;
      //usleep(50);
      c->disconnect();
      delete c;
    }      
    return Nrepeat;
  };
  int Nrepeat;
};


#endif
