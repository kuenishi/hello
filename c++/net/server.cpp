#include "tcp_server.h"
#include "udp_server.h"
#include <signal.h>
#include <stdlib.h>
void stop(int i){
  printf("server successfully stopped with %d even if you have exception.\n", i);
  ///TODO: double free when using CentOS.
  /// doesn't happen in MacOS...
  exit(1);
}

int setSignal(){
  //DB(fprintf(stderr,"InitSignal\n"));
  signal(SIGINT, stop);
  signal(SIGTERM, stop);
#ifdef LINUX
  signal(SIGCLD,stop);
#endif
  return(0);
}


void tcp(){

  tcp_server*  srv = new tcp_server();
  srv->start();
  delete srv;
}


int main(int args, char** argv){
  setSignal();
  printf( "server started, with pid : %d\n", getpid() );

  tcp();
}
//   udp_server * srv = new udp_server();
//   srv->start();
//   delete srv;

