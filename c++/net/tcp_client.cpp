#include "tcp_client.h"
#include <stdlib.h>

#include "common.h"

//int main(int args, char** argv){
void tcp_client::c( string hostname, long unsigned int port  ){
  char service[16];
  struct addrinfo hints, *res0, *res;
  int err;
  
 memset(&hints, 0, sizeof(hints));
 hints.ai_socktype = SOCK_STREAM;
 hints.ai_family = PF_UNSPEC;
 sprintf( service, "%ld", port );
 
 if ((err = getaddrinfo(hostname.c_str(), service, &hints, &res0)) != 0) {
   printf("error %d\n", err);
   exit(1);
 }

 cout << "connecting..." << std::flush;
 for (res=res0; res!=NULL; res=res->ai_next) {
   sd = socket(res->ai_family, res->ai_socktype, res->ai_protocol);
   if (sd < 0) {
     continue;
   }
 
   if (connect(sd, res->ai_addr, res->ai_addrlen) != 0) {
     close(sd);
     continue;
   }

   break;
 }
 
 if (res == NULL) {
   /* 有効な接続が出来なかった */
   printf("failed\n");
   return;
   //exit(1);
 }else{
 
   freeaddrinfo(res0);
   printf("conncted\n");
 }
 //r_.setfd(sd);
 //w_.setfd(sd);
}


void  tcp_client::r(){
  //r_.start();
 // r_.join();
  int total_byte_read = 0;
  int total_byte_to_read = FIXED_MSG_LENGTH;
  char str_to_read[FIXED_MSG_LENGTH+1];
  int ret = 0;
  memset( str_to_read, 0, FIXED_MSG_LENGTH+1);
  
  while( total_byte_read < total_byte_to_read ){
    ret = read( sd, str_to_read + total_byte_read,
		total_byte_to_read - total_byte_read);
    if( ret > 0 ){
      total_byte_read += ret;
    }else if( ret == 0 ){
      break; //end;
    }else{
      break; //some error
    }
  }
  printf("client recvd:%s\n", str_to_read);

}

void tcp_client::w( string s ){
  int total_byte_written  = 0;
  int total_byte_to_write = FIXED_MSG_LENGTH;
  char str_to_write[FIXED_MSG_LENGTH];
  int ret = 0;
  
  memset( str_to_write, 0, FIXED_MSG_LENGTH);
  sprintf( str_to_write, "%s", s.c_str());

  while( total_byte_written < total_byte_to_write ){
    ret = write(sd, str_to_write + total_byte_written, 
		total_byte_to_write - total_byte_written);
    if( ret > 0 ){
      total_byte_written += ret;
    }else if( ret == 0 ){
      break; //end;
    }else{
      break; //some error
    }
  }
}

void tcp_client::disconnect(){
  //  r_.kill();
  //  w_.kill();
  close(sd);
    /* クライアントと接続されているソケットからデータを受け取る */
}

tcp_client::tcp_client(){}
tcp_client::~tcp_client(){}
