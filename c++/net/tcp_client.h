#ifndef TCP_CLIENT_H__
#define TCP_CLIENT_H__

#include <iostream>
#include <sys/types.h>
#include <sys/socket.h>

#include <sys/un.h>
#include <netinet/in.h>
#include <netdb.h>
#include <string>
#include <iostream>

#include "threadpool.h"

using namespace std;
static const std::string SOCK_NAME="/tmp/socketooo";

class reader : public thread{
public:
  reader(int fd): thread(), fd_(fd){};
  reader(): thread(), fd_(STDIN_FILENO){};
  ~reader(){};
  void setfd(int fd){ fd_ = fd ; };
  int run(){
    int ret = 0;
    cout << "thread " << __func__ << " started." << endl;
    char buf[1024];
    while( ret >= 0 ){
      memset( buf, 0, 1024 );
      ret = read( fd_, buf, 1024 );
      cout << "read: " << buf << endl;

      string msg(buf);
      if( msg.find( "end" ) != string::npos )
	break;
    }
    return ret;
  }
private:
  int fd_;
};

class writer : public thread{
public:
  writer(): thread(), fd_(STDOUT_FILENO){};
  writer(int fd): thread(), fd_(fd){};
  ~writer(){};
  void setfd(int fd){ fd_ = fd ; };
  int run(){
    cout << "thread " << __func__ << " started." << endl;
    int ret = 0;
    char buf[1024];
    while( ret >= 0 ){
      cout << "write" << std::flush;
      memset( buf, 0, 1024 );
      fgets(buf, 1024, stdin);
      ret = write( fd_, buf, sizeof(buf) );
      if( ret < 0 )
	break;
    }
    return ret;
  }
private:
  int fd_;
};

class tcp_client{
 public:

  tcp_client();
  ~tcp_client();

  void c(string, long unsigned int);
  void r();
  void w(string);
  void disconnect();
  
 private:
  typedef struct sockaddr_in SOCKADDR;
  SOCKADDR  sock_addr;
  typedef socklen_t SOCKLEN; //モノによってはint
  //  SOCKLEN    len;
  struct hostent *hp;
  //  int    ret;
  //char   buf[1024];
  int sd;// = socket( AF_INET, SOCK_STREAM, 0); //O@ip
  reader r_;
  writer w_;
};
#endif
