#include "tcp_server.h"

//似非鸚鵡返しサーバー
//http://www.ueda.info.waseda.ac.jp/~toyama/network/example2.html

#include <sys/types.h>
#ifdef KQUEUE
#include <sys/event.h> //BSD specific
#endif
#ifdef EPOLL
#include <sys/epoll.h>
#endif
#include <sys/time.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include <stdlib.h>
#include "common.h"

int write_(int fd, const char * mesg, int total_byte_to_write){
  int total_byte_written  = 0;
  //  int total_byte_to_write = s.size();
  char str_to_write[FIXED_MSG_LENGTH];
  int ret = 0;
  
  memset( str_to_write, 0, FIXED_MSG_LENGTH);
  sprintf( str_to_write, "%s", mesg);

  while( total_byte_written < total_byte_to_write ){
    ret = write(fd, str_to_write + total_byte_written, 
		total_byte_to_write - total_byte_written);
    if( ret >= 0 ){
      total_byte_written += ret;
    }else if( ret == 0 ){
      close(fd);
      break;
    }else{
      perror( __func__ );
      exit(-1);
      return -1;
      //break;
    }
  }
  return ret;
}
int read_(int fd,  char * mesg, int total_byte_to_read){
  int total_byte_read = 0;
  //  int total_byte_to_read = FIXED_MSG_LENGTH;
  char * str_to_read = mesg; //[FIXED_MSG_LENGTH];
  int ret = 0;
  memset( str_to_read, 0, total_byte_to_read);//FIXED_MSG_LENGTH);

  while( total_byte_read < total_byte_to_read ){
    ret = read( fd, str_to_read + total_byte_read,
		total_byte_to_read - total_byte_read);
    if( ret > 0 ){
      total_byte_read += ret;
    }else if( ret == 0 ){
      //      close(fd);
      break;
    }else{
      perror( __func__ );
      //      break;
      //exit(-1);
      //return -1;
    }
  }
  return ret;
}

void workers::run(){
  //  int i = 0;
  task * t;
  //  struct sockaddr_in peer_sin;
  //  socklen_t len = sizeof(peer_sin);
  char buf[1024];
  int ret;
  while( ! stop_flag_ ){
    memset(buf, 0, 1024);
    try{
      //#ifdef KQUEUE
      t = equeue_->deq();
      //      ret = read(kep->ident, buf, 1024);
      //      ret = read_( kep->ident , buf, FIXED_MSG_LENGTH );
      cout << t->get_name() << endl;
      delete t;
//       string msg("Re: ");
//       msg += buf;
//       if( ret > 0 ){
// 	ret = write_( kep->ident, FIXED_MSG, FIXED_MSG_LENGTH);
//       //      cout << "mesg recvd : " << buf << endl;
//       //      if( msg.find( "end" ) != string::npos )
// 	if( ret > 0 )
// 	  close(kep->ident);
//       }
      //free(kep);
// #endif
// #ifdef EPOLL
//       eep = equeue_->deq();

//       ret = read_( eep->data.fd , buf, FIXED_MSG_LENGTH );
//       if( ret > 0 ){
// 	write(eep->data.fd, "Re:", 4);
// 	write(eep->data.fd, buf, 1024);
// 	cout << "mesg recvd : " << buf << endl;
//       }
      //close(eep->data.fd);
      //free(eep);
      //#endif
    }catch(...){
      usleep(10.0);
    }
  }
}
void workers::stop(){
  stop_flag_ = true;
}


//int main(int args, char** argv){
void tcp_server::init(){
  workers_.start();
  sd = socket( AF_INET, SOCK_STREAM, 0); //O@ip
  if( sd < 0 ){
    perror("socket(2)");
    exit(1);
  }
  
  memset( &sock_addr, 0, sizeof(struct sockaddr));
  memset( &caddr, 0, sizeof(struct sockaddr));

  int on = 1;
  setsockopt( sd, SOL_SOCKET, SO_REUSEADDR, &on, sizeof(on) );
  
  // ソケットの名前を入れておく
  sock_addr.sin_family = AF_INET;
  sock_addr.sin_addr.s_addr = htonl(INADDR_ANY);
  sock_addr.sin_port = htons(SERVER_PORT);
  //  strncpy(sock_addr.sin_path, SOCK_NAME.c_str(), SOCK_NAME.size() );
  // ソケットにアドレスをバインドする。bind()が失敗しないよう、最初にunlink()を
  // 使用して、ソケットに対応するファイルを消去する。
  if (bind(sd, (struct sockaddr *)&sock_addr, 
	   sizeof(sock_addr)/*.sin_family) + sizeof(sock_addr.sin_path)*/) < 0){
    perror("bind");
    exit(1);
  }else{
    cout << "bind successfully done." << endl;
  }
  
  // listenをソケットに対して発行する
  if (listen(sd, 1) < 0) {
    perror("listen");
    exit(1);
  }
#ifdef KQUEUE
  kq = kqueue();
  if( kq < 0 ){
    perror("kqueue");
    exit(1);
  }

// TODO: how do we stop this server?  SIGXXXX or ...
//   ctl = socket( AF_UNIX, SOCK_STREAM, 0 );
//   struct sockaddr_un addr;
//   strcpy(addr.sun_path, "/tmp/tcp_server");
//   addr.sun_family = AF_UNIX;
//   if(bind (ctl, (struct sockaddr *) &addr,
// 	   strlen(addr.sun_path) + sizeof (addr.sun_family)) < 0 ){    
//     perror("bind2");
//     exit(1);
//   }

  struct kevent * ke = (struct kevent * )malloc(sizeof(struct kevent));
  memset( ke, 0, sizeof(struct kevent));
  ke->ident = sd;
  ke->filter = EVFILT_READ;
  ke->flags  = EV_ADD;
  struct timespec waitspec;     /* kevent に待ち時間を指定するための構造体 */
  waitspec.tv_sec  = 2;         /* 待ち時間に 2.500 秒を指定 */
  waitspec.tv_nsec = 500000;
  printf( "%d: listen socket is added into kqueue.\n", 
	  kevent( kq, ke, 1,  ke, 1, &waitspec) );
  free(ke);
#endif
#ifdef EPOLL
  kq = epoll_create( 16 );
  struct epoll_event * ee = (struct epoll_event*)malloc( sizeof( struct epoll_event ));
  memset( ee, 0, sizeof( struct epoll_event) );
  ee->data.fd = sd;
  ee->events  = EPOLLIN;
  printf( "%d: listen socket is added into epoll.\n", 
	  epoll_ctl( kq, EPOLL_CTL_ADD, sd, ee ) );
  free(ee);
#endif
}

void tcp_server::start_loop(){  
  //  int    ret;

  struct timespec waitspec;     /* kevent に待ち時間を指定するための構造体 */
  waitspec.tv_sec  = 0;         /* 待ち時間に 2.500 秒を指定 */
  waitspec.tv_nsec = 50000;
  int nevents;
  struct sockaddr_in peer_sin;
  socklen_t len = sizeof(peer_sin);

  char buf[1024];
  int ret;
  int max_events = 128;
#ifdef KQUEUE
  struct kevent ke[max_events];
  struct kevent * keo = ke;
#endif
#ifdef EPOLL
  struct epoll_event e[max_events];
  struct epoll_event * ee = e;
#endif
  int i = 0;
  for(;;) {
    memset(buf, 0, 1024);

#ifdef KQUEUE
    //イベントをひとつずつ処理していく。。。
    //keo = (struct kevent * )malloc(max_events * sizeof(struct kevent));
    keo = ke;
    memset( keo, 0, max_events * sizeof(struct kevent));
    //    nevents = kevent( kq, NULL, 0,  keo, max_events, &waitspec);
    nevents = kevent( kq, NULL, 0,  keo, max_events, NULL);
    if( nevents > 0 )
      cout << "one poll..." << nevents << "/" << i++ <<  '\t' << equeue_->size() << " events." << endl;

    for( ; keo - ke < nevents ; keo++ ){
	//    printf( "nevents: %d\n", nevents);
	
	if( keo->ident == sd ){
	  memset( &peer_sin , 0, sizeof(struct sockaddr_in) );
	  keo->ident = accept(sd, (struct sockaddr *)&peer_sin, &len);
	  if( keo->ident < 0 )	    continue;
	  keo->filter = EVFILT_READ;// | EVFILT_WRITE;
	  keo->flags  = EV_ADD;
	  kevent( kq, keo, 1, NULL, 0 , &waitspec) ;
	  //	  free(keo);
	  cout << "accepted from " << inet_ntoa(peer_sin.sin_addr) 
	       << " : "  << ntohs(peer_sin.sin_port) << endl;
	}else{
	  //電文組み立ててタスクをエンキュー
	  memset(buf, 0, 1024);
	  if ( keo->flags == EV_EOF or (keo->fflags == NOTE_LOWAT && keo->data == 0 ) )
	    close(keo->ident);
	  else{
	    ret = read_( keo->ident , buf, FIXED_MSG_LENGTH );
	    if( ret <= 0 )
	      close(keo->ident);
	    else{
	      cout << buf << endl;
	      equeue_->enq( new task(buf) );
	    }
	  }
      }
    }
#endif
#ifdef EPOLL
    //ee = (struct epoll_event * )malloc(max_events * sizeof(struct epoll_event));
    memset( ee, 0, max_events * sizeof(struct epoll_event));
    nevents = epoll_wait( kq, ee, max_events, 10);
    if( nevents > 0 ){
      //    printf( "nevents: %d\n", nevents);
      
      if( ee->data.fd == sd ){
	memset( &peer_sin , 0, sizeof(struct sockaddr_in) );
	ee->data.fd = accept(sd, (struct sockaddr *)&peer_sin, &len);
	if( ee->data.fd < 0 )	    continue;
	ee->events = EPOLLIN;
	epoll_ctl(kq, EPOLL_CTL_ADD, ee->data.fd, ee);
	free(ee);
	cout << "accepted from " << inet_ntoa(peer_sin.sin_addr) 
	     << " : "  << ntohs(peer_sin.sin_port) << endl;
      }else{
	equeue_->enq( ee );
      }
    }
#endif
    if( nevents < 0) {
      perror( "kqueue/epoll_wait" );
      // exit(-1);
    }
  }
}

void  tcp_server::start(){
  init();
  start_loop();
};
tcp_server::tcp_server():
  equeue_( new equeue ),
  workers_(2, equeue_){
};
tcp_server::~tcp_server(){
  workers_.stop();
};

