#ifndef TCP_SERVER_H__
#define TCP_SERVER_H__

#include <iostream>
#include <sys/types.h>
#include <sys/socket.h>

#include <sys/un.h>
#include <netinet/in.h>
#include <string>
#include <iostream>

using namespace std;
static const unsigned int SERVER_PORT = 5001;

//select
//unix domain socket
//pio
//thread pool
#include "threadpool.h"
#include "xqueue.h"
#include "common.h"


class task{
public:
  task(char * str): name_(str){};
  ~task(){};
  string & get_name(){ return name_; };
private:
  string name_ ;
};

//  typedef  struct kevent  equeue;
//  typedef  xqueue<struct epoll_event* equeue;
typedef xqueue<task*> equeue;


class workers : public threadpool_tid{
public:
  workers(int i, equeue* eq) :
    threadpool_tid(i), equeue_(eq){  };

  ~workers(){};
  void run();
  void stop();
private:
  equeue*  equeue_;
};

///@brief test TCP server
class tcp_server{
 public:

  tcp_server();
  virtual ~tcp_server();

  void start();

 private:
  void init(); //createsock -> bind -> listen
  void start_loop(); // accept

  equeue*  equeue_;

  workers workers_;

  typedef struct sockaddr_in SOCKADDR;
  SOCKADDR  sock_addr,    caddr;
  typedef socklen_t SOCKLEN; //モノによってはint
  SOCKLEN    len;
  char   buf[1024];
  int sd;
  int kq;
  //  int ctl;
};

#endif
