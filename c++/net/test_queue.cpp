#include "xqueue.h"
#include <iostream>
#include <string>

using namespace std;

int stress_push(xqueue<string>* x){
  while(true){
    x->enq("hoag");
  }
}

int stress_wd(xqueue<string>* x){
  while(true){
    try{
      x->deq();
    }catch(...){
      ;
    }
  }
}
void stress(int n=5){
  xqueue<string> x;
  
  pthread_t ti[n];
  pthread_t to[n];
  for( int i = 0; i < n ; ++i ){
    usleep(500);
    pthread_create(&to[i], NULL, reinterpret_cast<void* (*)(void*)>(stress_wd), &x);
  }
  sleep(2);
  for( int i = 0; i < n ; ++i ){
    usleep(500);
    pthread_create(&ti[i], NULL, reinterpret_cast<void* (*)(void*)>(stress_push), &x);
  }
  //     pthread_create(&ta, NULL, reinterpret_cast<void* (*)(void*)>(stress_wd) ,  &x);
  
  while(true){
    cout << "in:" << x.get_inc() << "tps\tout:" << x.get_outc()
	 << "tps\tsize:" << x.size() << endl;
    x.clearc();
    sleep(1);
    
  }
}

int main(int args, char ** argv){
  stress();
}
