#include "threadpool.h"
#include <signal.h>
//スレッドプールらいぶらり　途中やめ状態

#include <errno.h>
#include <algorithm>

int threadpool_tid::start(){
  for( vector<pthread_t>::iterator it = ts_.begin();
       it != ts_.end() ; ++it ){
    pthread_create( &(*it) ,  0, threadpool_tid::start_routine, this );
  }
  return 0;
}
int threadpool_tid::join(){
  int ret ;
  for( vector<pthread_t>::iterator it = ts_.begin();
       it != ts_.end() ; ++it ){
    ret = pthread_join( *it ,  NULL );
    switch( ret ){
    case EINVAL: break;
//      [EINVAL]           The implementation has detected that the value speci-
//                         fied by thread does not refer to a joinable thread.
    case ESRCH: break;
//      [ESRCH]            No thread could be found corresponding to that speci-
//                         fied by the given thread ID, thread.
    case EDEADLK: break;
//      [EDEADLK]          A deadlock was detected or the value of thread speci-
//                         fies the calling thread.
    default: ;
    }
  }
  return 0;
}
int threadpool_tid::detach(){ //in fact I don't recommend using detach.
  for( vector<pthread_t>::iterator it = ts_.begin();
       it != ts_.end() ; ++it ){
    pthread_detach( *it );
  }
  return 0;
}
int threadpool_tid::kill(){
  for( vector<pthread_t>::iterator it = ts_.begin();
       it != ts_.end() ; ++it ){
    pthread_kill( *it ,  SIGTERM );
  }
  return 0;
}

void * threadpool_tid::start_routine(void* arg){
  if( arg == NULL ) return NULL;
  threadpool_tid* p = static_cast<threadpool_tid*>(arg);
  p->run();
  return arg;
}


thread::thread(): tid_(0), finished_(false), running_(false){
#ifdef _DEBUG_THREAD
  cerr << "thread created: " << __func__ << endl;
#endif
}
thread::~thread(){
#ifdef _DEBUG_THREAD
  cerr << "thread destructed: " << __func__ << endl;
#endif
}

void* thread::start_routine(void* arg){
  if(arg == 0)  return 0;
  thread* p = static_cast<thread*>(arg);
  p->run_thread();
  // if(p->finished()){
  //   delete p;
  //   p = NULL;
  // }
  return arg;
}

int thread::start(){
  if(running_ )
    return -1;
  else
    return::pthread_create(&tid_, 0, thread::start_routine, this);
}

int thread::join(){
  return pthread_join(tid_, NULL);
}

//
int thread::run_thread(){
  running_ = true;
  int r = run();
  running_ = false;
  finished_ = true;
  return r;
}

int thread::detach(){
  return pthread_detach(tid_);
}
int thread::kill(){
  return pthread_kill(tid_, SIGTERM);
}

//virtualized
// int thread::run(){
//   return 0;
// }
#ifdef THREADPOOL_TEST_DEMO
class threadpool_demo : public threadpool{
public:
  threadpool_demo(int i) : threadpool(i){  };
  ~threadpool_demo(){};
  void run() const {
    int i = 0;
    while( ! stop_flag_ )
      printf("I am thread: %d.\n", i++);
  };
  void stop(){
    stop_flag_ = true;
  };
};

int  main(){
  threadpool_demo tp(3);
  tp.start();
  sleep(1);
  tp.stop();
  tp.join();
}
#endif

threadpool_tobj::~threadpool_tobj(){
  for( vector<thread*>::iterator it = ts_.begin();
       it != ts_.end() ; ++it ){
    delete *it;
  }
}
int threadpool_tobj::start(){
  for( vector<thread*>::iterator it = ts_.begin();
       it != ts_.end() ; ++it ){
    (*it)->start();
  }
  return 0;
}
int threadpool_tobj::join(){
  for( vector<thread*>::iterator it = ts_.begin();
       it != ts_.end() ; ++it ){
    (*it)->join();
  }
  return 0;
}
int threadpool_tobj::detach(){
  for( vector<thread*>::iterator it = ts_.begin();
       it != ts_.end() ; ++it ){
    (*it)->detach();
  }
  return 0;
}
int threadpool_tobj::kill(){
  for( vector<thread*>::iterator it = ts_.begin();
       it != ts_.end() ; ++it ){
    (*it)->kill();
  }
  return 0;
}

int threadpool_tobj::add(thread* p_t){
  ts_.push_back(p_t);
  return p_t->start();
}
int threadpool_tobj::add(vector<thread*> vp_t){
  ts_.insert(ts_.begin(), vp_t.begin(), vp_t.end());
  for( vector<thread*>::iterator it = vp_t.begin();
       it != vp_t.end() ; ++it ){
    (*it)->start();
  }
  return 0;
}
