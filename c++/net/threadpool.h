#ifndef THREADPOOL_H
#define THREADPOOL_H

#include <pthread.h>
#include <vector>

using std::vector;

class thread{
 private:
  static void* start_routine(void *);
  
 public:
  thread();
  virtual ~thread();
  int start();     ///startでスレッドを起動できる
  int run_thread();     ///スレッドなしで処理を開始できる。
  
  int join();
  int detach();
  int kill();
  
  bool finished(){ return finished_; };
  bool started(){ return running_; };
  virtual int run() = 0; //処理する中身を書く
  
protected:
  pthread_t tid_;
  bool finished_;
  bool running_;
};

/**
 * @brief スレッドプールクラス。純粋仮想関数run()をオーバーライドすれば多数のスレッドを起動できる。
 * ストップフラグstop_flag_が用意されている。
 *
 **/

template <class ThreadType>
class threadpool{
public: 
  threadpool(int num_threads):
    ts_( vector<ThreadType>(num_threads) ),
    stop_flag_(false){
  };
  virtual ~threadpool(){};

  virtual int start() = 0;
  virtual int join() = 0;
  virtual int detach() = 0;
  virtual int kill() = 0;
  virtual void run() = 0;

  //  int add( ThreadType ) = 0 ;

 protected:
  threadpool(){};

 protected:
  vector< ThreadType > ts_;
  bool stop_flag_;
};

/**
 * @brief スレッドプールクラス。純粋仮想関数run()をオーバーライドすれば多数のスレッドを起動できる。
 * ストップフラグstop_flag_が用意されている。
 *
 **/
class threadpool_tid : public threadpool<pthread_t>{
public: 
  threadpool_tid(int num_threads): threadpool<pthread_t>(num_threads){};
  virtual ~threadpool_tid(){};

  int start();
  int join();
  int detach();
  int kill();
  virtual void run() = 0;

 private:
  static void * start_routine(void*);
  threadpool_tid(){};

};

class threadpool_tobj : public threadpool<thread*>{
public:
  threadpool_tobj():threadpool<thread*>(0){};
  virtual ~threadpool_tobj();

  int start();
  int join();
  int detach();
  int kill();
  virtual void run(){};
  
  int add( thread* );
  int add( vector<thread*> );
  
 private:
  static void * start_routine(void*);
};
#endif

