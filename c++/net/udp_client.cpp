#include "udp_client.h"
#include <stdlib.h>

///似非telnetクライアント
//http://www.ueda.info.waseda.ac.jp/~toyama/network/example2.html

//int main(int args, char** argv){
void udp_client::c( string hostname, long unsigned int port  ){
  sd = socket( AF_INET, SOCK_DGRAM, 0); //O@ip
  if( sd < 0 ){
    perror("socket(2)");
    exit(1);
  }

  if ((hp = gethostbyname(hostname.c_str())) == NULL) {
    perror("No such host");
    exit(1);
  }
  memset( &sock_addr, 0, sizeof(struct sockaddr));
  
  // ソケットの名前を入れておく
  bcopy(hp->h_addr, &sock_addr.sin_addr, hp->h_length);
  sock_addr.sin_family = AF_INET;
  sock_addr.sin_port = htons( port );
  //  strncpy(sock_addr.sin_path, SOCK_NAME.c_str(), SOCK_NAME.size() );
  
}
void  udp_client::r(){
}

void udp_client::w(string mesg){
  sendto(sd, mesg.c_str(), mesg.size(), 0, (struct sockaddr *) &sock_addr, sizeof (sock_addr));
}

udp_client::udp_client(){}
udp_client::~udp_client(){}
