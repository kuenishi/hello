#ifndef UDP_CLIENT_H__
#define UDP_CLIENT_H__

#include <iostream>
#include <sys/types.h>
#include <sys/socket.h>

#include <sys/un.h>
#include <netinet/in.h>
#include <netdb.h>
#include <string>
#include <iostream>

using namespace std;
//static const std::string SOCK_NAME="/tmp/socketooo";

class udp_client{
 public:

  udp_client();
  ~udp_client();

  void c(string, long unsigned int);
  void r();
  void w(string );
  
 private:
  typedef struct sockaddr_in SOCKADDR;
  SOCKADDR  sock_addr;
  typedef socklen_t SOCKLEN; //モノによってはint
  //  SOCKLEN    len;
  struct hostent *hp;
  int    ret;
  char   buf[1024];
  int sd;// = socket( AF_INET, SOCK_STREAM, 0); //O@ip
};
#endif
