#include "udp_server.h"

//似非鸚鵡返しサーバー
//http://www.ueda.info.waseda.ac.jp/~toyama/network/example2.html

#include <sys/types.h>
//#include <sys/event.h>
#include <sys/time.h>

#include <stdlib.h>

//int main(int args, char** argv){
void udp_server::init(){
  sd = socket( AF_INET, SOCK_DGRAM, 0); //O@ip
  if( sd < 0 ){
    perror("socket(2)");
    exit(1);
  }
  
  memset( &sock_addr, 0, sizeof(struct sockaddr));
  memset( &caddr, 0, sizeof(struct sockaddr));
  
  // ソケットの名前を入れておく
  sock_addr.sin_family = AF_INET;
  sock_addr.sin_addr.s_addr = htonl(INADDR_ANY);
  sock_addr.sin_port = htons(UDP_SERVER_PORT);
  //  strncpy(sock_addr.sin_path, SOCK_NAME.c_str(), SOCK_NAME.size() );
  
  // ソケットにアドレスをバインドする。bind()が失敗しないよう、最初にunlink()を
  // 使用して、ソケットに対応するファイルを消去する。
  if (bind(sd, (struct sockaddr *)&sock_addr, 
	   sizeof(sock_addr)/*.sin_family) + sizeof(sock_addr.sin_path)*/) < 0){
    perror("bind");
    exit(1);
  }else{
    cout << "bind successfully" << endl;
  }
  
//   // listenをソケットに対して発行する
//   if (listen(sd, 1) < 0) {
//     perror("listen");
//     exit(1);
//   }
//   kq = kqueue();
//   if( kq < 0 ){
//     perror("listen");
//     exit(1);
//   }

//   struct kevent * ke = (struct kevent * )malloc(sizeof(struct kevent));
//   memset( ke, 0, sizeof(struct kevent));
//   ke->ident = sd;
//   ke->flags  = EV_ADD;
//   struct timespec waitspec;     /* kevent に待ち時間を指定するための構造体 */
//   waitspec.tv_sec  = 2;         /* 待ち時間に 2.500 秒を指定 */
//   waitspec.tv_nsec = 500000;
//   printf( "%d: listen socket is added into kqueue.\n", kevent( kq, ke, 1,  ke, 1, &waitspec) );
//   free(ke);
}

void udp_server::start_loop(){  
  int    ret;

//   struct kevent * keo = (struct kevent * )malloc(sizeof(struct kevent) * 16);
//   memset( keo, 0, sizeof(struct kevent) * 16);
//   struct timespec waitspec;     /* kevent に待ち時間を指定するための構造体 */
//   waitspec.tv_sec  = 0;         /* 待ち時間に 2.500 秒を指定 */
//   waitspec.tv_nsec = 500000;
//   int nevents;
  for(;;) {
    //    nevents = kevent( kq, NULL, 0,  keo, 16, &waitspec);
    //    printf( "nevents: %d\n", nevents);
    len = sizeof(caddr);

    memset( buf, 0, 1024 );
    ret = recvfrom (sd, buf, 1024, 0, (struct sockaddr *) &sock_addr,  &len);
   
    cout << ret << " bytes read: " << buf << endl;
  }

  close(sd);

}

void  udp_server::start(){
  init();
  start_loop();
};
udp_server::udp_server(){};
udp_server::~udp_server(){};
