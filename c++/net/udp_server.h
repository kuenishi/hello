#ifndef UDP_SERVER_H__
#define UDP_SERVER_H__



#include <iostream>
#include <sys/types.h>
#include <sys/socket.h>

#include <sys/un.h>
#include <netinet/in.h>
#include <string>
#include <iostream>

using namespace std;
static const unsigned int UDP_SERVER_PORT = 5001;

//ignore: select
//todo: unix domain socket
//done: pio
//done: thread pool

class udp_server{
 public:

  udp_server();
  virtual ~udp_server();

  void start();

 private:
  void init(); //createsock -> bind -> listen
  void start_loop(); // accept

  int    i;
  int fd2;
  typedef struct sockaddr_in SOCKADDR;
  SOCKADDR  sock_addr,    caddr;
  typedef socklen_t SOCKLEN; //モノによってはint
  SOCKLEN    len;
  char   buf[1024];
  int sd;
};

#endif
