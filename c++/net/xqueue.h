#ifndef XQUEUE_H
#define XQUEUE_H

// main function for gatekeeper
// 
// mutex付きqueue (一列、二列、N列)を作っていく練習にもなるだろう。
#include <pthread.h> // for mutex
#include <queue>

#include <stdexcept>
using std::runtime_error;
using std::queue;

//for stressing test
#include <string>
using std::string;

template <class T>
class xqueue{
public:
  xqueue(): inc_(0), outc_(0), capped_(false){//:mutex_(PTHREAD_MUTEX_INITIALIZER){
    pthread_rwlock_init( &rwlock_ , NULL );
    pthread_mutex_init( &mutex_, NULL );
    pthread_cond_init(  &cond_,  NULL );
  };
  virtual ~xqueue(){
    pthread_cond_destroy(&cond_);
    pthread_mutex_destroy(&mutex_);
    pthread_rwlock_destroy(&rwlock_);
  };
  
    void encap(){
      capped_ = true;
    };
    bool capped(){
      return capped_;
    };
    virtual void enq(const T& t){
      wlock();
      q_.push(t);
      inc_++;
      release_w();
      pthread_cond_signal( &cond_ );
    };
    virtual T deq() throw(std::exception) {
      rlock();
      if( this->empty() ){
	pthread_cond_wait(&cond_ , &mutex_ );
      }
	//	throw std::runtime_error(string("q_ is empty") );//+ typeid(this).name());
      T ret;
      if( not q_.empty() ){
	wlock();
	ret = q_.front();
	q_.pop();
	outc_++;
	release_w();
      }
      release_r();
      return ret;
    };
    virtual bool empty(){
      return q_.empty();
    };
    virtual int size(){
      rlock();
      int ret = q_.size();
      release_r();
      return ret;
    };
    virtual void clearc(){
      wlock();
      inc_ = outc_ = 0;
      release_w();
    };
  unsigned long int get_inc(){
    return inc_;
  };
  unsigned long int get_outc(){
    return outc_;
  };
private:
  unsigned long int inc_, outc_;
  queue<T> q_;
  pthread_mutex_t mutex_;
  pthread_rwlock_t rwlock_;
  pthread_cond_t  cond_;
  bool capped_;

  void rlock(){
    pthread_rwlock_rdlock( &rwlock_ );
    //    pthread_mutex_lock(&m_reader_);    //    trylock_();
  };
  void wlock(){
    pthread_mutex_lock(&mutex_);    //    trylock_();
  };
//   void trylock_(){
//     int EBUSY=0;
//     if( pthread_mutex_trylock(&mutex_) not_eq EBUSY){// not_eq EBUSY ){
//       return;
//     }else      
//       throw std::exception();
//   }
  void release_r(){
    pthread_rwlock_unlock( &rwlock_ );
    //pthread_mutex_unlock(&m_reader_);
  };
  void release_w(){
    pthread_mutex_unlock(&mutex_);
  };
};

// int stress_push(xqueue<string>* x){
//   while(true){
//     x->push("hoag");
//   }
// }

// int stress_wd(xqueue<string>* x){
//   while(true){
//     try{
//       x->withdraw();
//     }catch(...){
//       ;
//     }
//   }
// }
//   void stress(int n=5){
//     xqueue<string> x;
    
//     pthread_t ti[n];
//     pthread_t to[n];
//     for( int i = 0; i < n ; ++i ){
//       pthread_create(&ti[i], NULL, reinterpret_cast<void* (*)(void*)>(stress_push), &x);
//     }
//     for( int i = 0; i < n ; ++i ){
//       pthread_create(&to[i], NULL, reinterpret_cast<void* (*)(void*)>(stress_wd), &x);
//     }
//     //pthread_create(&ta, NULL, reinterpret_cast<void* (*)(void*)>(stress_wd) ,  &x);
    
//     while(true){
//       //      cout << "in:" << x.get_inc() << "tps\tout:" << x.get_outc()
//       //	   << "tps\tsize:" << x.size() << endl;
//       x.clearc();
//       sleep(1);
      
//     }
//   }

//} //end namespace

#endif
// #include <pthread.h>

// using namespace std;
// int main(int args, char ** argv){
//   xqueue<string> x;
//   int n = 5;
//   pthread_t t[n];
//   for( int i = 0; i < n ; ++i ){
//     pthread_create(&t[i], NULL, reinterpret_cast<void* (*)(void*)>(stress_push), &x);
//   }
//   //pthread_create(&ta, NULL, reinterpret_cast<void* (*)(void*)>(stress_wd) ,  &x);

//   while(true){
//     cout << "in:" << x.get_inc() << "tps\tout:" << x.get_outc()
// 	 << "tps\tsize:" << x.size() << endl;
//     x.clearc();
//     sleep(1);
    
//   }
// }
