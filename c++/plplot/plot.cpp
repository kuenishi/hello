#include <plot.h>

#include <string>
//#include <plstream.h>

#include <stdexcept>

using std::runtime_error;
using std::string;

Plot::Plot()
{
  init( NULL, NULL );
}

Plot::Plot( const string devname )
{
  init( devname.c_str(), NULL );
}

Plot::Plot( const string devname, const string fname)
{
  init( devname.c_str(), fname.c_str() );
}

Plot::~Plot()
{
  end();
}

//1
void Plot::set_env( double xmin, double xmax, double ymin, double ymax, int just, int axis )
{
  isEnvSet_ =true;
  pls->env( xmin, xmax, ymin, ymax, just, axis );
}

//2
void Plot::set_labels( const string xlab, const string ylab, const string toplab )
{
  if( not isEnvSet_ )
    throw runtime_error( "call set_env() before set_labels()." );
  pls->lab( xlab.c_str(), ylab.c_str(), toplab.c_str() );
}

//2
void Plot::set_box( const string xopt, double xtick, int nxsub, const string yopt, double ytick, int nysub )
{
  if( not isEnvSet_ )
    throw runtime_error( "call set_env() before set_box()." );
  pls->box( xopt.c_str(), xtick, nxsub, yopt.c_str(), ytick, nysub );
}
//0
void Plot::set_subpage( int nx, int ny )
{
  pls->ssub( nx, ny );
}



void Plot::points( const MArray<double>& val, int symbol )
{
  RowVector idx( val.length() );
  for( int i=0 ; i<val.length() ; i++ ) idx(i) = i;

  points( idx, val, symbol );
}

void Plot::points( const MArray<double>& x, const MArray<double>& y, int symbol )
{
  if( x.length() != y.length() ) {
    //err_warn( "length of X and Y are differ" );
    return;
  }
  
  Plot::Data data_x( x );
  Plot::Data data_y( y );
  pls->poin( x.length(), data_x.ptr(), data_y.ptr(), symbol );
}

void Plot::lines( const MArray<double>& val )
{
  RowVector idx( val.length() );
  for( int i=0 ; i<val.length() ; i++ ) idx(i) = i;

  lines( idx, val );
}

void Plot::lines( const MArray<double>& x, const MArray<double>& y )
{
  if( x.length() != y.length() ) {
    //err_warn( "length of X and Y are differ" );
    return;
  }

  Plot::Data data_x( x );
  Plot::Data data_y( y );
  pls->line( x.length(), data_x.ptr(), data_y.ptr() );
}  

void Plot::rectangle( double x0, double x1, double y0, double y1, double value, int line_col )
{
  PLFLT x[5], y[5];

  x[0] = x[3] = x0;
  x[1] = x[2] = x1;

  y[0] = y[1] = y0;
  y[2] = y[3] = y1;

  //  pls->col1( value );   // $B$J$<$+;H$($J$$(B
  plcol1( value );          // $B$N$G(B plcol1() $B$r;HMQ$9$k(B
  pls->fill( 4, x, y );

  if( line_col != -1 ) {
    x[4] = x[0];
    y[4] = y[0];
    pls->col0( line_col );
    pls->line( 5, x, y );
  }

  pls->line( 4, x, y );
  pls->col0( Plot::BLACK );
}


void Plot::draw_Matrix( const Matrix& m )
{
  set_env( 0.0, m.rows(), 0.0, m.cols(), 1, -1 );

  double max = m.row_max().max();
  double min = m.row_min().min();

  for( int i=0 ; i<m.rows() ; i++ )
    for( int j=0 ; j<m.cols() ; j++ )
      rectangle( i, i+1, j, j+1, ( m(i, j)-min )/( max-min ) );
}






// private member function
void Plot::init( const char* devname, const char* fname )
{
  isEnvSet_ = false;
  pls = new plstream();

  if( devname != NULL )
    pls->sdev( devname );
  if( fname != NULL )
    pls->sfnam( fname );

  init_CMap0();
  init_CMap1();

  pls->init();
}  

void Plot::init_CMap0()
{
  // default $B$N(B Color Map0 $B$rJQ99(B
  pls->scol0( Plot::WHITE, 255, 255, 255 ); //  0 white (default background );
  pls->scol0( Plot::BLACK,   0,   0,   0 ); //  1 black (default foreground );
  pls->scol0( Plot::RED  , 255,   0,   0 ); // 15 red
}

void Plot::init_CMap1()
{
  // Gray scale ( )$B$G(B $B=i4|2=(B
  //set_CMap1( 0.0, 0.0, 0.0, 1.0, 0.0, 0.0 );
  set_CMap1( 240.0, 0.0, 0.5, 0.5, 0.8, 0.8 );
}

void Plot::set_CMap1( double h0, double h1, double l0, double l1, double s0, double s1 )
{
  PLFLT i[2]  = { 0.0, 1.0 };
  PLFLT h[2]  = { h0,  h1 };
  PLFLT l[2]  = { l0,  l1 };
  PLFLT s[2]  = { s0,  s1 };
  bool rev[2] = { false, false };

  pls->scmap1n( 256 );
  pls->scmap1l( false, 2, i, h, l, s, rev );
}
  
void Plot::end()
{
  //  pls->end();
  delete pls;
}

// *************
// Plot::Data 
Plot::Data::~Data()
{
  delete [] data;
}


Plot::Data::Data( const MArray<double>& x )
{
  data = new PLFLT[x.length()];
  for( int i=0 ; i<x.length() ; i++ )
    data[i] = static_cast<PLFLT>( x(i) );
}

PLFLT* Plot::Data::ptr() const
{
  return data;
}


