#ifndef PLOT_H
#define PLOT_H

#include <string>
#include <octave/config.h>
#include <octave/Matrix.h>
#include <plplot/plstream.h>

using std::string;

class Plot
{
 public:
  Plot();
  ~Plot();

  Plot( const string devname );
  Plot( const string devname, const string fname );

  void set_env( double xmin, double xmax, double ymin, double ymax, int just = 0, int axis = 0 );

  ///must be called after set_env
  void set_labels( const string xlab, const string ylab, const string toplab );

  ///must be called after set_env
  void set_box( const string xopt, double xtick, int nxsub, const string yopt, double ytick, int nysub );

  ///must be called before set_env??
  void set_subpage( int nx, int ny );

  void points( const MArray<double>& val, int symbol = 0 );
  void points( const MArray<double>& x, const MArray<double>& y, int symbol = 0 );
  void lines( const MArray<double>& val );
  void lines( const MArray<double>& x, const MArray<double>& y );
  void rectangle( double x0, double x1, double y0, double y1, double value, int line_col = -1 );

  void draw_Matrix( const Matrix& m );

 private:
  plstream *pls;
  bool isEnvSet_;

  void init( const char* devname, const char* fname );
  void init_CMap0();
  void init_CMap1();
  void set_CMap1( double r0, double r1, double g0, double g1, double b0, double b1 );

  void end();


  class Data {
  public:
    ~Data();
    Data( const MArray<double>& x );
    PLFLT* ptr() const;
    PLINT  length();

  private:
    Data() {};
    PLFLT* data;


  };
  

 public:
  static const PLINT WHITE      = 0;
  static const PLINT BLACK      = 1;
  static const PLINT YELLOW     = 2;
  static const PLINT GREEN      = 3;
  static const PLINT AQUAMARINE = 4;
  static const PLINT PINK       = 5;
  static const PLINT WHEAT      = 6;
  static const PLINT GREY       = 7;
  static const PLINT BROWN      = 8;
  static const PLINT BLUE       = 9;
  static const PLINT BLUEVIOLET = 10;
  static const PLINT CYAN       = 11;
  static const PLINT TURQUOISE  = 12;
  static const PLINT MAGENTA    = 13;
  static const PLINT SALMON     = 14;
  static const PLINT RED        = 15;

};

#endif

