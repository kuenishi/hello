#include <iostream>
using namespace std;

#include <plot.h>

//$Id: test.cpp 46 2007-01-10 04:00:40Z kuenishi $

int main(){
  cout << "hello plplot!" << endl;
  int n = 100;

  Plot p("gcw", "aho");
  //Plot p("ps", "aho.ps");
  Matrix m(n+1, n+1, 1.0);

  double c = 200.0;
  double C = 1.0;
  ColumnVector v(n+1, 0);
  ColumnVector x(n+1, 0);
  for( int i = 0; i <= n ; ++i ){
    v(i) = sin( i* M_PI /n  );
    x(i) = i * M_PI / n;
    for( int j = 0; j <= n; ++j ){
      m( i, j ) = C * cos( c * (i - j) * M_PI / (n * n) );
      m( i, j ) += C * sin( c *  i * j * M_PI / (n * n) );
    }
  }


  p.set_env(0, M_PI , -1, 1);
  p.set_box(" xopt ", 0.001, 0, "yopt", 0.1, 1); 
  p.set_labels( "hogex", "hogey", "aaaa");
  p.lines( x, v );
  p.draw_Matrix( m );
  for( int i = 0; i < n ; ++i ){
    m.insert( v, 0, i );
  }  
  p.draw_Matrix( m );
  //  p.lines( v );
}
