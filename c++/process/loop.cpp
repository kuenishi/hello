#include "process.h"

//$Id: loop.cpp 106 2007-06-03 06:42:20Z kuenishi $
#include <iostream>

using namespace std;
int main(int args, char** argv){
  //  cout << "hello loop!" << endl;
  static const int N = 100;
  int n = (args < 2)? N: atoi(argv[1]);
  double cnt = 0;
  for( int i = 0; i< n; i++){
    cnt += 1.0 + 10e-5;
  }
  cout << "result: " << cnt << endl;
}
