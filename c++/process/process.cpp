#include "process.h"

//$Id: process.cpp 104 2007-06-01 16:07:21Z kuenishi $



pid_t process::start_(){
  pid_ = fork();
  if( pid_ == 0 ){
    execv( cmd_.c_str(), NULL );
  }
  return pid_;
}


process::process(const string& cmd):cmd_(cmd){
  start_();
}

pid_t process::pid(){
  return pid_;
}
