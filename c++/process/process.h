#ifndef PROCESS_H
#define PROCESS_H

//$Id: process.h 104 2007-06-01 16:07:21Z kuenishi $

#include <sys/types.h>
#include <unistd.h>

#include <string>

using std::string;

class process{
 private:
  pid_t pid_;
  const string cmd_;

  pid_t start_();
  process(){};

 public:
  process(const string&);
  ~process(){};

  pid_t pid();
};

#endif

