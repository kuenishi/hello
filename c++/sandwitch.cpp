#include <iostream>

//$ g++ sandwitch.cpp
//$ ./a.out
//http://www.linux.or.jp/JF/JFdocs/Program-Library-HOWTO/miscellaneous.html

using namespace std;

extern "C" void cnup(int* i){
  cerr << __func__ << endl;
}

void _init(void){
  __attribute__((cleanup(cnup))) int i;
  //i = (int*)malloc( sizeof(int) );
}

void _fini(void){
  cerr << __func__ << endl;
}


int main(){
  {__attribute__((cleanup(cnup))) int i; }
  _init();
  cerr << __func__ << endl;
  //free(i);
};


void __attribute__ ((constructor)) ctor(void){
  cerr << __func__ << endl;
}

void __attribute__ ((destructor)) dtor(void){
  cerr << __func__ << endl;
}
