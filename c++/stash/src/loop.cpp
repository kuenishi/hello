//#include "process.h"

//$Id: loop.cpp 148 2007-08-17 17:39:31Z kuenishi $
#include <iostream>

using namespace std;
int main(int args, char** argv){
  //  cout << "hello loop!" << endl;
  static const int N = 100;
  int n = (args < 2)? N: atoi(argv[1]);
  double cnt = 0;
  for( int i = 0; i< n; i++){
    cnt += 1.0 + 10e-5;
  }
  cout << "result: " << cnt << endl;
  sleep(1);
}
