#include <iostream>
#include <string>
#include "process.h"
#include <vector>

#include <util.h>

using namespace std;
static const int N = 10;

int main(int args, char** argv){
  cout << "hello stash!" << endl;
  
  //vector<process*> onExec;
  
  for(int i=0;i<10;i++){
    int size = 1024;
    char chcmd[size];
    
    cout << "prompt:" << flush;
    cin.getline(chcmd, size);
    string cmd(chcmd);    

    vector<string> *tmp =  split_string(cmd);

#ifdef DEBUG
     cout << cmd << tmp->size() << endl;
      if( not tmp->empty() ){
	vector<string>::iterator it = tmp->begin();
        for(;it not_eq tmp->end(); ++it){
 	 cout << *it << '|' << flush;
        }
        cout << endl;
      
      }

#else
    //cout << cmd << "-" << cmd.size() << endl;
     if( not tmp->empty() ){
       try{
	 process* c = new process( *(tmp->begin()) );
      // onExec.push_back(c);
      //cout << cmd << ":" << c->pid() << endl;
	 c->wait();

	 cout << flush;
	 delete c;
       }catch(...){
	 cout << "no such command:" << cmd << endl;
       }
     }
#endif
     delete tmp;
  }

}

