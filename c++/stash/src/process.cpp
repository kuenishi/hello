#include "process.h"
#include <sys/wait.h>
#include <sys/types.h>

//$Id: process.cpp 149 2007-08-17 18:03:59Z kuenishi $

extern int errno;

pid_t process::start_(){
  if(pipe(pfd_)==-1){
    throw std::exception();
  }
  pid_ = fork();
  if( pid_ == 0 ){
    int ret = execvp( cmd_.c_str(), NULL );
    if (/* errno == EACCES or*/ ret < 0)
      throw std::exception();

    exit(-1);
  }
  return pid_;
}


process::process(const string& cmd):cmd_(cmd){
  start_();
}


process::process(const vector<string>& cmd):cmd_( *(cmd.begin()) ){
  start_();
}


void process::wait(){
  waitpid( pid_, &status_, 0);
}
pid_t process::pid(){
  return pid_;
}
