#ifndef PROCESS_H
#define PROCESS_H

//$Id: process.h 149 2007-08-17 18:03:59Z kuenishi $

#include <sys/types.h>
#include <unistd.h>
#include <exception>
#include <string>
#include <vector>

using std::string;
using std::vector;

class process{
 private:
  pid_t pid_;
  const string cmd_;
  int pfd_[2];
  int status_;
  const vector<string> argv_;

  pid_t start_();
  process(){};

 public:
  process(const string&);
  process(const vector<string>&);
  ~process(){};

  void wait();
  pid_t pid();
};

#endif

