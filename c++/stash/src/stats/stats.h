#ifndef STATS_H
#define STATS_H

#include <vector>
using std::vector;

#include <string>
using std::string;

#include <map>
using std::map;

namespace stats{


  map<string, double>* predict();

  class Data;
  class Model;

};
#endif

