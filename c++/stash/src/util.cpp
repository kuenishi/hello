#include <util.h>

/**
 * $Id$
 **/

vector<string>*  split_string(const string& splittee, char splitter){
  if( splittee.empty() )throw std::exception();

  std::vector<string>*  ret = new std::vector<string>();
  std::string tmp;
  std::string::const_iterator its = splittee.begin();
  
  for(;its not_eq splittee.end(); ++its){
    if( *its not_eq splitter )
      tmp.push_back(*its);
    else if(tmp.empty())
      continue;
    else{
      ret->push_back(tmp);
      tmp = "";
    }
  }
  if( not tmp.empty() )
    ret->push_back(tmp);
  //conversion from 
  //‘__gnu_cxx::__normal_iterator<const char*, std::basic_string<char, std::char_traits<char>, std::allocator<char> > >’
  // to non-scalar type
  //‘__gnu_cxx::__normal_iterator<char*, std::basic_string<char, std::char_traits<char>, std::allocator<char> > >’ requested

  return ret;
}

