#ifndef UTIL_H
#define UTIL_H

/**
 * $Id$
 **/
#include <string>
#include <vector>
#include <exception>

using std::string;
using std::vector;

vector<string>*  split_string(const string&, char =' ');


#endif
