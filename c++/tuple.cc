#include <tuple>
#include <iostream>

int main(){
  std::tuple<int,int> i,j;
  i = std::make_tuple<int,int>(1,2);
  j = std::make_tuple<int,int>(2,-2);
  //std::cout << i << std::endl;
  std::cout << std::get<0>(i) << std::endl;
  std::cout << std::get<1>(i) << std::endl;
  std::cout << (i==j) << std::endl;
  std::cout << (i<j) << std::endl;
}
