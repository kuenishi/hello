// from http://code.google.com/apis/v8/get_started.html
#include <v8.h>
#include <iostream>
#include <sstream>
#include <stdexcept>
using namespace std;
using namespace v8;

Local<Function> create_func(Persistent<Object>& global,
                           const char* code, const char* name){
  // cout << name << endl;
  // cout << code << endl;
  Handle<Script> s = Script::Compile(String::New(code), String::New(name));
  Handle<Value> fun_obj = s->Run();
  return Function::Cast(*(global->Get(String::New(name))));
}

Handle<Value> make_obj(const char* str){
  Handle<String> src = String::New(str);
  try{
    Handle<Script> script = Script::Compile(src);
    cout << "asdfafdsf" << endl;
    return script->Run();
  }catch(...){
    cout << "asdfafdsf" << endl;
    return v8::Undefined();
  }
}

void call_and_print(Persistent<Object>& global,
		    const char* name, const char* code,
		    int args, Handle<Value>* argv){
  stringstream code_;
  code_ << name << " = " << code;
  Local<Value> ret = create_func(global, code_.str().c_str(), name);
  Persistent<Value> f = Persistent<Value>::New(ret);
  if(! f->IsFunction()) throw std::runtime_error("not a function");
  Handle<Value> result = ((Function*)(*f))->Call(global, args, argv);
  String::AsciiValue ascii(result);
  printf("%s\n", *ascii);
}

int main(int argc, char* argv[]) {
  // Create a stack-allocated handle scope.
  HandleScope handle_scope;

  // Create a new context.
  Persistent<Context> context = Context::New();//NULL, global);
  Persistent<Object> global = Persistent<Object>::New(context->Global());
  
  // Enter the created context for compiling and
  // running the hello world script. 
  Context::Scope context_scope(context);

  // Create a string containing the JavaScript source code.
  //  Handle<String> source = String::New("'Hello' + ', World!'");
  // ("[32, 234, 23/4, {asfd:\"asdf\"}, \"hello\"];");
  //  ("{ someatom: 23, anotheratom: 3.3 };");

  Handle<Value> v0[1];
  v0[0] = make_obj("2345");
  //  global->Set("v0", 
  call_and_print(global, "fun0", "function(i){ return i+1; }", 1, v0);
  call_and_print(global, "fun1", "function(i){ return i-1; }", 1, v0);

  v0[0] = make_obj(" { asdf: 234, afsdasdf: [23,3,3,3] } ");
  call_and_print(global, "fun2", "function(i){ return i['asdf']; }", 1, v0);

  // Dispose the persistent context.
  context.Dispose();
  return 0;
}


// query language
// push_back(Key, Value)
// pop_front(Key)
// single_query(Key, Code)
// Code: (map :: a -> b, reduce :: b -> b -> b)
// multi_query(Key0, Key1, Code)
// Code: (map0 :: a -> b -> c, ...)

