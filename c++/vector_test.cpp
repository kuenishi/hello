#include <iostream>
#include <vector>
#include <string>

using namespace std;

template <class T>
void p( const vector<T>& t){
  typename vector<T>::const_iterator it = t.begin();
  for( ; it not_eq t.end() ; ++it ){
    cout << *it << " ";
  }
  cout << endl;
}

int main(){
  vector<string> v(10, "hoge");
  vector<string> u(3, "huga");
  p(v);
  p(u);
  copy( u.begin() , u.end() , back_inserter( v ) );
  p(v);

}
