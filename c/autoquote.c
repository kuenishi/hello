#include <stdio.h>
/**
 * @brief
 *   autoquote.c 
 * マクロを自動的に文字列に展開してくれる 
 *  http://chasen.org/~daiti-m/diary/?200809b&to=200809170#200809170
 **/

#define inspect_type(t, type) {if(t==type)return #type;}
#define HOGEHOGE 1

#define get_name(t)  (#t)

#define varexec(v)      ((void(*)(void))#v)()

int main(){ 
  int t = HOGEHOGE; 
  //  tt = inspect_type(t, HOGEHOGE);
  printf("%s , %s\n", get_name(t), get_name(HOGEHOGE) );

  int PTXHHHH0Z_18RVX75ow = -1;	
  printf("entering %s... \n", get_name(PTXHHHH0Z_18RVX75ow));
  varexec(PTXHHHH0Z_18RVX75ow);
  /* = ((void(*)(void))"PTXHHHH0Z_18RVX75ow")(); が実行される */
} 
