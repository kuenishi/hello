#include <stdio.h>
#include <db.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>

//http://www.oracle.com/technology/documentation/berkeley-db/db/gsg/C/DB.html#DBOpen

struct wc_pair_ {
  char * word;
  int count;
};

typedef struct wc_pair_ wc_pair;

int free_wc_pair(wc_pair* p){
  free( p->word );
  free( p );
  return 0;
}

int usage(){
  printf( "usage: $ ./a.out <infile>\n" );
  exit(0);
}

static const int SYNC_PER_TXN = 1000;

int main(int args, char ** argv){
  DB *dbp;           /* DB structure handle */
  u_int32_t flags;   /* database open flags */
  int ret;           /* function return value */

}
