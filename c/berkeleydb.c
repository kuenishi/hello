#include <stdio.h>
#include <db.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>

//http://www.oracle.com/technology/documentation/berkeley-db/db/gsg/C/DB.html#DBOpen

struct wc_pair_ {
  char * word;
  int count;
};

typedef struct wc_pair_ wc_pair;

int free_wc_pair(wc_pair* p){
  free( p->word );
  free( p );
  return 0;
}

wc_pair * parse_line_to_wc_pair( char* line ){
  int wlen ;
  int nlen;
  wc_pair * ret = (wc_pair*)malloc(sizeof(wc_pair));

  wlen = index(line, ' ') - line;
  char * np = rindex(line, ' ');
  nlen = line + strlen(line) - np -1 ;

  ret->word = (char*)malloc(wlen * sizeof(char));
  char n[nlen+1] ;
  strncpy( ret->word, line, wlen );
  strncpy( n, np,  nlen );
  ret->word[wlen]='\0';
  n[nlen]='\0';
  ret->count = atoi(n);
  return ret;
}
/*
DBT get_key( wc_pair* wcp ){
  DBT key;
  memset(&key, 0, sizeof(DBT));
  key.data = wcp->word;
  key.size = strlen( wcp->word ) +1;
  return key;
}
DBT get_value( wc_pair* wcp ){
  DBT value;
  memset(&value, 0, sizeof(DBT));
  value.data = & wcp->count;
  value.size = sizeof(int);
  return value;
  }*/

int usage(){
  printf( "usage: $ ./a.out <infile>\n" );
  exit(0);
}

static const int SYNC_PER_TXN = 1000;

int main(int args, char ** argv){
  DB *dbp;           /* DB structure handle */
  u_int32_t flags;   /* database open flags */
  int ret;           /* function return value */

  char *description = "Grocery bill.";
  DBT key, data;

  if(args < 2)
    usage();

  FILE * infile = fopen( argv[1], "r" );
  char*  buf;
  ssize_t read;
  size_t len=0;
  int n = 0;
  wc_pair * wcp;
  
  /* Initialize the structure. This
   * database is not opened in an environment, 
   * so the environment pointer is NULL. */
  ret = db_create(&dbp, NULL, 0);
  if (ret != 0) {
    /* Error handling goes here */
  }
  
  /* Database open flags */
  flags = DB_CREATE;    /* If the database does not exist, create it.*/
  
  /* open the database */
  ret = dbp->open(dbp,        /* DB structure pointer */
		  NULL,       /* Transaction pointer */
		  "my_db.db", /* On-disk file that holds the database. */
		  NULL,       /* Optional logical database name */
		  DB_BTREE,   /* Database access method */
		  flags,      /* Open flags */
		  0);         /* File mode (using defaults) */
  if (ret != 0) {
    exit(1);                  /* Error handling goes here */
  }
  struct timeval start, end;
  gettimeofday(&start, NULL);

  while( (read = getline( &buf, &len, infile )) >= 0 ){
    ++n;
    wcp = parse_line_to_wc_pair(buf);
    memset(&key, 0, sizeof(DBT));
    memset(&data, 0, sizeof(DBT));

    /* Zero out the DBTs before using them. */
    key.data = wcp->word;
    key.size = strlen(wcp->word) + 1;
    data.data = (void*)&wcp->count ;
    data.size = sizeof(int);
    ret = dbp->put(dbp, NULL, &key, &data, DB_NOOVERWRITE);
    if (ret == DB_KEYEXIST) {
      dbp->err(dbp, ret, "Put failed: key '%s'", key.data);
      printf( "'%s' \t %d\n", wcp->word, wcp->count ); 
    }else if( n % SYNC_PER_TXN == 0 ){
      dbp->sync(dbp, 0);
    }
    free_wc_pair(wcp);
    free(buf);
    buf = NULL;
  }
  gettimeofday(&end, NULL);
  double t = (end.tv_sec - start.tv_sec)+ ((double)(end.tv_usec - start.tv_usec))/1000000.0;
  printf("-----\t %f seconds - %f tps - %d datas to process\t-----\n", t , n/t, n);
  
  if( dbp != NULL ){
    dbp->close(dbp, 0 );
  }

}
