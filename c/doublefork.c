#include <stdio.h>
#include <unistd.h>

#include <sys/types.h>

int write_file(const char* filename){
  FILE* fp = fopen(filename, "a+");
  fwrite("hoge\n", sizeof(char), 5, fp);
  fclose(fp);
}

int main(){
  printf(__FILE__ ": grandparent: %d\n", getpid());
  
  daemon(0,0);

  int pid = fork();
  if(pid > 0){ //parent
    printf(__FILE__ ": grandparent: %d\n", getpid());
  }else if(pid == 0){
    printf(__FILE__ ": child: %d\n", getpid());
    int pid2 = fork();
    if( pid2 > 0 ){
      printf(__FILE__ ": child: %d\n", getpid());
      write_file("/tmp/child");
    }else if(pid == 0){
      printf(__FILE__ ": grandchild: %d\n", getpid());
      write_file("/tmp/grandchild");
      pause();
    }else{
      return -2;
    }
  }else{
    return -1;
  }
  
  printf(__FILE__ ": grandparent - all done: %d\n", getpid());
}
