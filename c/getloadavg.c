#include <stdio.h>
#include <stdlib.h>

#define N 3

int main(){
  double loadavg[N];
  int nelem=N;

  int nproc = getloadavg(loadavg, nelem);
  int i;
  for(i=0; i<nproc; ++i){
    printf("%f\n", loadavg[i]);
  }
}
