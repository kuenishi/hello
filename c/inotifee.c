#include <stdio.h>
#include <sys/inotify.h>
#include <stdlib.h>
#include <unistd.h>

// $Id: inotifee.c 30 2008-02-23 14:26:31Z kuenishi $
// inotify test.


typedef struct {
  struct inotify_event* ievents;
  int num_events;
} inotify_events;

typedef struct {
  int mask;
  const char* description;
} inotify_case;

int whats_this_case( int c ){
 //         ビット             説明
  inotify_case cases[] = {
    {IN_ACCESS,         "ファイルがアクセス (read) された。(*)"},
    {IN_ATTRIB,         "メタデータが変更された (許可、タイムスタンプ、拡張属性など)。(*)"},
    {IN_CLOSE_WRITE,    "書き込みのためにオープンされたファイルがクローズされた。(*)"},
    {IN_CLOSE_NOWRITE,  "書き込み以外のためにオープンされたファイルがクローズされた。(*)"},
    {IN_CREATE       ,  "監視対象ディレクトリ内でファイルやディレクトリが作成された。(*)"},
    {IN_DELETE    ,     "監視対象ディレクトリ内でファイルやディレクトリが削除された。(*)"},
    {IN_DELETE_SELF,    "監視対象のディレクトリまたはファイル自身が削除された。"},
    {IN_MODIFY,         "ファイルが修正された。(*)"},
    {IN_MOVE_SELF,      "監視対象のディレクトリまたはファイル自身が移動された。"},
    {IN_MOVED_FROM,     "ファイルが監視対象ディレクトリ外へ移動された。(*)"},
    {IN_MOVED_TO,       "ファイルが監視対象ディレクトリ内へ移動された。(*)"},
    {IN_OPEN,           "ファイルがオープンされた。(*)"},
    //       デ ィレクトリを監視する場合、上記でアスタリスク (*) を付けたイベントは、そのディレクトリ内のファイルに対して発生する。このとき inotify_event 構造体で返される name フィールドは、ディレクトリ内のファイル名を 表す。
    { IN_ALL_EVENTS,    "IN_ALL_EVENTS マクロは上記のイベント全てのマスクとして定義される。このマクロは inotify_add_watch(2) を呼 び出すときの mask 引き数として使える。"},
    //       さらに 2 つの便利なマクロがある。
    { IN_MOVE,          "IN_MOVE  は  IN_MOVED_FROM|IN_MOVED_TO   と 等 し く 、"},
    { IN_CLOSE,         "IN_CLOSE はIN_CLOSE_WRITE|IN_CLOSE_NOWRITE と等しい。"}
  };
  inotify_case * cp;
  for( cp = cases; cp->mask != IN_CLOSE; ++cp ){
    if( c == cp->mask ){
      printf( "%s\n", cp->description );
      return c;
    }
  }
  printf( "some other unknown or combination of known cases(implement this case).\n" );
  return c;
}

int main(int args, char** argv){
  char* target;
  if( args >1 )
    target = argv[1];
  else
    target = "tmp.txt";

  int ifd = inotify_init();
  if( inotify_add_watch( ifd, target, IN_ALL_EVENTS ) < 0 ){
    //sth wrong with add watch
    perror("can't add_watch ifd.");
    exit(1);
  }
  
  struct inotify_event ievent;
  int t = -120;
  printf( "waiting for file access to %s...\n", target );
  while( (t = read( ifd, (void*)&ievent, sizeof(struct inotify_event)+1 )) < 1 ){
    ;// printf("%d\n", t);
  }
  whats_this_case( ievent.mask );
  if( ievent.name == NULL )
    puts("name is null");
  printf( ">>>%s\n", ievent.name );
  close(ifd);

  return 0;
}
