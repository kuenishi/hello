#include <stdio.h>
#include <setjmp.h>

#define HERE(x)   printf("%s l%d:\t%s \n", __FILE__, __LINE__, __func__ )
#define HERED(x)   printf("%s l%d:\t%s - %d \n", __FILE__, __LINE__, __func__, x )

int jumpppp( jmp_buf * penv, int i ){
  HERED(i);
  longjmp(*penv, i+1);
  HERE();
}

int main(int args, char ** argv){
  jmp_buf env;
  HERE();
  int ret=setjmp(env);
  if( ret < 25 )
    jumpppp( &env, ret );
  HERED(ret);
  
  return 0;
}
