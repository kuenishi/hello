#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define KB (1024)
#define MB (1024*KB)
#define GB (1024*MB)

int main(int args, char ** argv){

  char * p, * q;
  unsigned int N = atoi(argv[1])*MB;
  p = malloc( N );
  q = malloc( N );
  memset(p, 0, N);
  memset(q, 0, N);
  printf("%d Bytes alloced.\n", N ); 
  free(p);
  free(q);

}
