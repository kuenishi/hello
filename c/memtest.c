#include <stdio.h>
#include <stdlib.h>
#include <string.h>


#define MEMSIZE ( 1024 * 1024 * 1024 )
#define MULTIPLIER  (5)

int main(void){
  char * ptr, * start;
  start = (char *)malloc( sizeof(char) * MEMSIZE * MULTIPLIER);
  unsigned char c = '\0';
  for( ptr = start; ptr - start < MEMSIZE * MULTIPLIER ; ++ptr ){
    *ptr = c++;
  }
  printf( "mem alloced.\n");
  getchar();
  free( ptr );

}
