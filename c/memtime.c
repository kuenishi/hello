#include <time.h>
#include <sys/times.h>
#include <sys/time.h>
#include <stdio.h>

#ifndef __APPLE__
#include <malloc.h>
#else
#include <stdlib.h> //for malloc in mac
#endif

#define WHEREAMI  puts(__func__);

//http://kzk9.net/column/time.html
int use_time( int f(), int n){
    clock_t t1;
    clock_t t2;
    double c = (double)CLOCKS_PER_SEC;
    WHEREAMI;
    printf("CLOCKS_PER_SEC = %f\n", c);
    
    t1 = clock();
    f(n);
    t2 = clock();
    printf("time = %10.30f\n", ((double)t2 - t1)/c);
}
double gettimeofday_sec()
{
    struct timeval tv;
    gettimeofday(&tv, NULL);
    return tv.tv_sec + (double)tv.tv_usec*1e-6;
}

int use_gettimeoftoday( int f(), int n)
{
    double t1,t2;

    WHEREAMI;
    t1 = gettimeofday_sec();
    f(n);
    t2 = gettimeofday_sec();
    printf("time = %10.30f\n", t2 - t1);
}

#include <sys/resource.h>
double getrusage_sec()
{
    struct rusage t;
    struct timeval tv;
    getrusage(RUSAGE_SELF, &t);
    tv = t.ru_utime;
    return tv.tv_sec + (double)tv.tv_usec*1e-6;
}

int use_gettrusage(int f(), int n)
{
    double t1,t2;
    WHEREAMI

    t1 = getrusage_sec();
    f(n);
    t2 = getrusage_sec();
    printf("time = %10.70f\n", t2 - t1);
}
#include <unistd.h>
clock_t times_clock()
{
    struct tms t;
    return times(&t);
}

int use_times(int f(), int n){
    clock_t t1;
    clock_t t2;
    double c = (double)sysconf(_SC_CLK_TCK);
    WHEREAMI
    printf("sysconf(_SC_CLK_TCK) = %f\n", c);

    t1 = times_clock();
    f(n);
    t2 = times_clock();
    printf("time = %10.30f\n", ((double)t2 - t1)/c);
}

int hoge(int n){
  int x = 1;
  int i;
  for( i = 1; i < n; ++i)
    x *= i;
}

int main(){
  int n = 10000000;
  use_time(hoge, n);
  use_gettimeoftoday(hoge, n);
  use_gettrusage(hoge, n);
  use_times(hoge, n);
  
}
