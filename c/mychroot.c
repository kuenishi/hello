#include <unistd.h>
#include <stdio.h>

#define PROGNAME "mychroot"

int usage(){
  fprintf(stderr, "usage: # " PROGNAME " <dir>\n");
}

int main(int args, char ** argv){
  if( args < 2 ){
    usage();
    return -1;
  }
  int r;
  if( (r=chroot(argv[1])) == 0 ){
    printf("got chroot! \n");
  }else{
    perror("chroot(2)");
  };
  return r;
}
