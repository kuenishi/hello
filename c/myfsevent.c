#include <CoreServices/CoreServices.h>
#include <pthread.h>
#include <stdio.h>


/*
$ gcc myfsevent.c -framework CoreServices

http://developer.apple.com/mac/library/documentation/Darwin/Conceptual/FSEvents_ProgGuide/UsingtheFSEventsFramework/UsingtheFSEventsFramework.html#//apple_ref/doc/uid/TP40005289-CH4-SW4
http://developer.apple.com/mac/library/documentation/Darwin/Reference/FSEvents_Ref/FSEvents_h/index.html
http://developer.apple.com/mac/library/samplecode/Watcher/Listings/Watcher_c.html
*/

FSEventStreamContext context =  {
  .version = 0,
  .info = NULL,
  .retain = NULL,
  .release = NULL,
  .copyDescription = NULL };

int flag = 0;

void mycallback(ConstFSEventStreamRef streamRef,
		void *callbackInfo,
		size_t numEvents,
		void *eventPaths,
		const FSEventStreamEventFlags eventFlags[],
		const FSEventStreamEventId eventIds[]){
  int i;
  char **paths = eventPaths;
  
  printf("Callback called(%p): %d events\n", pthread_self(), (int)numEvents);
  for (i=0; i<numEvents; i++) {
    int count;
    /* flags are unsigned long, IDs are uint64_t */
    printf("eventId(%d), EventFlags(%x) in %s\n",
	   (int)eventIds[i], (int)eventFlags[i], paths[i] ); //, eventFlags[i]);

    /** if you want to find what is changed here, 
	just keep older version, previous state of
	the directory. this API doesn't show ...
	such as what file is added, removed, written. **/
  }
}

void *loopy(void* arg){
  FSEventStreamRef stream = (FSEventStreamRef)(arg);
  printf("loopy start\n");
  FSEventStreamScheduleWithRunLoop(stream, CFRunLoopGetCurrent(), kCFRunLoopDefaultMode);
  while(!flag){
    //    puts(".");
    CFRunLoopRun();
  }
  printf("loopy end\n");
}

int main(int args, char ** argv){
/* Define variables and create a CFArray object containing CFString objects containing paths to watch.    */
  
  CFStringRef mypath = CFSTR( "/" );
  CFArrayRef pathsToWatch = CFArrayCreate(NULL, (const void **)&mypath, 1, NULL);
  void *callbackInfo = NULL; // could put stream-specific data here.
  FSEventStreamRef stream;
  CFAbsoluteTime latency = 1.0; /* Latency in seconds */
  bool r;
  int i;
  CFRunLoopRef loop;
  pthread_t pid;

  /* Create the stream, passing in a callback */
  stream = FSEventStreamCreate(NULL, mycallback, callbackInfo, pathsToWatch,
			       kFSEventStreamEventIdSinceNow, /* Or a previous event ID */
			       latency,
			       kFSEventStreamCreateFlagNone /* Flags explained in reference */   );


  FSEventStreamShow(stream);
  printf("starting to watch %s in thread(%p) \n", "/", pthread_self());

  FSEventStreamScheduleWithRunLoop(stream, CFRunLoopGetCurrent(), kCFRunLoopDefaultMode);
  r = FSEventStreamStart(stream);

  CFRunLoopRun();

  flag=1;

  loop = CFRunLoopGetCurrent();
  FSEventStreamStop(stream);
  FSEventStreamUnscheduleFromRunLoop( stream, loop, kCFRunLoopDefaultMode);
  FSEventStreamInvalidate(stream);
  FSEventStreamRelease(stream);
}
