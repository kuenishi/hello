#include <stdio.h>
#include <regex.h>

// see http://blog.majide.com/2009/03/c-regex-sample-code/
int main(int args, char ** argv){
  regex_t reg;
  const char * regex = "^hogehage$";
  int i;
  size_t nmatch = 5;
  regmatch_t pmatch[5];

  regcomp(&reg, regex, REG_EXTENDED|REG_NEWLINE);
  printf("match for '%s':\n", regex);
  for(i=1; i<args; ++i){
    printf("'%s': ", argv[i]);
    if(regexec( &reg, argv[i], nmatch, pmatch, 0) != 0){
      printf("doesn't match: %s\n", argv[i]);
    }else{
      printf("matched      : ");
      int j, k;
      for(j=0;j<nmatch;++j){
	if (pmatch[j].rm_so >= 0 && pmatch[j].rm_eo >= 0) {
	  printf("from %d to %d\n", (int)pmatch[j].rm_so, (int)pmatch[j].rm_eo);
	  //	  for (k = pmatch[j].rm_so ; k < pmatch[j].rm_eo; k++) {
	  //	    putchar(argv[i][k]);
	}
      }
    }
  }
  regfree(&reg);
}
