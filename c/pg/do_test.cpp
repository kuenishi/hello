//see   http://www.enterprisedb.com/docs/jp/8.3/server/libpq-example.html
// $ gcc -lpq -I /opt/local/include

#include "trans.h"
#include "util.h"

int main(int args, char ** argv){
  int maxN = 65536;
  int M = 0;
  int i, j;
  int n = 0;
  int id;
  my::rand r;
  int N;


  struct timeval start, end;
  char key[] = "123456789abcdef";
  char value[] = "123456789abcdef"
    "123456789abcdef";
  
  creat_conn();

  for(  N = 1 ; N <= maxN; N *= 10 ){
    int ids[N];
    srand(start.tv_usec * start.tv_sec);

    //    update_test(s);
    
    M = get_max_id();
    //    printf("%d\n", M );
    if( M > 0 ){
      for( i = 0 ; i < N ; ++i ){
        ids[i] = rand() % M;
      }
      //qsort( ids , N, sizeof(int) , (int (*)(const void*, const void*))comp);
      //printf("0 - %d, %d - %d\n",  ids[0], N/2, ids[N/2] );
    }
    gettimeofday(&start, NULL);
    char * k, * v;
    //select_test("");
    for( i=0; i<N; ++i ){ //
      begin_transaction();
        //id = ids[i];
      //    id = i;
 
      for( j = 0; j < 30; ++j){
	doc * d = select_doc(ids[i]);
	//select_docid(r.str(64));
	//printf("%d/%d \t...%d\n", i, N, id );
	
	
	update_test( ids[i], r.str(65) ); r.c();
	//      simple_insert(  r.str(66),  ); r.c();
	if( d != NULL ){
	  simple_insert(  r.str(66), d->key ); r.c();
	  DEL_DOC(d);
	}
      }
      //
      //k = r.str(32);
      //v = r.str(64);

      //id = 5;
      //   insert_tag(ids[i]); 
      
      //     free(k); free(v);
      //  for( i=0; i<N; ++i )  update_test(i, "hoge");
      // insert_with( r.str(32), r.str(64) ); r.c();
      commit_transaction();
    }
    
    
    gettimeofday(&end, NULL);
    double t = (end.tv_sec - start.tv_sec)+ ((double)(end.tv_usec - start.tv_usec))/1000000.0;
    // t/N tps achieved.
    printf( "%f tps for %d datas in %f secs; %f ms for each data.\n", N/t, N, t, 1000*t/N );

  }
  fin_conn();
  return 0;
}

