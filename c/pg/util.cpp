#ifdef HAVE_CONFIG_H
#include <config.h>
#endif
#define SIZEOF_UNSIGNED_INT (4)
#include "util.h"
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <cassert>

using namespace my;
using namespace std;

string my::getfilename(const string& fpath){
  string ret = fpath.substr(fpath.find_last_of("/")+1, string::npos );
  return ret;
}
string my::getfilename(const char * fpath){
  return my::getfilename(string(fpath));
}

unsigned int my::getfilesize(const char * fpath){
  struct stat s;
  stat( fpath, &s ); // see stat(2)
  return s.st_size;
}

char* my::uint2char(const size_t & l){
  char* lch=(char*)malloc(sizeof(char)*SIZEOF_UNSIGNED_INT);
  memcpy( lch, &l, SIZEOF_UNSIGNED_INT );
  return lch;
}
unsigned int my::char2uint(const char* s){
  unsigned int ret;
  memcpy( &ret, s, SIZEOF_UNSIGNED_INT );
  return ret;
}

bool my::have_file(const string& fpath){
  struct stat s;
  int ret = stat( fpath.c_str(), &s ); // see stat(2)
  return ( ret == 0 );
}
string my::green(const char* s){
  //  string ret = "\\[\\033[1;30m\\]";
  string ret = "\033[32m";
  return ret+s+"\033[m";
}
string my::red(const char* s){
  //  string ret = "\\[\\033[1;30m\\]";
  string ret = "\033[34m";
  return ret+s+"\033[m";
}
//   http://www.linux.or.jp/JF/JFdocs/Bash-Prompt-HOWTO-5.html
// Black       0;30     Dark Gray     1;30
// Blue        0;34     Light Blue    1;34
// Green       0;32     Light Green   1;32
// Cyan        0;36     Light Cyan    1;36
// Red         0;31     Light Red     1;31
// Purple      0;35     Light Purple  1;35
// Brown       0;33     Yellow        1;33
// Light Gray  0;37     White         1;37

// string my::random_filename(){
//   char buf[]="/tmp/test.XXXXXX";
  
//   return string(buf);
// }


my::rand::rand():src_fd_( open( "/dev/urandom", O_RDONLY ) ),l_(strlen(my::letters)){
  assert( src_fd_ > 0 ) ;
}
my::rand::~rand(){
  close(src_fd_);
}
char* my::rand::str( unsigned int w){
  int i;
  unsigned char buf[w+1];
  char * ret = (char*)malloc(sizeof(char)*(w+1) );
  int r = read( src_fd_, buf, w );
  if( r < 0 ) perror( "SthWrong" );
  ret[w] = '\0';
  unsigned char * p;
  char * q;
  //  char tmp; 
  for(i=0,p=buf,q=ret;i<w;++i,++p,++q){   //p = buf; *p != '\0'; ++p ){
    /** caution!!!
     * when *p == -128, *p *= -1 does not changes the value of *p
     * because char type can't express +128.
     * -128 is 0x80, because 0xFF - 0x7F = 0x80.
     * 0x7F is 127, but 0x80 is -128. (0x81 is -127).
     **/
    *p %= l_;
    assert( 0 <= *p && *p <= l_ );
    *q = my::letters[ *p ];
  }
  tmp_ = ret;
  return ret;
}

int my::dd(my::FD from, my::FD to, unsigned int length, const int BUF_SIZE){
  const int BUF_SIZE_ = ( BUF_SIZE > length )?length:BUF_SIZE;
    
  char buf[BUF_SIZE_];
  //printf("l=%d, to=%d\n", length, to);
  int bytes_to_read = length;
  int bytes_to_write = length;
  int bytes_read = 0;
  int bytes_written= 0;
  int r = 0;
  int w = 0;
  int bytes_to_write_tmp, bytes_written_tmp;
  
  //printf("read=%d, written=%d, %d %d\n", bytes_read, bytes_written, bytes_to_read, bytes_to_write);
  while( bytes_read < bytes_to_read and bytes_written < bytes_to_write){
    //    putchar
    r = read( from, buf, BUF_SIZE_ );
    bytes_read += r;
    bytes_written_tmp = 0;
    bytes_to_write_tmp = r;
    //printf("%d bytes read from read\n", r);
    if( r < 0 ){
      printf("-read=%d, written=%d\n", bytes_read, bytes_written);
      break;
    }
    while( bytes_written_tmp < bytes_to_write_tmp ){
      w = write( to, buf + bytes_written_tmp , bytes_to_write_tmp - bytes_written_tmp );
      //printf("%d bytes written by write\n", w);
      bytes_written_tmp += w;
      bytes_written += w;
      if( w < 0 ){
	printf("--read=%d, written=%d\n", bytes_read, bytes_written);
	break;
      }
    }
    memset( buf, 0, BUF_SIZE_);
  }
  //printf("read=%d, written=%d\n", bytes_read, bytes_written);
  return bytes_written;
}

char* my::dd(FD from, unsigned int length ){
  //  char buf[l];
  char * ret = (char*)malloc(sizeof(char)*(length+1)) ;
  int bytes_read = 0;
  int bytes_to_read = length;
  //  string mesg;
  //  memset(buf, 0, l );
  //データを一度全部受け取る
  while( bytes_read < bytes_to_read ){
    int r = read( from, (void*)( ret + bytes_read), bytes_to_read - bytes_read );
    if( r < 0 ){
      perror( __func__ );
    }
    bytes_read += r;
  }
  ret[length] = '\0';
  return ret;
}
