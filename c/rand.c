#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <assert.h>
#include <string.h>

//char table which you want to use in output string
static const char letters[] = "0123456789"
  "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
  "abcdefghijklmnopqrstuvwxyz"
  "_-";

//get random char* string by using...
// 
//*1. /dev/urandom    //better rand and fast and non-deterministic
// 2. /dev/random     //best rand but slow and non-deterministic
// 3. rand(3)         //worst rand and deterministic and not thread-safe.
// 4. random(3)       //worse rand and deterministic

char* rand_str( unsigned int w){
  int l = strlen(letters);
  int i;
  int src_fd = open( "/dev/urandom", O_RDONLY );
  if( src_fd < 0 )return "";
  //char buf[w+1];
  unsigned char * buf = (unsigned char*)malloc(sizeof(unsigned char)*(w+1) );
  int ret = read( src_fd, buf, w );
  if( ret < 0 ) perror( "SthWrong" );
  buf[w] = '\0';
  unsigned char * p;
  char * q;
  char tmp; 
  char * r = (char*)malloc(sizeof(char)*(w+1) );
  memset(r, 0, w+1);
  for(i=0,p=buf,q=r;i<w;++i,++p,++q){   //p = buf; *p != '\0'; ++p ){
    /** caution!!!
     * when *p == -128, *p *= -1 does not changes the value of *p
     * because char type can't express +128.
     * -128 is 0x80, because 0xFF - 0x7F = 0x80.
     * 0x7F is 127, but 0x80 is -128. (0x81 is -127).
     **/
    *p %= l;
    assert( *p <= l );
    *q = letters[ *p ];
  }
  return r;
}

int main(){
  int i;
  char * s = rand_str(32);
  printf("%d: %s\n",  (int)strlen(s), s );
  assert( strlen(s) == 32 );
}
