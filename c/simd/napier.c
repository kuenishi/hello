#include <stdio.h>
#include <sys/time.h>

// -mfpmath=sse -msse4
//#include <emmintrin.h>// SSE2
#include <pmmintrin.h>// SSE3
//#include <smmintrin.h>// SSE4

#define E (2.718281828459045235360287471352)

/**

  A program that compares normal double-float computation and
  SSE-enabled computation of Napier number. On my core i7:

  $ cc napier.c -mfpmath=sse -msse4 && ./a.out 
  (0 msec) const:         2.71828182845904509080
  (1894.87msec) f0:       2.71828182845904553488
  (1224.71msec) f1:       2.71828182845904553488

 **/

// compute Napier number naturally
double f0(unsigned long long n){
  double sum = 1.0;
  double fact = 1.0;
  unsigned long long i;
  for(i=2; i<=n; ++i){
    sum += 1.0 / fact;
    fact *= i;
  }
  return sum;
}

double f1(unsigned long long n){
  double d[2] = {1.0, 1.0}; // even, odd
  double f[2] = {2.0, 6.0}; // 2n!, (2n+1)!
  double ns[2] = {3.0, 4.0}; // n, n+1
  double one[2] = {1.0, 1.0}; // 1, 1
  __m128d sums = _mm_load_pd(d);
  __m128d fact = _mm_load_pd(f);
  __m128d is = _mm_load_pd(ns);
  __m128d ones = _mm_load_pd(one);
  unsigned long long i;
  for(i=1; i<=n; i+=2){
    sums = _mm_add_pd(sums, _mm_div_pd(ones, fact));
    fact = _mm_mul_pd(fact, is);
    is = _mm_add_pd(is, ones);
    fact = _mm_mul_pd(fact, is);
    is = _mm_add_pd(is, ones);
  }
  _mm_store_pd(d, sums);
  return d[0] + d[1];
}

double timer(double (*fun)(unsigned long long), unsigned long long n){
  struct timeval start;
  struct timeval end;
  gettimeofday(&start, NULL);
  double sum = fun(n);
  gettimeofday(&end, NULL);
  double t
    = (end.tv_sec - start.tv_sec) * 1000.0
    + (end.tv_usec - start.tv_usec) / 1000.0;
  printf("(%.2fmsec) ", t);
  return sum;
}

int main(){

  printf("(0 msec) const: \t%.20f\n", E);
  printf("f0: \t%.20f\n", timer(f0, 0xFFFFFFF));
  printf("f1: \t%.20f\n", timer(f1, 0xFFFFFFF));
  
  return 0;
}
