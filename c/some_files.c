#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <strings.h>
#include <string.h>

int usage(){
  printf( "usage\n" );
  exit(1);
}


struct wc_pair_ {
  char * word;
  int count;
};

typedef struct wc_pair_ wc_pair;

int free_wc_pair(wc_pair* p){
  free( p->word );
  free( p );
  return 0;
}

wc_pair * parse_line_to_wc_pair( char* line ){
  int wlen ;
  int nlen;
  wc_pair * ret = (wc_pair*)malloc(sizeof(wc_pair));

  wlen = index(line, ' ') - line;
  char * np = rindex(line, ' ');
  nlen = line + strlen(line) - np -1 ;

  ret->word = (char*)malloc(wlen * sizeof(char));
  char n[nlen+1] ;
  strncpy( ret->word, line, wlen );
  strncpy( n, np,  nlen );
  ret->word[wlen]='\0';
  n[nlen]='\0';
  ret->count = atoi(n);
  return ret;
}

int main(int args, char ** argv){

  FILE * infile = fopen( argv[1], "r" );
  char*  buf;
  ssize_t read;
  size_t len=0;
  if(args < 2)
    usage();
  int i = 0;
  //  buf = (char*)malloc(sizeof(char)*1024);
  
/*   char c; */
/*   while( (c=fgetc(infile)) != EOF ){ */
/*     putchar(c); */
/*   } */
  while( (read = getline( &buf, &len, infile )) >= 0){ 
    wc_pair * p = parse_line_to_wc_pair(buf) ;
    printf( "%d - %s:%d\n",  i++ , p->word, p->count );
    free_wc_pair(p);
    free(buf); 
    buf = NULL; 
  } 
  if(  buf != NULL ) free(buf); ///getlineが、bufを確保して-1で返ることがある
  fclose(infile);
  return 0;
}
