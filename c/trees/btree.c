#include "btree.h"
#include <string.h>
#include <stdio.h>

data_t * new_data(size_t size_of_data){
  data_t * ret = (data_t*)malloc(sizeof(data_t));
  ret->data = malloc(size_of_data);
  ret->size = size_of_data;
  return ret;
}

int free_data( data_t * d){
  free( d->data );
  free( d);
  return 0;
}

btree_node *  new_btree_node(data_t * initial_key,
			     data_t * initial_val){

  btree_node * ret = (btree_node*)malloc(sizeof(btree_node));
  memset(ret, 0,  sizeof(btree_node) );
  if( initial_key == NULL ){
    ret->key.data = NULL;
  }else{
    ret->key.data = (void*)malloc(initial_key->size);
    memcpy(ret->key.data, initial_key->data, initial_key->size);
    ret->key.size = initial_key->size;
  }

  if( initial_val == NULL ){
    ret->value.data = NULL;
  }else{
    ret->value.data =(void*)malloc(initial_val->size);
    memcpy(ret->value.data, initial_val->data, initial_val->size);
    ret->value.size = initial_val->size;
  }
  return ret;
}

int free_btree_node(btree_node * n, int(*free_data)(void*)){
  if( n == NULL )
    return -1;

  if( n->key.data != NULL ){
    ( free_data == NULL )?
      free( n->key.data ):
      free_data( n->key.data ); //XXX:TODO: ちょっと中途半端
  }
  if( n->value.data != NULL ){
    ( free_data == NULL )?
      free(n->value.data):
      free_data( n->value.data );
  }
  if( n->left != NULL ){
    free_btree_node(n->left, free_data);
  }
  
  if( n->right != NULL )
    free_btree_node(n->right, free_data);

  free(n);
  return 0;
}

btree_node * add_pair(btree_node * n, 
		      data_t * key, data_t * val,
		      int(*cmp)(void* left, void* right)){

  if( n->key.data == NULL){
    n->key.data = malloc(key->size);
    n->key.size = key->size;
    memcpy( n->key.data, key->data, key->size);
    n->value.data = malloc(val->size);
    n->value.size = val->size;
    memcpy( n->value.data, val->data, val->size);
    //   printf( "%d:%d\n", *(int*)key->data, *(int*)val->data );
    //   printf( "%d:%d\n", *(int*)n->key.data, *(int*)n->value.data );
    return n;
  }
  int comparance = cmp( n->key.data, key->data );

/* case the key already exists */
  if( comparance == 0){
    return NULL; //update or ignore?
  }  

  else  if( comparance < 0 ){
    if( n->left == NULL )
      return n->left = new_btree_node(key, val);
    else
      return add_pair( n->left, key, val, cmp );
  }
  else if( comparance > 0 ){
    if( n->right == NULL )
      return n->right = new_btree_node(key, val);
    else
      return add_pair( n->right, key, val, cmp );
  }
  else
    return NULL;
  return NULL;
}

data_t* get_node_value(btree_node * n, data_t * key,
	      int(*cmp)(void* left, void* right) ){
  if( n->key.data == NULL){
    return NULL;
  }
  int comparance = cmp(n->key.data, key->data);
  if( comparance ==  0 ){
    return &n->value;
  }
  else  if( comparance < 0 ){
    return (n->left == NULL)? NULL :
      get_node_value( n->left, key, cmp );
  }
  else if( comparance > 0 ){
    return (n->right == NULL)? NULL :
      get_node_value( n->right, key, cmp );
  }
  else
    return NULL;

}


int print_btree_node(btree_node* n, int depth, 
		     int(*p_node)(data_t * k, data_t * v)){
  int i;
  if( n->left != NULL )
    print_btree_node( n->left, depth + 1, p_node );
  for( i = 0; i<depth; ++i){
    putchar( ' ' );
    putchar( ' ' );
  }
  p_node( & n->key, & n->value );
  if( n->right != NULL )
    print_btree_node( n->right, depth + 1, p_node );

  return 0;
}

//�|�C���^�̕t���ւ�������Ƃ������킩��ɂ���
btree_node * rotate_node_left( btree_node * n ){
  if( n->right == NULL )
    return n;
  btree_node * b = n->right->left;
  btree_node * q = n->right;
  
  n->right = b;
  q->left = n;
  return q;
}
btree_node * rotate_node_right( btree_node * n ){
  if( n->left == NULL )
    return n;

  btree_node * p = n->left;
  btree_node * b = n->left->right;

  n->left = b;
  p->right = n;
  return p;
}

btree * new_btree(int (*compare)(void* l, void* r),
		  int (*print)(data_t* k, data_t* v))
{
  btree * ret = (btree*)malloc(sizeof(btree));
  memset(ret, 0, sizeof(btree));
  ret->root = new_btree_node(NULL, NULL);
  ret->size = 0;
  ret->compare = compare;
  ret->print = print;
  return ret;
}

int free_btree(btree* t){
  free_btree_node(t->root, NULL);
  free(t);
  return 0;
}

int print_btree(btree * t){
  //  btree_node* cur = printee->root;
  print_btree_node(t->root, 0, t->print);
  return 0;
}

data_t * get_value(btree* t, data_t* key){
  return get_node_value( t->root, key, t->compare );
}
int      add_value(btree* t, data_t* key, data_t* value){
  if( add_pair(t->root, key, value, t->compare) >= 0 ){
    return ++(t->size);
  }
  else return -1;
}

int   move_to_root( btree* t, data_t * key){
  btree_node ** stack =
    (btree_node**)malloc( sizeof( btree_node* ) * MAX_TREE_DEPTH ); 

  memset( stack, 0, MAX_TREE_DEPTH);

  btree_node ** stack_top = stack;
  btree_node ** stack_pointer;
  *stack_top = t->root;

  int depth = 0;
  //find it first
  while( 1 ){
    int comparance = t->compare( (*stack_top)->key.data, key->data);
    if( comparance == 0 ){
      break; //normal route in case (1)
    }
    stack_top++;
    depth++;
    if( comparance < 0 ){
      *stack_top = (*(stack_top-1))->left;
    }
    else if( comparance > 0 ){
      *stack_top = (*(stack_top-1))->right;
    }
    
    if( *stack_top == NULL ){ //normal route in case (4)
      return -1;
    }
  }//here we completed stack list of node pointer
  
/*   printf( "%d;adding-> %s\n" , depth, (char*)key->data); 
   print_btree_node( *(stack_top-1), depth-1, t->print ); 
   print_btree_node( *stack_top, depth, t->print ); */
 
  stack_pointer = stack_top;
  while( stack_pointer - stack > 0 ){// while( x is not root ):
    btree_node * x = *( stack_pointer ); //parent
    btree_node * p = *( stack_pointer - 1 ); //parent
    
    if( stack_pointer - stack == 1 ){
      if( p->left == x ){
	t->root = rotate_node_right( p );
      }
      else if( p->right == x ){
	t->root = rotate_node_left( p );
      }
    }
    else
    if ( stack_pointer - stack > 1 ){
      btree_node * g = *( stack_pointer - 2 ); //grand-parent
      //btree_node ** target;
      if( g->left == p ){
	if( p->left == x ){
	  g->left = rotate_node_right( p );
	}
	else if( p->right == x ){
	  g->left = rotate_node_left( p );
	}
	*(stack_pointer -1 ) = g->left;
      }
      else if ( g->right == p ){
	if( p->left == x ){
	  g->right = rotate_node_right( p );
	}
	else if( p->right == x ){
	  g->right = rotate_node_left( p );
	}
	*(stack_pointer -1 ) = g->right;
      }
    }
    //    print_btree_node( *stack, 0, t->print);
    stack_pointer--;
  }
  return 0;
}

