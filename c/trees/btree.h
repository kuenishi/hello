#ifndef BTREE_H_
#define BTREE_H_
#include <stdlib.h>

/**
 *  btree  -  2分木→2分平衡木を作る予定
 *  いろいろまだまだ
 *
 *
 **/

typedef struct data_ data_t;
struct data_ {
  void * data;
  size_t size;
};

data_t * new_data(size_t size_of_data);
int free_data(data_t * d);

/*  core modules */
typedef struct btree_node_ btree_node;
struct btree_node_ {
  btree_node * left;
  btree_node * right;
  data_t key;
  data_t value;
};


btree_node *  new_btree_node(data_t * initial_key,
			     data_t * initial_val);

int           free_btree_node(btree_node* n,
			      int(*free_data)(void*) );

btree_node * add_pair(btree_node * n,  
	     data_t * key, data_t * val,
	     int(*cmp)(void* left, void* right) );

data_t * get_node_value(btree_node * n, data_t * key,
	      int(*cmp)(void* left, void* right) );

int print_btree_node(btree_node* n, int depth, 
		     int(*p_node)(data_t*, data_t*));

//see  http://en.wikipedia.org/wiki/Tree_rotation
btree_node * rotate_node_left( btree_node * n );  //rotate reverse-clockwise
btree_node * rotate_node_right( btree_node * n ); //rotate clockwise

/**  surrounding module
 *  wrapper struct for btree_node
 **/
typedef struct btree_ btree;
struct btree_ {
  btree_node * root;
  int size;
  int (*compare)(void* l, void* r);
  int (*print)( data_t *k, data_t *v);
} ;

btree * new_btree(int (*compare)(void* l, void* r),
		  int (*print)(data_t* k, data_t* v));
int     free_btree(btree* t);
int     print_btree(btree* t);
data_t *get_value(btree* t, data_t * key);
int     add_value(btree* t, data_t * key, data_t * value);

int   move_to_root( btree* t, data_t * key);

#define MAX_TREE_DEPTH (1024)

#endif
