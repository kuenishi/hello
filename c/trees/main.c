#include <stdio.h>
#include "btree.h"
#include <stdlib.h>

#include <string.h>
#include <sys/time.h>
#include "my_util.h"
#include "splay.h"
#include "tests.h"

int comp( void* l, void * r){
  return (*(int*)l - *(int*)r);
}

int my_strcmp( void* l, void* r ){
  return strcmp(  (char*)r, (char*)l);
}

int p( data_t * k, data_t * v ){
  if( v != NULL  &&  k != NULL 
      && v->data != NULL && k->data != NULL)
    printf( "(%s, %d)\n", (char*)k->data, *(int*)v->data );
  return 0;
}



btree * btree_template(char ** leaves, int nLeaves ){
  btree * ret = new_btree(my_strcmp, p );
  int i, tmp;
  for( i=0; i<nLeaves; ++i){
    data_t * pdata, *qdata;

    pdata = new_data( sizeof(char)* (strlen( leaves[i] )+1) );
    memcpy( pdata->data, leaves[i], sizeof(char)*(strlen(leaves[i])+1) );
    qdata = new_data(sizeof(int) );
    tmp = 12;
    memcpy( qdata->data, &tmp, sizeof(int) );
    
    if( add_value( ret, pdata, qdata ) < 0 ){
      printf("insertion failed.\n");
    }
    free_data(pdata);
    free_data(qdata);

  }
  return ret;
}

int known_splay_test(){
  char * tmp[] = {    "ccc",
	       	"bbb", "ddd", 
         "bab", "bcb", "dcd", "dfd",
"baa", "bac", "bca", "bcc", "dcc", "dce", "dfc", "dff"};
  btree * bt = btree_template( tmp, 15);
  print_btree( bt );
  data_t k;

  //trying to read "aa", which will make zig-zig
  char* hoge = "bcb";
  k.data = (void*)hoge;
  k.size = sizeof( hoge);
  splay( bt,  &k );
  puts(" --------------- " );
  print_btree( bt );
  free_btree( bt );
  return 0;
}
int main(int args, char ** argv ){
  int l = 3;
  int N = 0x10000;//65536 = 0x100000

  btree * bt = generate_random_btree(N, l, my_strcmp, p);
  //  print_btree( bt );
  performance_test(bt, N, l);
  splay_performance_test(bt,  N, l);
  //  print_btree( bt );
  //  return known_splay_test();

  free_btree(bt);
}
