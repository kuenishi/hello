#include "my_util.h"
#include <stdio.h>
#include <assert.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/uio.h>
#include <unistd.h>
#include <string.h>

#include <stdlib.h>


//http://d.hatena.ne.jp/kuenishi/20080420/1208618721

//char table which you want to use in output string
/*extern char letters[] = "0123456789"
  "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
  "abcdefghijklmnopqrstuvwxyz"
  "_-";
*/

/** 長さwのランダムな文字列を/dev/urandomからとってくる
 *  範囲はlettersで規定
 **/
char* rand_str( unsigned int w){
  char letters[] = "0123456789"
  "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
  "abcdefghijklmnopqrstuvwxyz";

  int l = strlen(letters);
  int i;
  int src_fd = open( "/dev/urandom", O_RDONLY );
  if( src_fd < 0 )return "";
  //char buf[w+1];
  char * buf = (char*)malloc(sizeof(char)*(w+1) );
  int ret = read( src_fd, buf, w );
  if( ret < 0 ) perror( "SthWrong" );
  buf[w] = '\0';
  char * p;
  //  char tmp; 
  for(i=0,p=buf;i<w;++i,++p){   //p = buf; *p != '\0'; ++p ){
    /** caution!!!
     * when *p == -128, *p *= -1 does not changes the value of *p
     * because char type can't express +128.
     * -128 is 0x80, because 0xFF - 0x7F = 0x80.
     * 0x7F is 127, but 0x80 is -128. (0x81 is -127).
     **/
    *p %= l;
    if( *p < 0 )*p = - *p;
    assert( 0 <= *p && *p <= l );
    *p = letters[ (int)*p ];
  }
  close( src_fd );
  return buf;
}


data_array * new_data_array(int N){
  data_array * ret = (data_array*)malloc(sizeof(data_array));
  ret->N = N;
  ret->t = (double*)malloc(sizeof(double)*N);
  ret->sum = 0;
  return ret;
}

int print_stats( data_array * d , const char * task ){
  printf( "total %f sec, %f sec for each %s (0-%d data).\n",
	  d->sum, d->sum / d->N, task, d->N );
  return 0;
}




