#ifndef MY_UTIL_H_
#define MY_UTIL_H_


//http://d.hatena.ne.jp/kuenishi/20080420/1208618721

//char table which you want to use in output string

/** 長さwのランダムな文字列を/dev/urandomからとってくる
 *  範囲はlettersで規定
 **/
char* rand_str( unsigned int w);

typedef struct data_array_ data_array;
struct data_array_ {
  int N;
  double * t;
  double sum;
};

data_array * new_data_array(int N);

int print_stats( data_array * d , const char * task );


#endif

