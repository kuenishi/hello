#include "splay.h"
#include <string.h>
#include <stdio.h>

  //search key stacking its reference...
  // key found
  //   (1) -> the value nil -> get -> return value
  //   (2) -> the value not nil -> fail
  // key not found
  //   (3) -> the value nil -> fail
  //   (4) -> the value not nil -> add -> return added value
//#define DEBUG_

// http://www.geocities.jp/m_hiroi/clisp/clispb07.html
// top-down splay
data_t * splay( splaytree* st, data_t * key){
  
  splaytree_node * n;
  //  splaytree_node * t, *l , *r;
  //l = r = st->root;

  while( 1 ){
    n = st->root;
    int comparance = st->compare( n->key.data, key->data);
    if( comparance < 0 ){ //key is in the left of the tree
      //rotate
      if( n->left == NULL ) break;
      int c = st->compare( n->left->key.data, key->data ); 
      if( c < 0 ){ //key is in the left-left of the tree ; do zig-zig
	if( n->left->left == NULL ) break;
	zig_zig( &(st->root), n, n->left, n->left->left );
      }
      else if( c > 0 ){
	if( n->left->right == NULL ) break;
	zig_zag( &(st->root), n, n->left, n->left->right );
      }else{
	st->root = zig( n, n->left );
      }
    }
    else if( comparance > 0 ){ //key is in the right of  the tree
      //rotate
      if( n->right == NULL )break;// || n->right->right == NULL) break;
      int c = st->compare( n->right->key.data, key->data ); 
      if( c > 0 ){ //key is in the left-left of the tree ; do zig-zig
	if( n->right->right == NULL ) break;
	zig_zig( &(st->root), n, n->right, n->right->right );
      }
      else if( c < 0 ){
	if( n->right->left == NULL ) break;
	zig_zag( &(st->root), n, n->right, n->right->left );
      }else{
	st->root = zig( n, n->right );
      }

    }
    else { //( comparance == 0 ){
	//TODO: case (2) -> error handling;
      break; //normal route in case (1)
    }
  }

  return 0;
}

/* zig ステップ */
/*     p が根ノードの場合に実行される。木の回転は、x と p を繋ぐ辺の上で行われる。zig ステップはスプレー操作前の状態で x の深さが奇数だったときだけ、スプレー操作の最後のステップとして実行される。 */
splaytree_node * zig(splaytree_node * p, splaytree_node * x){
#ifdef DEBUG_
  puts( __func__ );
  printf( " doing zig in %s - %s \n", (char*)p->key.data, (char*)x->key.data );
#endif
  if( p->left == x ){
    return rotate_node_right( p );  
  }  else if( p->right == x ){
    return rotate_node_left( p );
  }else
    return p; //なんかおかしい場合はもとに戻す
}

/* zig-zig ステップ */
/*     p が根ノードではなく、x も p も親に対して共に右の子ノード、あるいは共に左の子ノードの場合、実行される。下図では、x も p の左の子ノードの場合を示している。木の回転は p とその親である g を繋ぐ辺の上で行われ、次に x と p を繋ぐ辺の上で行われる。zig-zig ステップは、Allen と Munro が rotate to root と名づけた手法とスプレー木の唯一の違いである。 */
int zig_zig(splaytree_node ** p_destination, splaytree_node * g, splaytree_node * p, splaytree_node * x){
#ifdef DEBUG_
  puts( __func__ );
#endif

  //  if( g == NULL || p == NULL || x == NULL )return -1;
  *p_destination = zig( g, p );
  *p_destination = zig( *p_destination, x );

  return 0;
}

/* zig-zag ステップ */
/*     p が根ノードではなく、x が右の子ノードで p が左の子ノードの場合（または逆の組合せの場合）、実行される。木の回転はまず x と p を繋ぐ辺の上で行われ、次に x と新たな親ノードとなった g とを繋ぐ辺の上で行われる。 */
int zig_zag(splaytree_node ** p_destination, splaytree_node * g, splaytree_node * p, splaytree_node * x){
#ifdef DEBUG_
  puts( __func__ );
#endif

  if( g->left == p )
    g->left = zig( p, x );
  else if( g->right == p )
    g->right = zig( p, x );

  *p_destination = zig( *p_destination, x );
  //return zig( zig(g,p) ,x);
  return 0;
}

