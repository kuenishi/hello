#ifndef SPLAY_H_
#define SPLAY_H_

#include "btree.h"

//スプレー木の最大深さを想定；balance_splayでmallocするサイズ
#define MAX_SPLAY_TREE_DEPTH (1024)

typedef btree splaytree;
typedef btree_node splaytree_node;

data_t * splay( splaytree* st, data_t * key );

splaytree_node * zig(splaytree_node * p, splaytree_node * x);
int zig_zig(splaytree_node ** p_destination, splaytree_node * g, splaytree_node * p, splaytree_node * x);
int zig_zag(splaytree_node ** p_destination, splaytree_node * g, splaytree_node * p, splaytree_node * x);


#endif
