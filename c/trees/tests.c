#include "tests.h"
#include <stdio.h>
#include <sys/time.h>
#include <time.h>
#include <string.h>

int zigs_test(splaytree* bt){

  if( bt->root->left != NULL && bt->root->left->left != NULL ){
    puts("-----------zig-zig left:");
    zig_zig( &(bt->root), bt->root, bt->root->left, bt->root->left->left);
    print_btree( bt );
  }
  if( bt->root->right != NULL && bt->root->right->right != NULL ){
    puts("-----------zig-zig right:");
    zig_zig( &(bt->root), bt->root, bt->root->right, bt->root->right->right);
    print_btree( bt );
  }
  if( bt->root->left != NULL && bt->root->left->right != NULL ){
    puts("-----------zig-zag left:");
    zig_zag( &(bt->root), bt->root, bt->root->left, bt->root->left->right);
    print_btree( bt );
  }
  if( bt->root->right != NULL && bt->root->right->left != NULL ){
    puts("-----------zig-zag right:");
    zig_zag( &(bt->root), bt->root, bt->root->right, bt->root->right->left);
    print_btree( bt );
  }

  return 0;
}


int performance_test(btree * bt, int N, int l){

  //  btree * bt = generate_random_btree(N, l, cmp, p_node);
  //print_btree( bt );
  printf("%d\n" , bt->size );
  data_t * d, *v;
  d= new_data( sizeof(char)*(l+1) );

  //start benchmarking...
  struct timeval start, end;
  int i, n, c;
  n = 0x10000;
  c = 0;
  char * tmp;
  data_array * stats = new_data_array( n ); 

  //print_stats( stats, "hoge" );

  for( i = 0; i < n; ++i ){
    gettimeofday( &start, NULL );
    
  //embed code here;
    tmp = rand_str( l );
    d->data = (void*)tmp;
    v = get_value(bt, d);
    if ( v != NULL )++c;
    free( tmp );
  
    gettimeofday( &end  , NULL );
    stats->t[i] = end.tv_sec - start.tv_sec + (end.tv_usec - start.tv_usec ) * 1e-6 ;
    stats->sum += stats->t[i];
  }

  //  printf("%d\n", stats->N );
  printf( "%d/%d hit:", c, n );
  print_stats( stats, "read of binary tree" );

  free( d );
  free( stats->t );
  free( stats );
  return 0;
}

int splay_performance_test(splaytree* bt , int N, int l){

  //print_btree( bt );
  data_t * d, *v;
  d= new_data( sizeof(char)*(l+1) );

  //start benchmarking...
  struct timeval start, end;
  int i, n, c;
  n = 0x10000;
  c = 0;
  char * tmp;
  data_array * stats = new_data_array( n ); 

  for( i = 0; i < n; ++i ){
    gettimeofday( &start, NULL );
    
  //embed code here;
    tmp = rand_str( l );
    d->data = (void*)tmp;
    v = get_value(bt, d);
    if ( v != NULL ){
      //      puts( tmp );
      splay( bt , d );
      ++c;
    }
    free( tmp );

    gettimeofday( &end  , NULL );
    stats->t[i] = end.tv_sec - start.tv_sec + (end.tv_usec - start.tv_usec ) * 1e-6 ;
    stats->sum += stats->t[i];
  }

  printf( "%d/%d hit:", c, n );
  print_stats( stats, "read of splay tree" );

  free( stats->t );
  free( stats );

  return 0;
}




btree * generate_random_btree(int N, int L,
			      int(*cmp)(void* left, void* right),
			      int(*p_node)(data_t*, data_t*)){
  
  btree*  bt   = new_btree(cmp, p_node);
  int i, tmp;
  char* stmp;
  data_t * pdata, *qdata;
  srandom( time(NULL ) );
  struct timeval start, end;
  data_array * stats = new_data_array(N);

  for( i = 0; i < N; ++i ){
    gettimeofday( &start, NULL );

    pdata = new_data( sizeof(char)* (L+1) );
    stmp = rand_str( L );
    memcpy( pdata->data, stmp, sizeof(char)*(L+1) );
    qdata = new_data(sizeof(int) );
    tmp = random() % LOCAL_MAX_RAND;
    memcpy( qdata->data, &tmp, sizeof(int) );
    //  p(pdata, qdata);
    //    if( add_pair( btn, pdata, qdata, comp ) < 0 )
    if( add_value( bt, pdata, qdata ) < 0 ){
      printf("insertion failed.\n");
    }
    gettimeofday( &end, NULL );
    stats->t[i] = end.tv_sec - start.tv_sec + (end.tv_usec - start.tv_usec ) * 1e-6 ;
    stats->sum += stats->t[i];
    free_data(pdata);
    free_data(qdata);
    free(stmp);
  }

  print_stats( stats , "write" );
  free( stats->t );
  free( stats );

  return bt;
}

