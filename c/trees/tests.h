#ifndef TESTS_H_
#define TESTS_H_

#include "btree.h"
#include "splay.h"
#include "my_util.h"

int zigs_test(splaytree* bt);
int performance_test(btree * bt, int N, int l);
int splay_performance_test(btree* bt, int N, int l);

static const int LOCAL_MAX_RAND = 65536;


btree* generate_random_btree(int N, int L,
			     int(*cmp)(void* left, void* right),
			     int(*p_node)(data_t*, data_t*));

#endif

