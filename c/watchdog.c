/*
  file system event watcher - with kevent. but badly works 
  because directory's discriptor event can't watch grand-
  children's (and their descents') events.
 */
#include <stdio.h>
#include <fcntl.h>  // for open(2)
#include <stdlib.h> // for exit(3)
#include <unistd.h> // for pipe(2)
#include <signal.h> // for signals

#include <sys/types.h>
#include <sys/time.h>

#ifdef __APPLE__
#include <sys/event.h>
#endif

int pipes[2];

static void stop(int i){
  write(pipes[1], "a", 1);
  printf("stopping.\n");
}

int main(int args, char ** argv){
  char * watchee;
  if( args < 2 ){
    watchee = ".";
  }
  int fd = open(watchee, O_RDONLY);
  if( fd < 0 ){
    perror( watchee );
    exit(1);
  }
  
  int pfd = kqueue();
  int nevents;
  struct kevent ev;
  int flag = 1;

  EV_SET(&ev, fd, EVFILT_VNODE, EV_ADD|EV_CLEAR, //EV_CLEAR make it edge-trigger'd.
	 NOTE_WRITE|NOTE_DELETE|NOTE_EXTEND|NOTE_ATTRIB|NOTE_LINK|NOTE_RENAME|NOTE_REVOKE,
	 0, NULL );
  kevent( pfd, &ev, 1, NULL, 0, NULL );
  
  pipe(pipes);
  EV_SET(&ev, pipes[0], EVFILT_READ, EV_ADD, 0, 0, NULL );
  
  // not working
  //EV_SET(&ev, STDIN_FILENO, EVFILT_READ, EV_ADD, 0, 0, NULL);

  printf("watching %s...\n", watchee);
  do{
    nevents = kevent( pfd, NULL, 0, &ev, 1, NULL );
    if( nevents > 0 ){
      if( ev.ident == pipes[0]){ // || ev.ident == STDIN_FILENO ){
	printf("event.\n");
	flag = 0;

      }else if( ev.ident == fd ){
	printf("event. - ");
	switch( ev.fflags ){
	case NOTE_DELETE:
	  printf("NOTE_DELETE");
	  break;
	case NOTE_WRITE:
	  printf("NOTE_WRITE");
	  break;
	case NOTE_EXTEND:
	  printf("NOTE_EXTEND");
	  break;
	case NOTE_ATTRIB:
	  printf("NOTE_ATTRIB");
	  break;
	case NOTE_LINK:
	  printf("NOTE_LINK");
	  break;
	case NOTE_RENAME:
	  printf("NOTE_RENAME");
	  break;
	case NOTE_REVOKE:
	  printf("NOTE_REVOKE");
	  break;
	default:
	  printf("unknown event: %d", ev.fflags);
	}
	printf("\n");
      }
    }
  }while(flag);

  close(fd);
  close(pfd);
}

