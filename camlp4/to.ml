open Camlp4

module To = struct
  let name = "pa_to"  (* change *)
  let version = "0.0.1"  (* change *)
end

module Make (Syntax : Sig.Camlp4Syntax) = struct
  open Sig
  include Syntax
  open Ast

  (* 文法拡張部 *)
  let a _loc = <:expr<[]>>;;
  let b _loc = <:patt<[]>>;;
(*  let _ = <:ctyp< 'a list >>;;
  let _ = <:ctyp< ('a, 'b) Hastbl.t>>;;
  let _ = <:ctyp< int list option>>;;
*)
  let f myloc = <:patt@myloc<[], "", wwaasd, asdf>>

end

let module M = Register.OCamlSyntaxExtension(To)(Make) in ()
