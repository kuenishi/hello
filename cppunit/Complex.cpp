#include <cppunit/TestCase.h>


class Complex {
public:
  double real, imaginary;
  Complex(const double & r, const double & i):real(r),imaginary(i){};
  ~Complex(){};
  friend bool operator==(const Complex &l, const Complex & r );
};

bool operator==(const Complex &l, const Complex & r ){
  return true;
}


class ComplexTest : public CppUnit::TestCase { 
private:
  Complex *m1, *m2, *m3;
public: 
  void setUp(){
    m1 = new Complex(10,1);
    m2 = new Complex(1,0);
    m3 = new Complex(1.0,123);
  }
  void tearDown(){
    delete m1, m2, m3;
  };

  ComplexTest( std::string name ) : CppUnit::TestCase( name ) {}
  
  void testEq() {
    CPPUNIT_ASSERT( Complex (10, 1) == Complex (10, 1) );
    CPPUNIT_ASSERT( !(Complex (1, 1) == Complex (2, 2)) );
  }

};
