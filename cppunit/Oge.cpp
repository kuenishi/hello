#include "Oge.h"

Oge(const int& id, const string& ad, const double& hoge, const double& hage, const void *& moe):
  id_( id),
  ad_( ad),
  hoge_( hoge),
  hage_( hage),
  moe_( moe){
};

Oge::Oge(){
}

Oge::~Oge(){
}

void Oge::hoge(){
}

void Oge::hoge2(const int& a, const double& b){
}
