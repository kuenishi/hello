#ifndef OGE_H_
#define OGE_H_

//
// Oge.h
// 
// Made by kuenishi
// Login   <kuenishi@kushana>
// Generated on 2008/06/22 22:07:14 JST
//

class Oge{
 private:
  int id_;
  string ad_;
  double hoge_;
  double hage_;
  void * moe_;
 public:
  Oge(const int& id, const string& ad, const double& hoge, const double& hage, const void *& moe);
  Oge();
  ~Oge();
  void hoge();
  void hoge2(const int& a, const double& b);
};

#endif

