#include <cppunit/ui/text/TestRunner.h>
#include <cppunit/TextOutputter.h>
#include <cppunit/CompilerOutputter.h>
#include <cppunit/extensions/TestFactoryRegistry.h>
//#include <cppunit/TestRunner.h>

int main(int argc, char* argv[])
{

  CppUnit::TextUi::TestRunner runner;
  runner.addTest(CppUnit::TestFactoryRegistry::getRegistry().makeTest());
  CppUnit::Outputter* outputter = 
    new CppUnit::CompilerOutputter(&runner.result(),std::cout);
  //    new CppUnit::TextOutputter(&runner.result(),std::cout);
  //    new CppUnit::XmlOutputter(&runner.result(),std::cout);
  runner.setOutputter(outputter);
  return runner.run() ? 0 : 1;

    /* TestRunner を生成して run() を実行する。 
    CppUnit::TestRunner runner;
     ここに今後テストを追加していく。 
    CppUnit::TestSuite suite;
    CppUnit::TestResult result;
    return runner.run(argc, argv);*/
}
