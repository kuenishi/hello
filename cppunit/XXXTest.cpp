
/*
 * test skeleton XXXTest.cpp [1]
 * thx 2 http://www.s34.co.jp/cpptechdoc/misc/cppunit_guide/index.html
 * it worked! 
 * $ g++ -I/opt/local/include -L/opt/local/lib -lcppunit
 */

// 必要なヘッダの #include [2]
// ex. #include "XXX.h"

#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/TestAssert.h>

// XXX をテストする [3]
class XXXTest : public CppUnit::TestFixture {
  CPPUNIT_TEST_SUITE(XXXTest); // [4]
  CPPUNIT_TEST(test_one);      // [5]
  CPPUNIT_TEST(test_two);      // [5]
  CPPUNIT_TEST(test_three);    // [5]
  CPPUNIT_TEST(test_four);     // [5]
  CPPUNIT_TEST(test_five);     // [5]
  CPPUNIT_TEST(test_six);      // [5]
  CPPUNIT_TEST_SUITE_END();    // [4]
private:

  // 必要な メンバ変数/関数 [6]

public:

  /*
   * コンストラクタ/デストラクタ [7]
   */
  XXXTest() {
  }

  ~XXXTest() {
  }

  /*
   * 初期化/後始末 [8]
   */
  virtual void setUp() { 
  }

  virtual void tearDown() { 
  }

  /*
   * テスト・ケース [9]
   */
  void test_one() {
    CPPUNIT_ASSERT( 0 == 1 );
  }

  void test_two() {
    CPPUNIT_ASSERT_MESSAGE( "0 is not equal to 1", 0 == 1 );
  }

  void test_three() {
    CPPUNIT_FAIL( "never reached here!" );
  }

  void test_four() {
    CPPUNIT_ASSERT_EQUAL( 0, 1 );
  }

  void test_five() {
    CPPUNIT_ASSERT_EQUAL_MESSAGE( "1 is not equal to 0", 0, 1 );
  }

  void test_six() {
    CPPUNIT_ASSERT_DOUBLES_EQUAL( 1.23, 1.24, 0.001 );
  }

};

/*
 * register test suite
 */
CPPUNIT_TEST_SUITE_REGISTRATION(XXXTest); // [10]

/**
#include <cppunit/ui/text/TestRunner.h>
#include <cppunit/TextOutputter.h>
#include <cppunit/CompilerOutputter.h>
#include <cppunit/extensions/TestFactoryRegistry.h>

int main() {
  CppUnit::TextUi::TestRunner runner;
  runner.addTest(CppUnit::TestFactoryRegistry::getRegistry().makeTest());
  CppUnit::Outputter* outputter = 
    new CppUnit::CompilerOutputter(&runner.result(),std::cout);
  //    new CppUnit::TextOutputter(&runner.result(),std::cout);
  //    new CppUnit::XmlOutputter(&runner.result(),std::cout);
  runner.setOutputter(outputter);
  return runner.run() ? 0 : 1;
}
*/
