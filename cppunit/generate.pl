#!/usr/bin/env perl -W
#
# @breif what this script does (aims at):
# 1. Generates a C++ class file from template without writing some misc.
#    such as 
#      - class name,
#      - author,
#      - member variables,
#      - member setter, member getter,
#      - member functions,
#      - ctor, dtor,
#    and anything else.
# 2. Generates a cppunit test suite from template without complex syntax.
# 3. Inputs the new class into whole test suite.
#
# usage:
#  $ ./generate.pl NewClassName <members> <methods?> 

#my $template = "XXXTest.cpp.tmpl";
#my $template = "XXX.cpp.tmpl";
#my $template = "XXX.cpp.tmpl";

package ClassModel;

# コンストラクタ
sub new{

    my $this = shift;
    my ( $name, @var ) = @_ ;

    my $class = { "name" = $name, "var" = [] } ;

    bless $class, $this;

    return $class ;
}

sub set_var{
    $this = shift;
    $this->{var};
}

# main のパッケージ名を設定
package main;

my $tmp = new ClassModel "Hoge";

print  "class name:\n";
my $classname = $ARGV[0]; # or <STDIN> or die;
#print "Hello\n";

my $outfilename = $classname."Test.cpp";


if(!open (TEMPLATE, "<$template")){
    print "Failed Loading ", $template, "\n";
    exit(-1);
}
if(!open (OUTPUT, ">$outfilename")){
    print "Failed Opening ", $outfilename, "\n";
    exit(-1);
}

while(<TEMPLATE>){
    $_ =~ s/(XXX)/$classname/g;
    print OUTPUT;
}
close(TEMPLATE);
close(OUTPUT);

print $outfilename , "\n";
