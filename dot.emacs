(define-key global-map "\C-o" 'dabbrev-expand)
;(global-unset-key "\C-x\C-c")

(setq user-full-name "UENISHI Kota")
(setq user-mail-address "kuenishi@gmail.com")

;(calendar)
;(diary)

;;fullscreen,color,alpha
(if window-system
    (progn ()
           (toggle-scroll-bar nil)
           (tool-bar-mode)
           (setq mac-autohide-menubar-on-maximize t)

           (setq default-frame-alist
                 (append (list
                          '(height . 39)
			  '(width . 120)
                          )))

           ;(require 'color-theme)
           ;(color-theme-initialize)
           ;(color-theme-dark-laptop)

           ;(set-frame-parameter nil 'alpha 90 )
           (set-background-color "Black")
           (set-foreground-color "LightGray")
           (set-cursor-color "Gray")
           )
  )

;(setq auto-mode-alist
;  (cons '("\\.ml\\w?" . tuareg-mode) auto-mode-alist))
;(autoload 'tuareg-mode "tuareg" "Major mode for editing Caml code." t)
;(autoload 'camldebug "cameldeb" "Run the Caml debugger." t)

; inhibit startup-splash screen (where a gnu appears)
(setq inhibit-splash-screen t)

;(load-file "/usr/share/emacs/site-lisp/xcscope.el")
;(require 'xcscope)

(setq-default indent-tabs-mode nil);; tab ではなく space を使う
;(setq-default tab-width 3);; tab 幅を 4 に設定
;(setq next-line-add-newlines nil);; バッファの最後の行で next-line しても新しい行を作らない
;(put 'narrow-to-region 'disabled nil) ;; narrowing を禁止

;; c-mode, c++-mode
;(add-hook 'c-mode-common-hook
;          '(lambda ()
;             ;(c-set-style "k&r")             ;;; K&R のスタイルを使う
;             (setq indent-tabs-mode nil)             ;;; インデントには tab を使わない
;             (setq c-basic-offset 4)             ;;; インデント幅
;             ))

(setq auto-mode-alist
  (cons '("\\.php\\w?" . php-mode) auto-mode-alist))
(autoload 'php-mode "php-mode" "PHP mode." t)

;http://sakito.jp/emacs/emacs23.html#id17
(create-fontset-from-ascii-font "Menlo-14:weight=normal:slant=normal" nil "menlokakugo")
(set-fontset-font "fontset-menlokakugo"
                  'unicode
                  (font-spec :family "Hiragino Kaku Gothic ProN" :size 16)
                  nil
                  'append)
(add-to-list 'default-frame-alist '(font . "fontset-menlokakugo"))

;(setq load-path (cons "/usr/local/go/misc/emacs" load-path))
;; Go mode
;(require 'go-mode-load)

(when (>= emacs-major-version 24)
  (require 'package)
  (add-to-list
   'package-archives
   '("melpa" . "http://melpa.org/packages/")
   t)
  (package-initialize))
