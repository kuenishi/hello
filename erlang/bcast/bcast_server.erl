%%% File    : bcast_server.erl
%%% Author  : UENISHI Kota <kuenishi@gmail.com>
%%% Description : 
%%% Created : 13 Mar 2010 by UENISHI Kota <kuenishi@gmail.com>

-module(bcast_server).
-compile(export_all).

-define(PORT, 10007).
-define(ADDR, {224,0,0,251}).

start()->
    {ok,Socket}=gen_udp:open(?PORT, [binary,inet,{active,true},{reuseaddr,true},{ip,?ADDR}]),
    ok=inet:setopts(Socket, [{add_membership,{?ADDR,{0,0,0,0}}}]),
    Pid=spawn(?MODULE, serve, [Socket]),
    gen_udp:controlling_process(Socket, Pid),
    {Socket, Pid}.

stop(Pid)->
    Pid ! {self(), close}.

bcast(Payload)->
    {ok,Socket}=gen_udp:open(0, [binary,inet,{reuseaddr,true},{broadcast,true}]),
    ok=gen_udp:send(Socket,?ADDR,?PORT,term_to_binary(Payload)),
    ok=gen_udp:close(Socket).

serve(Sock)->
    receive
	{udp,Socket,IP,InPortNo,Packet}->
	    io:format("~p from ~p:~p~n", [binary_to_term(Packet), IP, InPortNo]),
	    serve(Socket);
	{_From, close}->
	    gen_udp:close(Sock)
    end.


