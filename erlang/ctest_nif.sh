#!/bin/sh

ERL_ROOT=`erl -noshell -eval 'io:format("~s", [code:root_dir()])' -s init stop`

echo $ERL_ROOT
gcc -fPIC -g -o test_nif.so test_nif.c -bundle -undefined suppress -flat_namespace -I $ERL_ROOT/usr/include/
