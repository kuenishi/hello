%%% @author UENISHI Kota <kuenishi@gmail.com>
%%% @copyright (C) 2010, UENISHI Kota
%%% @doc
%%%
%%% @end
%%% Created :  8 Jul 2010 by UENISHI Kota <kuenishi@gmail.com>

-module(exception).
-author('kuenishi@gmail.com').

-export([main/0]).

main()->
    try
	somexcp(10)
    catch
	error:Reason->
	    erlang:display(Reason);
	throw:Thrown->
	    erlang:display(Thrown)
    end.

somexcp(0)->
%    1/0.
    throw(throwable1);
somexcp(N) when is_integer(N), N>0 ->
    somexcp(N-1).

