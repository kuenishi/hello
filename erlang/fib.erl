-module(fib).
-export([fib/1]).

fib(1) -> 1;
fib(2) -> 2;
fib(N) when is_integer(N), N > 0 ->
    fib(N-1) + fib(N-2).

