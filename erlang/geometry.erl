-module(geometry).
-export([area/2]).

area(rectangle,{Width,Ht})-> Width*Ht;
area(square, {X} )        -> area(rectangle,{X,X});
area(circle,   {R})       -> 3.141592*R*R.
