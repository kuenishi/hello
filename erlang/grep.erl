%#!/usr/bin/env escript
%simplest grep in erlang $Id: grep.erl 402 2007-09-25 18:13:02Z kuenishi $

-module(grep).
-export([main/1,hello/0, repeat/1]).

%Run:
% escript grep.erl

main({regexp,file}) ->
	repeat(2).
%main([_])->hello().

hello() ->
	io:format("Hello world!~n").

repeat(N) when N == 1 ->hello();
repeat(N) when N > 1 -> hello(), repeat(N-1).
	
