-module(hello).
-export([hello/0]).

% hello.erl
%
% Compilation:
% $ erlc hello.erl
%
% Run:
% $ erl -noshell -s hello hello -s init stop
% Hello world!

hello() ->
	io:format("Hello world!~n").
