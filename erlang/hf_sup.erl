%% -*- coding: utf-8 -*-
-module(hf_sup).
-author('kuenishi@gmail.com').

-behaviour(supervisor).

-export([start_link/0, init/1]).
-ifdef(UNITTEST).
-include_lib("eunit/include/eunit.hrl").
-endif.

start_link()->
    supervisor:start_link({local, ?MODULE}, ?MODULE, []).

init([])->
    Children = [
		{hf_storage,  {hf_storage,  start_link, []}, permanent, 2000, worker, [hf_storage]},
		{hf_listener, {hf_listener, start_link, []}, permanent, 2000, worker, [hf_listener]}
	       ],
    ok = supervisor:check_childspecs(Children),
    {ok, {{one_for_one, 1, 1}, Children}}.

-ifdef(EUNIT).
my_test()->
    {ok, {{one_for_one, 1, 1}, _}}=hf_sup:init([]).
-endif.
