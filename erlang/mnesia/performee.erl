-module(performee).
-author('kuenishi@gmail.com').

-export([
	 start/1,
	 stop/0
	]).

-record(r, {id,content}).

start(Nodes)->
    mnesia:create_schema(Nodes),
    mnesia:start(),
    mnesia:create_table(r, [{disc_copies, Nodes},
			    {attributes,  record_info(fields,r)}
			   ]).

stop()->
    mnesia:stop().
			    
