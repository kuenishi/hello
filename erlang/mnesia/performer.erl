-module(performer).
-author('kuenishi@gmail.com').

-export([
	 start/3,
	 stop/0
	]).

-record(r, {id,content}).

-import(random).

generate(0)-> [];
generate(Length)->
    RChar = 96+random:uniform(26),
    [RChar|generate(Length-1)].

gen_atom(Length)-> erlang:list_to_atom(generate(Length)).

start(Nodes, NumThreads, T)->
    {A1, A2, A3}=now(),
    random:seed(A1, A2+T, A3),
    Start = now(),
    register(  coordinator, self() ),
    spawn_q(NumThreads, T, []),
    TotalDone = get_result(NumThreads, 0),
    End = now(),
    Duration = timer:now_diff( End, Start ) / 1000000.0,
    io:format( "Total done: ~p in ~p sec., at least  ~p qps...~n", 
	       [ TotalDone, Duration, NumThreads * T / Duration ]
	      ),
    unregister( coordinator ).

get_result(0, TotalDone)->
%    io:format("~p qps.~n", [ResultArray]),
    stop(),
    TotalDone;
get_result(P, TotalDone)->
%    io:format("~p , ~p.~n", [P, ResultArray]),
    receive
	{done, Done} ->
	    get_result( P-1, Done+TotalDone )
    end.
	
spawn_q(0, _T, _Processes)->
    ok;
spawn_q(P, T, Processes)->
    Pid = spawn_link( fun()-> q( T, 0 ) end),
    spawn_q(P-1, T, [Pid|Processes]).
			

q(0, Done)->
    coordinator ! {done, Done};
q(N, Done)->
    Key = gen_atom(16),
    Reader = fun()-> mnesia:read(r, Key, read) end, %% 13k qpsくらい出る　スレッド数によっては20k行く
    Writer = fun() ->                               %%  4k qpsくらい出る　
		     Value = generate(256),
		     mnesia:write(#r{id=Key,content=Value})
	     end,
    case mnesia:transaction(Reader) of
%    case mnesia:transaction(Writer) of
	{atomic, ok}->
	    q(N-1, Done+1);
	{atomic, []} -> 
	    q(N-1, Done+1);
 	{atomic, [E]} -> 
	    q(N-1, Done+1);
 	{aborted, Reason} ->
	    io:format("transaction failed: ~p~n", [Reason]),
 	    q(N-1, Done);
	Other ->
	    io:format("transaction failed: ~p~n", [Other])
    end.

stop()->
    ok.%    mnesia:stop().
			    
