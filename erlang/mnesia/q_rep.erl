-module(q_rep).
-author('kuenishi@gmail.com').

-export([
	 start/1,
	 stop/0,
	 insert/1,
	 select/1,
	 agent_loop/3,
	 map_in_put/3,
	 write/2
	]).

-record(kgv, {key, generation, value}).

start(Nodes)->
    mnesia:create_schema(Nodes),
    mnesia:start(),
    mnesia:create_table(kgv, [{disc_copies, Nodes},
			    {attributes,  record_info(fields,kgv)}
			   ]).

stop()->
    mnesia:stop().
			    
insert( {Key, Generation, Value} )->
    mnesia:transaction(
      fun()->
	      mnesia:write(#kgv{key=Key, generation=Generation, value=Value})
      end
     ).

select( Key )->
    [E] = mnesia:dirty_read(kgv, Key),
    E.

%% frontend...
write(Nodes, {Key, Generation, Value})->
    lists:foreach(
      fun(Node) -> spawn(?MODULE, map_in_put, [Node, {Key, Generation, Value}, self()]) end,
      Nodes
     ),
    [N, W] = [length(Nodes), 2],
    gather_in_put(N, W).

map_in_put(Node, Data, Pid) ->
    case rpc:call(Node, ?MODULE, insert, [Data]) of
	{atomic, ok}   ->
	    Pid ! {ok, Pid};
        {error, Reason} ->
            Pid ! {error, Reason};
        Other ->
            Pid ! {exception, Other}
    end.

gather_in_put(_N, 0) ->
    ok;
gather_in_put(0, _W) ->
    {error, ebusy};
gather_in_put(N, W) ->
    receive
        {ok, Pid}     -> 
	    gather_in_put(N-1, W-1);
        _Other -> 
	    io:format( "~p~n", [_Other]),
	    gather_in_put(N-1, W)

    after 12000 ->
            {error, etimedout}
    end.

agent_loop(master,  Nodes, {N, W, R})->
    ok;
agent_loop(replica, Nodes, {N, W, R})->
    ok.
