-module(test_mnesia).
-author('kuenishi@gmail.com').

-export([main/1]).
-import(mnesia).

-record( file, {path, 
		content = "", 
		generation = 0,
		locker, 
		lock_queue = [],
		child_nodes = []
	       }).

main(_)->
    mnesia:create_schema([node()]),
    mnesia:start(),
    {atomic, ok}=mnesia:create_table( file, [{ram_copies, [node()] }, % disc_copies
					     {access_mode, read_write},
					     {attributes, record_info(fields, file)}]),
    mnesia:transaction(
      fun()-> mnesia:write(#file{path="/", locker=none}) end
     ),
    mnesia:transaction(
      fun()->
	      [E]=mnesia:read(file, "/", write),
	      {Row, Path, Content, Generation, Locker, LockQueue, Children}=E,
	      N=E#file{child_nodes=lists:append(Children, ["/usr"])},
%	      io:format("old:~p, new:~p~n", [E,N] ),
	      mnesia:write(N),
	      mnesia:write(#file{path="/usr", locker=none}) end 
     ),
    mnesia:transaction(
      fun()->
	      [E]=mnesia:read(file, "/", write),
	      {Row, Path, Content, Generation, Locker, LockQueue, Children}=E,
	      N=E#file{child_nodes=lists:append(Children, ["/aa"])},
%	      io:format("old:~p, new:~p~n", [E,N] ),
	      mnesia:write(N),
	      mnesia:write(#file{path="/aa", locker=none}) end 
     ),
    mnesia:transaction(
      fun()->
	      lists:foreach(
		fun(X)->
			io:format("~p~n", [mnesia:read(file, X, read)])
		end,
		mnesia:all_keys(file))
      end),
%    mnesia:info(),
%    mnesia:schema(file),
    mnesia:stop(),
    io:format("~p~n", [ hogehoge ]).

