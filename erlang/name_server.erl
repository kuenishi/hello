-module(name_server).
-export([init/0, add/2, whereis/1, handle/2, stress/1]).
%-import(server1,[rpc/2]).
-import(server2,[rpc/2]).

%client routines
add(Name, Place) -> rpc(name_server, {add, Name, Place} ).
whereis( Name ) ->
    rpc(name_server, {whereis, Name}).

%callback
init()->
    dict:new().

handle({add, Name, Place}, Dict )->
    {ok, dict:store(Name, Place, Dict)};
handle({whereis, Name}, Dict) ->{dict:find(Name, Dict),Dict}.


stress(0)-> name_server:add(hoge, "last");
stress(N)-> 
    name_server:add(hoge, "here"),
    stress(N-1).


% usage:
%> server1:start(name_server, name_server).
%> name_server:add(joe, "at home").
%> name_server:whereis( joe ).
