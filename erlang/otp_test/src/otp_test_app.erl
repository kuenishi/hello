%%%-------------------------------------------------------------------
%%% Author  : martinjlogan 
%%% @doc The entry point into our application.
%%% @end
%%%-------------------------------------------------------------------
-module(otp_test_app).

-behaviour(application).
%%--------------------------------------------------------------------
%% Include files
%%--------------------------------------------------------------------
%-include("location_server.hrl").

%%--------------------------------------------------------------------
%% External exports
%%--------------------------------------------------------------------
-export([
     start/2,
     shutdown/0,
     stop/1
     ]).

%%====================================================================
%% External functions
%%====================================================================
%%--------------------------------------------------------------------
%% @doc The starting point for an erlang application.
%% @spec start(Type, StartArgs) -> {ok, Pid} | {ok, Pid, State} | {error, Reason}
%% @end
%%--------------------------------------------------------------------
start(Type, StartArgs) ->
    io:format("~p started....~n", [?MODULE]),
%    otp_test_sup2:start_link(StartArgs),
    case otp_test_sup:start_link(StartArgs) of
	{ok, Pid} ->
	    {ok, Pid};
	Error ->
	    Error
    end.

%%--------------------------------------------------------------------
%% @doc Called to shudown the auction_server application.
%% @spec shutdown() -> ok
%% @end
%%--------------------------------------------------------------------
shutdown() ->
    io:format("shutdown called.~n", [] ),
    application:stop(otp_test_sup).

%%====================================================================
%% Internal functions
%%====================================================================

%%--------------------------------------------------------------------
%% Called upon the termination of an application.
%%--------------------------------------------------------------------
stop(State) ->
    ok.
