-module(otp_test_fsm).
-author('kuenishi@gmail.com').

%%finite state machine sample
%% see also: $ erl -man gen_fsm


%%  STATE DIAGRAM OF AN INFANT: any other state transition is ignored.
%%
%%   +--------> crying <-------+
%%   |            |            |
%%   |            | praise!    | scare!
%%   |timeout     |            |
%%   | (10sec)    +-> happy ---+
%%   |                  | <----+
%%   |           timeout|      |
%%   |            (5sec)|      | praise!
%%   |                  |      | 
%%   +--------- sad <---+      |
%%               |             |
%%               +-------------+

%-compile(export_all).
-behaviour(gen_fsm).

-export( [code_change/4,
	 handle_event/3,
	 handle_info/3, 
	 handle_sync_event/4,
	 init/1,
	 terminate/3
	]).
-export( [start/0, stop/0,  scare/0, praise/0 , current/0, crying/2, happy/2 , sad/2 ]).

%-compile(export_all).

start() ->
    gen_fsm:start_link({local, ?MODULE}, ?MODULE, [], []).

stop() -> 
    io:format("~p ~p.~n",  [?MODULE, stopped]),
    gen_fsm:send_all_state_event(?MODULE, stop).

scare() ->
    gen_fsm:send_event(?MODULE, scare).
praise() ->
    gen_fsm:send_event(?MODULE, praise).

current()->
    gen_fsm:send_all_state_event(?MODULE, current).

init([]) ->
    io:format("~p ~p.~n",  [?MODULE, started]),
    process_flag(trap_exit, true),
    {ok, crying, []}.

crying(praise, History) ->
    NewHistory = [crying|History],
    io:format("~p , ~p.~n", [crying, History]),
    {next_state, happy, NewHistory, 5000};
crying(_, History)->
    {next_state, sad, History}.

happy(scare, History)->
    NewHistory = [happy|History],
    io:format("~p , ~p.~n", [happy, History]),
    {next_state, crying, NewHistory};

happy(praise, History)->
    NewHistory = [happy|History],
    io:format("~p , ~p.~n", [happy, History]),
    {next_state, happy, History, 5000};

happy(timeout, History)->
    NewHistory = [happy|History],
    io:format("~p , ~p.~n", [happy, History]),
    {next_state, sad, NewHistory, 10000};
happy(_, History)->
    {next_state, happy, History, 5000}.

sad(praise, History)->
    NewHistory = [sad|History],
    io:format("~p , ~p.~n", [sad, History]),
    {next_state, happy, NewHistory, 5000};

sad(timeout, History) ->
    NewHistory = [sad|History],
    io:format("~p , ~p.~n", [sad, History]),
    {next_state, crying, NewHistory, 10000};
sad(_, History)->
    {next_state, sad, History, 10000}.

handle_event(stop, _StateName, StateData) ->
    {stop, normal, StateData};
handle_event(current, _StateName, StateData) ->
    {current, normal, StateData};
handle_event(_Reason, StateName, StateData) ->
    {current, StateName, StateData}.

handle_sync_event( Event, From, StateName , StateData )->
    {Event, From, StateName, StateData}.

handle_info(_, _, _)->
    ok.

terminate(Reason, _StateName, _StateData) ->
    io:format("~p terminated (~p).~n", [?MODULE, Reason]),
    ok.

code_change(OldVsn, _StateName, _StateData, _Hoge )->
    {ok, crying, []}.
