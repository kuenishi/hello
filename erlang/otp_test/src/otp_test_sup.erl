%%%-------------------------------------------------------------------
%%% Author  : martinjlogan 
%%% @doc The top level supervisor for our application.
%%% @end
%%%-------------------------------------------------------------------
-module(otp_test_sup).

-behaviour(supervisor).

%%--------------------------------------------------------------------
%% External exports
%%--------------------------------------------------------------------
-export([start_link/1]).

%%--------------------------------------------------------------------
%% Internal exports
%%--------------------------------------------------------------------
-export([init/1]).

%%--------------------------------------------------------------------
%% Macros
%%--------------------------------------------------------------------
-define(SERVER, ?MODULE).

%%====================================================================
%% External functions
%%====================================================================
%%--------------------------------------------------------------------
%% @doc Starts the supervisor.
%% @spec start_link(StartArgs) -> {ok, pid()} | Error
%% @end
%%--------------------------------------------------------------------
start_link(_StartArgs) ->
    case supervisor:start_link({local, ?SERVER}, ?MODULE, []) of
	{ok, Pid } ->
	    {ok, Pid };
	{error, Error} ->
	    io:format("error:~p.~n", [Error] ),
	    {error, Error}
    end.

%%====================================================================
%% Server functions
%%====================================================================
%%--------------------------------------------------------------------
%% Func: init/1
%% Returns: {ok,  {SupFlags,  [ChildSpec]}} |
%%          ignore                          |
%%          {error, Reason}
%%--------------------------------------------------------------------
init([]) ->
    io:format("~p started.~n", [?MODULE]),
    RestartStrategy    = one_for_one,
    MaxRestarts        = 1000,
    MaxTimeBetRestarts = 3600,

    SupFlags = {RestartStrategy, MaxRestarts, MaxTimeBetRestarts},

    ChildSpecs =
    [
      {otp_test_gen_server,
       {otp_test_gen_server, start, []},
       permanent,
       1000,
       worker,
       [otp_test_gen_server]} ,
      {otp_test_sup2,
       {otp_test_sup2, start_link, []},
       permanent,
       infinity,
       supervisor,
       []},
     {otp_test_event,
      {gen_event, start_link, [{local, otp_test_event}]},
      permanent,
      1000,
      supervisor, %      worker,
      dynamic}
    ],
						%    io:format("~p starting !~n", [?MODULE]),
    {ok,{SupFlags, ChildSpecs}}.
