%%%-------------------------------------------------------------------
%%% Author  : martinjlogan 
%%% @doc The top level supervisor for our application.
%%% @end
%%%-------------------------------------------------------------------
-module(otp_test_sup2).

-behaviour(supervisor).

%%--------------------------------------------------------------------
%% External exports
%%--------------------------------------------------------------------
-export([start_link/1, start_link/0, start/1]).

%%--------------------------------------------------------------------
%% Internal exports
%%--------------------------------------------------------------------
-export([init/1]).

%%--------------------------------------------------------------------
%% Macros
%%--------------------------------------------------------------------
-define(SERVER, ?MODULE).

%%====================================================================
%% External functions
%%====================================================================
%%--------------------------------------------------------------------
%% @doc Starts the supervisor.
%% @spec start_link(StartArgs) -> {ok, pid()} | Error
%% @end
%%--------------------------------------------------------------------
start_link(_StartArgs) ->
    io:format("~p:start_link/1 !~n", [?MODULE]),
%    supervisor:start_link(?MODULE, []).
    supervisor:start_link({local, ?SERVER}, ?MODULE, []).
start_link() ->
    io:format("~p:start_link/0 !~n", [?MODULE]),
%    supervisor:start_link(?MODULE, []).
    supervisor:start_link({local, ?SERVER}, ?MODULE, []).
start(_Args) ->
    io:format("~p:start !~n", [?MODULE]),
%    supervisor:start_link(?MODULE, []).
    case supervisor:start_link({local, ?SERVER}, ?MODULE, []) of
	{ok, Pid } ->
%	    io:format("error:~p at ~p.~n", [Pid, ?MODULE] ),
	    {ok, Pid };
	{error, Error} ->
	    io:format("error:~p at ~p.~n", [Error, ?MODULE] ),
	    {error, Error}
    end.

%%====================================================================
%% Server functions
%%====================================================================
%%--------------------------------------------------------------------
%% Func: init/1
%% Returns: {ok,  {SupFlags,  [ChildSpec]}} |
%%          ignore                          |
%%          {error, Reason}
%%--------------------------------------------------------------------
init([]) ->
    io:format("~p started !~n", [?MODULE]),
    RestartStrategy    = one_for_one,
    MaxRestarts        = 1000,
    MaxTimeBetRestarts = 3600,
    SupFlags = {RestartStrategy, MaxRestarts, MaxTimeBetRestarts},
    ChildSpecs =
    [
       {otp_test_fsm,
        {otp_test_fsm, start, []},
        permanent,
        1000,
        worker,
        [otp_test_fsm]}
%       {otp_test_server,
%%        {otp_test_server, start_link, []},
%%        permanent,
%%        1000,
%%        worker,
%%        [otp_test_server]}
     ],
    {ok,{SupFlags, ChildSpecs}};
init(_) -> 
    io:format("...~p started !~n", [?MODULE]),    
    {error, bad_argument}.
