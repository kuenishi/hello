#!/usr/bin/env escript

main([])->
    code:add_path( "./ebin" ),
    application:load(otp_test),
    application:start(otp_test),
    io:format("!!!!!!!===================~n", [] ),
    application:stop(otp_test),
    application:unload(otp_test).
