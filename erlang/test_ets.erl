-module(test_ets).
-export([start/0]).
-compile( export_all ).
-author(kuenishi@gmail.com).

% hello.erl
%
% Compilation:
% $ erlc hello.erl

start()->
    lists:foreach( fun test_ets/1,
		   [set, ordered_set, bag, duplicate_bag]).

test_ets(Mode)->
    TableId = ets:new(test, [Mode]),
    ets:insert(TableId, {"/usr/local/hoge",1}),
    ets:insert(TableId, {"/usr/local/huga",2}),
    ets:insert(TableId, {"/usr/local",1}),
    ets:insert(TableId, {"/aasfd",3}),
    List=ets:tab2list(TableId),
    io:format("~-13w => ~p~n", [Mode, List]),
    ets:delete(TableId).


hello()->
    io:format("Hello world!~n").

