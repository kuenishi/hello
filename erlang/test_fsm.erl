-module(test_fsm).
-author('kuenishi@gmail.com').
-behaviour(gen_fsm).

%%finite state machine sample
%% see also: $ erl -man gen_fsm


%%  STATE DIAGRAM OF AN INFANT: any other state transition is ignored.
%%
%%   +--------> crying <-------+
%%   |            |            |
%%   |            | praise!    | scare!
%%   |timeout     |            |
%%   | (10sec)    +-> happy ---+
%%   |                  | <----+
%%   |           timeout|      |
%%   |            (5sec)|      | praise!
%%   |                  |      | 
%%   +--------- sad <---+      |
%%               |             |
%%               +-------------+

-compile(export_all).

main(_)->
    io:format("hoge~p~n",[aho]).

start() ->
    gen_fsm:start_link({local, ?MODULE}, ?MODULE, [], []).

stop() -> gen_fsm:send_all_state_event(?MODULE, stop).

scare() ->
    gen_fsm:send_event(?MODULE, scare).
praise() ->
    gen_fsm:send_event(?MODULE, praise).

current()->
    gen_fsm:send_all_state_event(?MODULE, current).

init(_) ->
    io:format("~p.~n",  [started]),
    {ok, crying, []}.

crying(praise, History) ->
    NewHistory = [crying|History],
    io:format("~p , ~p.~n", [crying, History]),
    {next_state, happy, NewHistory, 5000};
crying(_, History)->
    {next_state, sad, History}.

happy(scare, History)->
    NewHistory = [happy|History],
    io:format("~p , ~p.~n", [happy, History]),
    {next_state, crying, NewHistory};

happy(praise, History)->
    NewHistory = [happy|History],
    io:format("~p , ~p.~n", [happy, History]),
    {next_state, happy, History, 5000};

happy(timeout, History)->
    NewHistory = [happy|History],
    io:format("~p , ~p.~n", [happy, History]),
    {next_state, sad, NewHistory, 10000};
happy(_, History)->
    {next_state, happy, History, 5000}.

sad(praise, History)->
    NewHistory = [sad|History],
    io:format("~p , ~p.~n", [sad, History]),
    {next_state, happy, NewHistory, 5000};

sad(timeout, History) ->
    NewHistory = [sad|History],
    io:format("~p , ~p.~n", [sad, History]),
    {next_state, crying, NewHistory, 10000};
sad(_, History)->
    {next_state, sad, History, 10000}.

handle_event(stop, _StateName, StateData) ->
    {stop, normal, StateData};
handle_event(current, _StateName, StateData) ->
    {current, normal, StateData}.

terminate(normal, _StateName, _StateData) ->
    ok.
