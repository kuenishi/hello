-module(test_nif).
-export([init/0, hello/0, set/1, get/0, p/2]).

init() ->
    erlang:load_nif("./test_nif", 0).

hello() ->
    "NIF library not loaded".
set(_) ->
    "NIF library not loaded".
get() ->
    "NIF library not loaded".
p(_,_)->
    "NIF library not loaded".
