-module(test_server).
-author(kuenishi@gmail.com).

%-import([gen_tcp, lib_primes]).

-behaviour(gen_server).
-export([new_prime/1, start/0, stop/0]).

-export([init/1, handle_call/3, handle_cast/2, handle_info/2,
	 terminate/2, code_change/3]).
-compile( export_all ).

-define( DEFAULT_TIMEOUT, -1 ).

start()->
    gen_server:start_link({local, ?MODULE}, ?MODULE, [], []). 
stop()->
    gen_server:call(?MODULE, stop).
%    handle_call( stop, normal, 1).

init([])->
    process_flag(trap_exit, true),
    io:format("~p starting ~n", [?MODULE]),
    {ok,0}.

new_prime(N)->
    gen_server:call(?MODULE, {prime, N}, 20000).

handle_call( {prime, K}, From, N)->
    io:format("~p called, ~p.~n", [prime, K]),
    {reply, make_new_prime(K), N+1}.

handle_cast( Msg, N ) ->  { noreply, N }.
handle_info( Info, N)->   {noreply, N}.

terminate(Reason, N)->
    io:format("~p stopping:~p~n", [?MODULE, [N, Reason]]),
    ok.
    
code_change(OldVersion, N, Extra)->{ok, N}.
    
make_new_prime(K)->
    io:format("~p called, ~p.~n", [make_new_prime, K]),
    if
	K > 100 ->
	    alarm_handler:set_alarm(tooHot),
	    N=lib_primes:make_prime(K),
	    alarm_handler:clear_alarm(tooHot),
	    N;
	true ->
	    lib_primes:make_prime(K)
    end.


loop_accept(ListenSocket)->
    case gen_tcp:accept(ListenSocket, ?DEFAULT_TIMEOUT) of
	{ok, Socket}->
	    loop_recv(Socket),
	    gen_tcp:close(Socket);
	    %loop_accept(ListenSocket);
	{error, Reason}->
	    io:format("failed at accept:~p.~n", [Reason])
    end,
    gen_tcp:close(ListenSocket).

loop_recv(Socket)->
    case gen_tcp:recv(Socket, 3, ?DEFAULT_TIMEOUT) of
	{ok, Packet}->
	    gen_tcp:send(Socket, "aho"),
	    io:format("~p~n", Packet);
	{error, Reason}->
	    io:format("failed at recv:~p.~n", [Reason])
    end.
    


test_ets(Mode)->
    TableId = ets:new(test, [Mode]),
    ets:insert(TableId, {"ahoge",1}),
    ets:insert(TableId, {"bdesu",2}),
    ets:insert(TableId, {"ahoge",1}),
    ets:insert(TableId, {"aasfd",3}),
    List=ets:tab2list(TableId),
    io:format("~-13w => ~p~n", [Mode, List]),
    ets:delete(TableId).


hello()->
    io:format("Hello world!~n").

