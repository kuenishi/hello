-module(test_tcp).

-compile( export_all ).
-author(kuenishi@gmail.com).
-import(gen_tcp).


-export([start/2, process/1]).

% hello.erl
%
% Compilation:
% $ erlc hello.erl
start(Num,LPort) ->
    case gen_tcp:listen(LPort,[{active, false},{packet,2}]) of
        {ok, ListenSock} ->
            start_servers(Num,ListenSock),
            {ok, Port} = inet:port(ListenSock),
            Port;
        {error,Reason} ->
            {error,Reason}
    end.

start_servers(0,_) ->
    ok;
start_servers(Num,LS) ->
    spawn(?MODULE,server,[LS]),
    start_servers(Num-1,LS).
                       
server(LS) ->
    case gen_tcp:accept(LS) of
        {ok,S} ->
            loop(S),
            server(LS);
        Other ->
            io:format("accept returned ~w - goodbye!~n",[Other]),
            ok
    end.

loop(S) ->
    inet:setopts(S,[{active,once}]),
    receive
        {tcp,S,Data} ->
            Answer = process(Data), % Not implemented in this example
            gen_tcp:send(S,Answer),
            loop(S);
        {tcp_closed,S} ->
            io:format("Socket ~w closed [~w]~n",[S,self()]),
            ok
    end.

process(Data)->
    io:format("[~p]recv.~n", [Data]),
    "ans:hogeeee".

%%A simple client could look like this:

client(PortNo,Message) ->
    {ok,Sock} = gen_tcp:connect("localhost",PortNo,[{active,false},
                                                    {packet,2}]),
    gen_tcp:send(Sock,Message),
    A = gen_tcp:recv(Sock,0),
    gen_tcp:close(Sock),
    io:format("~p~n", [A]).

massive_client(0, Message) ->
    {ok, Message};
massive_client(N, Message)->
    client( 3000, Message ),
    massive_client( N-1, Message).

%% -define( DEFAULT_TIMEOUT, -1 ).

%% start()->
%%     case gen_tcp:listen(3000, [binary, {packet, line}, {active, false}, {reuseaddr, true}]) of
%% 	{ok, ListenSocket}->
%% 	    loop_accept(ListenSocket);
%% 	{error, Reason} ->
%% 	    io:format("failed at listen:~p.~n", [Reason])
%%     end.

%% loop_accept(ListenSocket)->
%%     case gen_tcp:accept(ListenSocket, ?DEFAULT_TIMEOUT) of
%% 	{ok, Socket}->
%% 	    loop_recv(Socket),
%% 	    gen_tcp:close(Socket);
%% 	    %loop_accept(ListenSocket);
%% 	{error, Reason}->
%% 	    io:format("failed at accept:~p.~n", [Reason])
%%     end,
%%     gen_tcp:close(ListenSocket).

%% loop_recv(Socket)->
%%     case gen_tcp:recv(Socket, 3, ?DEFAULT_TIMEOUT) of
%% 	{ok, Packet}->
%% 	    gen_tcp:send(Socket, "aho"),
%% 	    io:format("~p~n", Packet);
%% 	{error, Reason}->
%% 	    io:format("failed at recv:~p.~n", [Reason])
%%     end.
    


%% test_ets(Mode)->
%%     TableId = ets:new(test, [Mode]),
%%     ets:insert(TableId, {"ahoge",1}),
%%     ets:insert(TableId, {"bdesu",2}),
%%     ets:insert(TableId, {"ahoge",1}),
%%     ets:insert(TableId, {"aasfd",3}),
%%     List=ets:tab2list(TableId),
%%     io:format("~-13w => ~p~n", [Mode, List]),
%%     ets:delete(TableId).


%% hello()->
%%     io:format("Hello world!~n").

