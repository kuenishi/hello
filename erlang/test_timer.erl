-module(test_timer).

-export([my_thread/2, wait_thread/1]).

my_thread(Pid, T)-> Pid ! {new, T}.


wait_thread(T) ->
    receive
	{new, N} ->
	    wait_thread(N)
	after T ->
		io:format( "~p.~n", [T] ),
		wait_thread(T)
	end.
