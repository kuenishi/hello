package org.bitbucket.kuenishi.hello;

import io.grpc.Metadata;
import io.grpc.ServerCall;
import io.grpc.ServerCallHandler;
import io.grpc.ServerInterceptor;
import io.grpc.stub.ServerCalls;

// No auth for now
public class KnapsackAuth implements ServerInterceptor {
    @Override
    public <ReqT, RespT> ServerCall.Listener<ReqT> interceptCall(ServerCall<ReqT, RespT> call, Metadata headers, ServerCallHandler<ReqT, RespT> next) {
        System.err.println("intercepted ><" + call.getAuthority() + "|||attr:"+ call.getAttributes() + "||||headers:" + headers.toString() + "|||next:" + next.toString());

        // let it fail!!
        //throw new AssertionError("hey no chance!");

        return next.startCall(call, headers);
    }
}
