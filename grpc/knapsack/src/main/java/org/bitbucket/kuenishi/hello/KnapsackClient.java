package org.bitbucket.kuenishi.hello;

import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.StatusRuntimeException;

import java.io.Closeable;
import java.util.Iterator;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;

public class KnapsackClient implements Closeable {
    private final ManagedChannel channel;
    private final KnapsackGrpc.KnapsackBlockingStub blockingStub;

    /**
     * Construct client connecting to HelloWorld server at {@code host:port}.
     */
    public KnapsackClient(String host, int port) {
        this(ManagedChannelBuilder.forAddress(host, port)
                // Channels are secure by default (via SSL/TLS). For the example we disable TLS to avoid
                // needing certificates.
                .usePlaintext(true)
                .build());
    }

    /**
     * Construct client for accessing RouteGuide server using the existing channel.
     */
    KnapsackClient(ManagedChannel channel) {
        this.channel = channel;
        blockingStub = KnapsackGrpc.newBlockingStub(channel);
    }

    public void close() {
        try {
            channel.shutdown().awaitTermination(5, TimeUnit.SECONDS);
        } catch (InterruptedException e) {

        }
    }

    public boolean ping() throws Exception {
        PingRequest request = PingRequest.newBuilder().build();
        PingResponse response = blockingStub.ping(request);
        return response.getPong();
    }

    public void solve(int i) throws Exception {
        Problem problem  = Problem.newBuilder().setId(i).build();
        Iterator<Answer> answerIterator = blockingStub.solve(problem);
        while(answerIterator.hasNext()) {
            Answer answer = answerIterator.next();
            System.out.println(answer.getId());
        }
    }
    public static void main(String[] args) throws Exception {
        int ret = -1;
        try (KnapsackClient client = new KnapsackClient("127.0.0.1", 50002)) {
            if (args[0].equals("ping")) {
                if (client.ping()) {
                    System.out.println("ping");
                    ret = 0;
                } else {
                    System.err.println("pang");
                }

            } else if (args[0].equals("solve")) {
                System.out.println("solve");
                client.solve(42);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            System.exit(ret);
        }
    }
}
