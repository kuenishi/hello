package org.bitbucket.kuenishi.hello;

import io.grpc.Server;
import io.grpc.ServerBuilder;
import io.grpc.stub.StreamObserver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

public class KnapsackServer {
    private static final Logger LOG = LoggerFactory.getLogger(KnapsackServer.class);

    private int port = 50002;
    private Server server;

    private void start() throws IOException {
        server = ServerBuilder.forPort(port)
                .addService(new KnapsackImpl())
                .intercept(new KnapsackAuth())
                .build()
                .start();
        LOG.info("Server started, listening on " + port);

        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                // Use stderr here since the logger may have been reset by its JVM shutdown hook.
                System.err.println("*** shutting down gRPC server since JVM is shutting down");
                KnapsackServer.this.stop();
                System.err.println("*** server shut down");
            }
        });
    }

    private void stop() {
        if (server != null) {
            server.shutdown();
        }
    }

    private void blockUntilShutdown() throws InterruptedException {
        if (server != null) {
            server.awaitTermination();
        }
    }

    public static void main(String... argv) throws Exception {
        LOG.info("hello, knapsack!");
        final KnapsackServer server = new KnapsackServer();

        server.start();
        server.blockUntilShutdown();
    }

    static class KnapsackImpl extends KnapsackGrpc.KnapsackImplBase {
        @Override
        public void ping(PingRequest request, StreamObserver<PingResponse> responseObserver) {
            LOG.info("Request (ping)");
            PingResponse response = PingResponse.newBuilder().setPong(true).build();
            responseObserver.onNext(response);
            responseObserver.onCompleted();
        }

        @Override
        public void solve(Problem request, StreamObserver<Answer> responseObserver) {
            LOG.info("Request: ", request);
            new Thread(() -> {
                for (int i = 0; i < 10; i++) {
                    Answer answer = Answer.newBuilder().setId(i).build();
                    responseObserver.onNext(answer);
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        LOG.error("failed to sleep", e);
                    }                }
                responseObserver.onCompleted();
            }).start();
        }
    }
}
