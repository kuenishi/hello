'''
Example code for knapsack python client
'''


import grpc
import knapsack_pb2
import knapsack_pb2_grpc

channel = grpc.insecure_channel('localhost:50002')
stub = knapsack_pb2_grpc.KnapsackStub(channel)
response = stub.Ping(knapsack_pb2.PingRequest())
print("Response to ping: %s " % response.pong)


for answer in stub.Solve(knapsack_pb2.Problem(id=42)):
    if not answer: print(answer)
    print("answer=%s" % answer)