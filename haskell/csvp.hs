import Text.ParserCombinators.Parsec

main = putStrLn $ show $ parse_csv "'hoge',\"aaa asfd \"\nha ge, ge\n"

csvFile = endBy line eol

line = sepBy cell (char ',')
cell = quotedCell <|> quotedCell2 <|> many (noneOf ",\n\r")

quotedCell = do
  char '"'
  content <- many quotedChar
  char '"' <?> "quote at end of cell"
  return content
  
quotedCell2 = do
  char '\''
  content <- many1 quotedChar
  char '\'' <?> "quote at end of cell"
  return content

quotedChar = do  
  noneOf "\"\'" <|> try (string "\"\"" >> return '"')

eol = try (string "\n\r")
      <|> try (string "\r\n")
      <|> string "\n" 
      <|> string "\r" 
      <?> "couldn't find eol"

parse_csv input = parse csvFile "(unknown)" input
