import Text.ParserCombinators.Parsec

main = putStrLn $ show $ parse_csv "'hoge',\"aaa asfd \"\nha ge, ge\n"

data Expr a = Expr a
            | Term a 
            | EOF

jsonFile = endBy line eol

line = sepBy cell (char ',')
cell = quotedCell <|> quotedCell2 <|> many (noneOf ",\n\r")

quotedCell = do
  char '"'
  content <- many quotedChar
  char '"' <?> "quote at end of cell"
  return content
  
quotedCell2 = do
  char '\''
  content <- many1 quotedChar
  char '\'' <?> "quote at end of cell"
  return content

quotedChar = do  
  noneOf "\"\'" <|> try (string "\"\"" >> return '"')

eol = try (string "\n\r")
      <|> try (string "\r\n")
      <|> string "\n" 
      <|> string "\r" 
      <?> "couldn't find eol"

parse_json input = parse jsonFile "(unknown)" input
