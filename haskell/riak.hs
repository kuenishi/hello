import qualified Network.Riak.Basic as RB
import qualified Network.Riak.Types as RT
import Network.Riak.Connection (
       defaultClient, connect, disconnect,
       exchange, exchangeMaybe, exchange_
       )
import Network.Riak.Request (
       ping, put, get
       )
import qualified Network.Riak.Response as Res
import Network.Riak.Content (
       binary
       )
-- import qualified Data.ByteString.Char8 as C
import qualified Data.ByteString.Lazy.Char8 as BS

main = do
     conn <- connect $ RB.Client "127.0.0.1" "8087" (BS.pack "testme");
     resp0 <- exchange_ conn ping;
     resp1 <- exchange conn $ put (BS.pack "foo") (BS.pack "bar") Nothing (binary $ BS.pack  "content") RT.Default RT.Default False
     resp2 <- exchangeMaybe conn $ get (BS.pack "foo") (BS.pack "bar") RT.Default;
     disconnect conn
     putStrLn $ show $ Res.get resp2
--         Just (seq_content, vclock) -> putStrLn $ show seq_content
--         Nothing -> putStrLn "nothing"

