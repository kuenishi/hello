#!/bin/sh

#autoconf
#automake -a

if [ -e configure ]; then
    autoreconf
    ./configure
    make
else
    aclocal
    autoheader
    autoconf
    libtoolize --automake
    automake -a
    ./configure
    make
fi