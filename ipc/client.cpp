#include "client.h"
#include <fcntl.h>
#include <sys/types.h>
#include <stdexcept>
#include "util.h"
#include <config.h>
#include <sstream>

using std::runtime_error;
using namespace my;
using namespace std;

// ClientSocket::ClientSocket():init(){ }
ClientSocket::ClientSocket(const agent_model& am){
  stringstream s;
  s << am.port_;
  Open2( am.hostname_, s.str() );
  // init();
  // Open( host(am.hostname_), port(am.port_) );
}
//  ClientSocket::ClientSocket(const ipaddress& addr, const port& p){
//    init(); Open(addr, p);
//  }
ClientSocket::ClientSocket(const string& h, const string& p){
  //  init(); Open( h, p );
  Open2(h, p);
}

void ClientSocket::Open2(const string& h, const string& p){
  struct addrinfo hints, *res0, *res;
  memset(&hints, 0, sizeof(hints));
  hints.ai_socktype = SOCK_STREAM;
  hints.ai_family = AF_INET;
  hints.ai_protocol = IPPROTO_TCP;
  int err;
  if((err = getaddrinfo( h.c_str(), p.c_str(), &hints, &res0 )) != 0 or res0 == NULL){
    perror(__func__);
    throw exception();
  }
  
  for (res=res0; res!=NULL; res=res->ai_next) {
    d_ = socket(res->ai_family, res->ai_socktype, res->ai_protocol);
    if (d_ < 0) {
      continue;
    }
    
    if (connect(d_, res->ai_addr, res->ai_addrlen) != 0) {
      close(d_);
      continue;
    }
    break;
  }
  if (res == NULL) {
    /* 有効な接続が出来なかった */
    perror("connect");
    //    Log::ger::error( (h.name() + ": Connection may be refused @connect" ).c_str() );
    throw runtime_error(__func__);
  }
 freeaddrinfo(res0);
 // stringstream ss;
 // ss << "[" << typeid(this).name() << __func__ << " connected to " 
 //   << h << "]:" <<  p;
 // Log::ger::debug(ss.str().c_str());
 
 /* ここ以降にsockを使った通信を行うプログラムを書いてください */
 // close(sock);
}
ClientSocket::~ClientSocket(){
  close( d_ );
}

void ClientSocket::init(){
  memset( &addr_, 0, sizeof(struct sockaddr));
  d_ = socket(AF_INET, SOCK_STREAM, 0) ;
  if( d_ < 0 ){
    perror(__func__);
    throw runtime_error("can't create socket...");
  }
  addr_.sin_family = AF_INET;
}

int ClientSocket::Writeln(const string& s) const {
  return this->Write( s+"\r\n" );
}

void ClientSocket::SendBinary( const string& filename ){
  unsigned int s = my::getfilesize(filename.c_str());
  
  write(d_, (char*)&s, SIZEOF_UNSIGNED_INT);
  //printf( "s=%d r=%d\n", s, r );
  

  SOCKD from = 0, to = d_;
  from = open( filename.c_str(), O_RDONLY, 0);
  if( from <= 0 ){
    //    Log::ger::error(("! can't open file to be sent: usially libhoge.so.:" + filename).c_str());
    throw runtime_error("! can't open file to be sent: usially libhoge.so.:" + filename);
  }
  //  else
    //    Log::ger::debug("[ClientSocket::SendBinary]: start sending...");
  cerr << "[" << __func__ << "] start sending... " << flush;

   int wtotal = my::dd( from, to, s );

   unsigned static const int BUF_SIZE = 1024;
//   int rcount;
//   off_t  wtotal;
   char buf[BUF_SIZE];
//   char * bufp;
//   ssize_t wcount;
//   size_t wresid;
//  memset( buf, 0, BUF_SIZE);
//   wtotal = 0;
//   while((rcount = read( from, buf, BUF_SIZE )) > 0){
//     //cerr << rcount << " byte read." << endl;
//     for( bufp = buf, wresid = rcount; ;
// 	 bufp += wcount, wresid -= wcount){
//       wcount = write( to, bufp, wresid );
//       if( wcount <= 0 ) break;
//       wtotal += wcount;
//       //	if( info 
//       if( wcount >= (ssize_t)wresid){
// 	break;
//       }
//     }
//     //cerr << wcount << " byte sent." << endl;
//     if( wcount != (ssize_t)wresid){
//       break;
//     }
//   }
  //  write(to, bufp, -1 );
  memset( buf, 0, BUF_SIZE);
  sprintf(buf, " %s: %d bytes sent." , typeid(this).name(), (int)wtotal);
  //  Log::ger::debug(buf);
		  
  close( from );
}


///似非telnetクライアント till rev.30
//http://www.ueda.info.waseda.ac.jp/~toyama/network/example2.html
// int main(int args, char** argv){
//   typedef struct sockaddr_in SOCKADDR;
