#ifndef CLIENT_H__
#define CLIENT_H__

//#include <iostream>

#include "socket.h"


//sing namespace std;
namespace my{

  typedef struct agent_{
  public:
  agent_( const string& h, int p): hostname_(h), port_(p)
    {  };
    ~agent_(){};
    
    string hostname_;
    int port_;
  } agent_model;


  class ClientSocket: public AbstractSocket{
  public:
    ClientSocket(const agent_model&);
    ClientSocket(const string& h, const string & p );
    //    ClientSocket(const ipaddress&, const port&);
    //    ClientSocket(const host&, const port&);
    ~ClientSocket();
    void init();
    //    void Open(const ipaddress&, const port&);
    //    void Open(const string&, const port&);
    void Open2(const string&, const string&);
    int Writeln(const string&) const ;

    //sends binary file without opening/closing TCP session
    void SendBinary(const string& filename);
    
  private:
    ClientSocket(){};
  };
}

#endif
