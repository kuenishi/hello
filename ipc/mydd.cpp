#include <util.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

extern FILE* stderr;

void usage(){
  char mesg[] = "usage: $ mydd <fromfile> <tofile> <bytestocopy>"
    ;
  fprintf( stderr, "%s\n", mesg );
  exit(1);
}

typedef int FD;
int main(int args, char ** argv){

  if( args != 3 && args != 4)
    usage();

  int b = ( args == 4 )? atoi( argv[3] ): my::getfilesize(argv[1]);
  
  FD in = open( argv[1] , O_RDONLY );
  if( in < 0 ){
    perror(__FILE__);
    close(in);
    exit(1);
  }
  FD out = open( argv[2] , O_WRONLY|O_CREAT );
  if( out < 0 ){
    perror("can't open outfile.");
    close(in); close(out);
    exit(1);
  }

  int r = my::dd( in, out, b );
  close(in);
  close(out);
  if( b == r ){
    printf( "copy success.\n");
  }else{
    printf( "%d/%d bytes copy success.\n", r, b);
  }
}
