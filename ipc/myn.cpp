#include <util.h>
#include <client.h>
#include <server.h>
#include <sessionhandler.h>
#include <Options.h>

extern FILE* stderr;
static const unsigned int DEFAULT_PORT=28000;

void usage(){
  char mesg[] = "usage:\n"
    "$ myn -s -p <port>\n"
    "$ myn -c -p <port>\n"
    ;
  fprintf( stderr, "%s\n", mesg );
  exit(1);
}

class samplesh: public my::SessionHandler{
public:
  samplesh(int fd):SessionHandler(fd){};
  virtual ~samplesh(){};
  int parse_request_(const string& s){ 
    return     Write(s);
  };

};

class nserver: public my::AbstractServer{
public:
  nserver(const string& p): AbstractServer(p, ""){};
  virtual ~nserver(){};
  virtual void new_session(int fd){
    samplesh s(fd);
    s.start_thread();
    //    sh.detach();
  };
};

int main(int args , char ** argv){
  Options opt(args, argv);
  if( not(opt.has("s") xor opt.has("c") ) )
    usage();
  string port = (opt.has("p"))?opt.getLatest():"28000";

  //server routine
  if(opt.has("s")){
    nserver srv(port);
    srv.Start();
  }

  //client routine
  my::ClientSocket cs("localhost", port);
  cs.Write("hoge");
  char * mesg = cs.Read();
  printf("%s: %s\n", port.c_str(), mesg );
}
