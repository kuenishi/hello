#include <mythread.h>
#include <signal.h>

//#define _DEBUG_THREAD
#ifdef _DEBUG_THREAD
#include <iostream>
using namespace std;
#endif

using namespace my;

thread::thread(): tid_(0), finished_(false), running_(false){
#ifdef _DEBUG_THREAD
  cerr << "thread created: " << __func__ << endl;
#endif
}

thread::~thread(){
  //  if(running_ and not finished_)
  //  join_thread();
#ifdef _DEBUG_THREAD
  cerr << "thread destructed: " << __func__ << endl;
#endif
}
#include <stdio.h>
void* thread::start_routine(void* arg){
  if(arg == 0)  return 0;
  thread* p = static_cast<thread*>(arg);
  p->run_thread();
  // if(p->finished()){
  //   delete p;
  //   p = NULL;
  // }
  return arg;
}

int thread::start_thread(){
  if(running_ )
    return -1;
  else
    return::pthread_create(&tid_, 0, thread::start_routine, this);
}

int thread::join_thread(){
  return pthread_join(tid_, 0);
}

//
int thread::run_thread(){
  running_ = true;
  int r = run();
  running_ = false;
  finished_ = true;
  return r;
}

int thread::detach(){
  return pthread_detach(tid_);
}
int thread::kill(){
  return pthread_kill(tid_, SIGTERM);
}

//virtualized
// int thread::run(){
//   return 0;
// }
