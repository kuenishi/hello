#ifndef MYTHREAD_H
#define MYTHREAD_H

#include <pthread.h>

//thx2: http://www-online.kek.jp/~keibun/pukiwiki/index.php?C%2B%2B%A4%C8thread

namespace my{
  class thread{
  private:
    static void* start_routine(void *);
  
  public:
    thread();
    virtual ~thread();
    int start_thread();     ///start_threadでスレッドを起動できる
    int run_thread();     ///スレッドなしで処理を開始できる。

    int join_thread();
    int detach();
    int kill();

    bool finished(){ return finished_; };
    bool started(){ return running_; };
    virtual int run() = 0; //処理する中身を書く
  
  protected:
    pthread_t tid_;
    bool finished_;
    bool running_;
  };
};

#endif
