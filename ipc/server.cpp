#include "server.h"
#include "util.h"
//#include <errno.h> //extern int errno;

using namespace std;
using namespace my;

AbstractServer::AbstractServer(const string& p, const string& path, const int& max_con):
  sockname_(SOCK_NAME_BASE+path ),
  max_connection_(max_con), port_(p)
{
  d_ = socket( PF_INET, SOCK_STREAM, 0 ); //O for ip
  if( d_ < 0 ){
    const char* msg  = "can't open socket";
    //    Log::ger::fatal(msg);
    perror(msg);
    throw exception();
  }
  memset( &addr_, 0, sizeof(struct sockaddr));
  memset( &caddr_, 0, sizeof(struct sockaddr));
  
  // ソケットの名前を入れておく
  addr_.sin_family = AF_INET;
  addr_.sin_addr.s_addr = htonl(INADDR_ANY);
  addr_.sin_port = htons( atoi(p.c_str()) );
  //  strncpy(addr_.sin_path, SOCK_NAME.c_str(), SOCK_NAME.size() );
  
  // ソケットにアドレスをバインドする。bind()が失敗しないよう、最初にunlink()を
  // 使用して、ソケットに対応するファイルを消去する。
  unlink(sockname_.c_str());
  if (bind(d_, ( struct sockaddr *)&addr_, 
	   sizeof(addr_)/*.sin_family) + sizeof(sock_addr.sin_path)*/
	   ) < 0
      ){
    perror("bind");    throw exception();
  }else{
    cout << "[bind " << green("OK") << "]" << flush;
  }
  if (listen(d_, max_connection_) < 0) {
    perror("listen");   throw exception();
  }else{
    cerr << "[listen " << green("OK") << "]" << flush;
  }
  cerr << "[port " <<  port_ << ": " << green("OK") << "]" << endl;
  //  Log::ger::info("now listening.");
}
AbstractServer::~AbstractServer(){ close(d_); }

void AbstractServer::Start(){
  SOCKLEN len = sizeof(caddr_);
  while( true ){//    i++;
    len = sizeof(caddr_);
    int fd2;
    // if ((fd2 = accept(d_, (struct sockaddr *)&caddr_, &len)) < 0) {
    if(( fd2 = accept(d_, NULL, NULL)) < 0){
      perror("accept");
      //   throw runtime_error("sth wrong wigh accept.");
    //   //logger("sth wrong wigh accept.");
    }
    new_session(fd2);
  }
  cerr << "never reaches here:" << __FILE__ << __LINE__ << endl;
}

//似非鸚鵡返しサーバー till rev.30
//http://www.ueda.info.waseda.ac.jp/~toyama/network/example2.html
//int old_main(int args, char** argv){
