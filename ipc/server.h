#ifndef SERVER_H__
#define SERVER_H__



#include <iostream>
#include <sys/types.h>
#include <sys/socket.h>

#include <sys/un.h>
#include <netinet/in.h>
#include <string>
#include <iostream>
#include <socket.h>
#include <vector>

//#include <logger.h>

using namespace std;
static const std::string SOCK_NAME="/tmp/socketooo";
static const unsigned int SERVER_PORT = 5001;

namespace my{
  static const std::string SOCK_NAME_BASE="/tmp/socketooo";
  static const int DEFAULT_MAX_CONNECTION=500;

  class AbstractServer{
  public:
    AbstractServer(const string&, const string& = "/tmp",
		   const int& = DEFAULT_MAX_CONNECTION);
    virtual ~AbstractServer();

    //    void Open(const ipaddress&, const port&);
    virtual void Start();
    virtual void new_session(int ) = 0;

  protected:
    const string sockname_;
    const int max_connection_;
    const string port_;
    SOCKADDR caddr_;
    struct sockaddr_in addr_;
    SOCKD d_;
    AbstractServer();
  };
}



#endif
