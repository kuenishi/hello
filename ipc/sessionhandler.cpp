#include "sessionhandler.h"

#include <sstream>//for parse_request

#include <fcntl.h> //for recieve_file
#include <sys/types.h>
#include <signal.h> //for kill
#include "util.h"
#include <config.h>

using namespace my;

SessionHandler::SessionHandler(const SOCKD& fd):
  AbstractSocket(), thread(),  MAX_LEN_(1024)//, libfile_(libfile), data_(data), entrypoint_(ep)
{ d_=fd;
} 
SessionHandler::~SessionHandler(){}

int SessionHandler::Writeln(const string& line) const {
  return this->Write( line+"\r\n" );
}


//static const unsigned int BUF_SIZE = 1024;
//最初の4バイトはファイルサイズ。
void SessionHandler::recieve_file_( const string& filename )const{
  unsigned int l;
  if( read(d_, (char*)&l, SIZEOF_UNSIGNED_INT)  < 0 ){
    perror("something wrong at recieve_file_.");
  }
  //int ret=recv(fd_, buf, MAX_LEN_, 0);
  string filename_with_path("./");
  //  Log::ger::debug("recieving file...");
  
  filename_with_path += filename;
  SOCKD from = d_;
  int to = open( filename_with_path.c_str(), O_WRONLY|O_CREAT,
		 S_IWUSR|S_IRUSR|S_IRGRP|S_IROTH);//00644 permission
  if( to < 0 ){
    //    Log::ger::error("can't open file to save.");
    throw std::exception();
  }

  int wtotal = my::dd( from, to, l );

  close(to);
   //#ifdef _DEBUG
  std::stringstream ss;
  ss  << __func__ << ": totally " <<  wtotal << " bytes read.";
  //  Log::ger::debug(ss.str().c_str());
  //#endif

}

void SessionHandler::send_ack() const {
  this->Writeln("OK");
}
void SessionHandler::send_nack() const {
  this->Writeln("NG");
  //  this->Writeln_("PERDON?");
}
int SessionHandler::run(){
  //logger("entered into session handler::run");
  //libfileをダイナミックロードする
  //エントリポイントentrypoint_.c_str()としてdlsym
  //データは引数で食わせる//雛形ライブラリを作っておく
  //結果を返送する
  //サーバプロセスには返らないので、longjmpを使うとjoinとかしなくていいかもしれない
  
  // START_SESSION://gotoじゃなくて継続で書くといいんだろうけど。。。
  char* request =  Read();
  //printf("%s\n", request);
  parse_request_(request);
  delete request;
  return 0;
}
