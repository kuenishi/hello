#ifndef SESSIONHANDLER_H
#define SESSIONHANDLER_H

#include <string>
#include <mythread.h>
#include <socket.h>
using std::string;

namespace my{
  class SessionHandler: public AbstractSocket, public thread{
  public:
    SessionHandler( const SOCKD& );
    //SessionHandler(const string&, const string&, const string& = "enter");
    virtual ~SessionHandler();
    virtual int parse_request_(const string& )=0;
    //virtual void process_()=0;

    int run();

    void recieve_file_(const string&) const ;
    void send_ack() const ;
    void send_nack() const ;

    //char* Read(const int& ) const ;
    //char* Read() const ;
    int Writeln(const string&) const ;
    //    int Write(const string&) const ;
    //int Write(const char*) const ;

  protected:
    const int MAX_LEN_;// = 1024;
    string libfile_;
    string entrypoint_;
  };
}

#endif
