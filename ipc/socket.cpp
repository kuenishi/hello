#include "socket.h"
//#include <sstream>
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif
#include <string>
using std::string;
#include <util.h>

using namespace my;
//void ClientSocket::close(){}

//this is the only method that calls write(2).
int AbstractSocket::Write(const char*  s) const {
  return Write(string(s));
}
int AbstractSocket::Write(const string& s) const {

  //first send length as int;
  char* lch = uint2char( s.size() ); //  memcpy( lch, &l, SIZEOF_UNSIGNEDp_INT );
  int r = write(d_, lch, SIZEOF_UNSIGNED_INT);
  // for( int i = 0; i < 4 ; ++i){
  //   printf("%#x ", lch[i] );
  // }
  //printf( " %d|%s|%s\n", r, lch, s.c_str() );
  size_t num_bytes_written = 0;
  
  bool done = false;
  while (num_bytes_written < s.size()) {
    ssize_t len;
    //do {
      // TODO: timeout is not handled here.
    len = write(d_, s.data() + num_bytes_written,
		s.size() - num_bytes_written);
    done =  (len + num_bytes_written >= s.size());
    
    //} while (len <= 0 );// && errno == EINTR);
    if (len < 0) {  // Ignore errors.       //cerr << __func__ << "err: sent: " << s <<  endl;
      break;
    }
    num_bytes_written += len;
  }
  //return this->Write( s.c_str() );
//   std::stringstream ss; 
//   ss << "[" << typeid(this).name() << __func__ << "]: " 
//      << s.size() << " bytes sent." << ((s.size() < 15)? s : "");
  //  Log::ger::debug(ss.str().c_str());
  return (int)num_bytes_written;
}

char* AbstractSocket::Read(const int & bytes_to_read) const {
  //printf("start reading at %s\n", __func__ );
  int bytes_read = 0;
  char *buf = (char*)malloc(sizeof(char)*( bytes_to_read+1));
  memset( buf, 0, bytes_to_read);
  while( bytes_read < bytes_to_read ){
    int ret=read(d_, buf + bytes_read, bytes_to_read - bytes_read);
    //    printf("%s\n", buf);
    bytes_read += ret;
    //    if( ret < 0 ){     }
  }
  buf[bytes_to_read] = '\0';
  if( bytes_to_read < 50 )
    //    Log::ger::debug( buf );
  return buf;
}
char * AbstractSocket::Read()const{
  unsigned int l;
  if( read(d_, (char*)&l, SIZEOF_UNSIGNED_INT)  < 0 ){
    perror("something wrong at Read.");
  }
  if( l < 0 or 0xffffffff < l );
//   std::stringstream ss; 
//   ss << "[" << typeid(this).name() << "]: reading " << l << "bytes..." ;
  //  Log::ger::debug(ss.str().c_str());
  //  printf("[%s#%s]: reading %d bytes... \n", typeid(this).name() , __func__, l );
  return this->Read(l);
}
