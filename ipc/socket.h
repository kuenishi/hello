#ifndef SOCKET_H__
#define SOCKET_H__

#include <iostream>
#include <sys/types.h>
#include <sys/socket.h>

#include <sys/un.h>
#include <netinet/in.h>
#include <netdb.h>

#ifdef _DEBUG
#include <iostream>
using namespace std;
#endif

#include <exception>
#include <string>
//#include "logger.h"

using std::string;
//static const std::string SOCK_NAME="/tmp/socketooo";

//#include "ipaddress.h"

namespace my{
  typedef struct sockaddr_in SOCKADDR; //sockaddr/inet
  typedef socklen_t SOCKLEN; //sock_addr length type
  typedef int       SOCKD;  //socket discriptor

  ///abstract class socket
  class AbstractSocket{
  public:
    AbstractSocket(SOCKD d):d_(d){};
    virtual ~AbstractSocket(){ this->Close(); };
    // virtual void Open(const ipaddress&, const port&)=0;
    virtual int Write(const string&)const;
    virtual int Write(const char*)const;
    virtual char* Read(const int &) const;
    virtual char* Read() const ;
    virtual void Close(){ 
      //      cerr << typeid(this).name() << ": socket closed." << endl;
      close(d_); 
    };

    SOCKD get_d() const { return d_; };
    bool operator ! (){ return false; };

  protected:
    AbstractSocket(){};
    struct sockaddr_in addr_;
    //SOCKADDR addr_;
    SOCKD d_;
  };
  
  
};

#endif
