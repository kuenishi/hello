#ifndef UTIL_H
#define UTIL_H

#include <string>
using std::string;

namespace my{

  ///strip path and get pure filename to save in.
  string getfilename(const string& );
  string getfilename(const char* );
  unsigned int getfilesize(const char* );
  char* uint2char(const size_t & );
  unsigned int char2uint(const char*);
  bool have_file(const string& );
  string green(const char*);
  string red(const char*);
  
  static const char letters[] = "0123456789"
    "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
    "abcdefghijklmnopqrstuvwxyz"
    "_-";
  class rand{
  public:
    rand();
    ~rand();
    //   get random char* string by using...
    //*1. /dev/urandom    //better rand and fast and non-deterministic
    // 2. /dev/random     //best rand but slow and non-deterministic
    // 3. rand(3)         //worst rand and deterministic and not thread-safe.
    // 4. random(3)       //worse rand and deterministic
    char* str(unsigned int);
    int src_fd_;
    int l_;
    //char table which you want to use in output string

  };

  typedef int FD;
  int dd(FD from, FD to, unsigned int length, const int BUF_SIZE=65536);

  //ファイルディスクリプタd_からlバイト確実に読み込む
  char* dd(FD from, unsigned int length );

  //ファイルディスクリプタd_にchar*を確実に書き込む
  //int dd(FD to, const char * );
};

#endif
