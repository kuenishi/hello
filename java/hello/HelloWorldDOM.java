package net.kuenishi.hello.java;
import java.io.*;
import javax.xml.parsers.*;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

//TODO パスの通ったxmlファイルをbuilder.parseに渡してやること
public class HelloWorldDOM {
  public static void main (String[] args) {
    try {
      // ドキュメントビルダーファクトリを生成
      DocumentBuilderFactory dbfactory = DocumentBuilderFactory.newInstance();
      // ドキュメントビルダーを生成
      DocumentBuilder builder = dbfactory.newDocumentBuilder();
      // パースを実行してDocumentオブジェクトを取得
      System.out.println("start parsing...");
      Document doc = builder.parse(new File("xml/A01F0055.xml"));
      System.out.println("parsing finished.");
      // ルート要素を取得（タグ名：message）
      Element root = doc.getDocumentElement();
      // 最初の子ノード（テキストノード）の値を表示
      System.out.print(root.getFirstChild().getNodeValue());
	  NodeList nl = doc.getChildNodes();
	  for(int i=0;i<nl.getLength();i++)
		  child(0, nl.item(i));

    } catch (Exception e) {
      e.printStackTrace();
    }
  }
  public static void child(int depth, Node n){
	  for(int i=0;i<depth;i++)
		  System.out.print(" ");
	  System.out.println(n.getNodeName());
	  NodeList nl = n.getChildNodes();
	  if(nl != null)
		  for(int i=0;i<nl.getLength();i++)
			  child(depth+1, nl.item(i));
  }
}