package net.kuenishi.hello.java;
import java.net.*;
import java.io.*;

public class HelloWorldGET {
  public static void main(String[] args) {
    try {
      // URLクラスのインスタンスを生成
      URL helloURL = 
         new URL("http://nausicaa/~kuenishi/");
      // 入力ストリームを生成
      BufferedReader in = new BufferedReader(
                                new InputStreamReader(
                                helloURL.openStream()));

      // 一行ずつ読み込みます
      String line;
      while ((line = in.readLine()) != null) {
        // 表示します
        System.out.println(line);
      }
    
      // 入力ストリームを閉じます
      in.close();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
}
