package net.kuenishi.hello.java;

import java.util.TreeMap;
import java.util.Map;
import java.util.Iterator;
import java.util.Set;


public class HelloWorldMap {
	static int N = 100;
	static int T = 10;
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		TreeMap[] tm = new TreeMap[T];
		for(int t = 0;t<T; t++){
			tm[t]=new TreeMap();
		
			for(int i=0;i<100;i++){
				String tmp = "i = "+(i+t);
				tm[t].put(tmp, new Integer(i));
			}
		}
		
		for(int t = 1; t<T; t++){
			Set s = tm[t].keySet();
			Iterator i = s.iterator();
		
			//merge 2 maps into 1 map
			while(i.hasNext()){
				String key = (String)i.next();
				//mergeCount(key, wc.get(key));
				//System.out.println(key+"\t"+tm[t].get(key));
				int value = ((Integer)tm[t].get(key)).intValue();
				if(tm[0].containsKey(key)){
					value += ((Integer)tm[0].get(key)).intValue();
				}
				tm[0].put(key, new Integer(value));
			}
		}

		Set s = tm[0].keySet();
		Iterator i = s.iterator();
	
		while(i.hasNext()){
			String key = (String)i.next();
			System.out.println(key+"\t"+tm[0].get(key));
		}
	}

}
