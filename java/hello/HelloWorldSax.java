package net.kuenishi.hello.java;

import javax.xml.parsers.*;
import org.xml.sax.*;
import org.xml.sax.helpers.*;
import java.io.*;

//TODO パスの通ったxmlファイルをparser.parseに渡してやること
public class HelloWorldSax extends DefaultHandler {
	public static String targetid = "A01F0055";
  public static void main(String[] argv) {
    try {
      // SAXパーサーファクトリを生成
      SAXParserFactory spfactory = SAXParserFactory.newInstance();
      // SAXパーサーを生成
      SAXParser parser = spfactory.newSAXParser();
      // XMLファイルを指定されたデフォルトハンドラーで処理します
      parser.parse(new File("xml/"+targetid+".xml"), new HelloWorldSax());
    } catch (Exception e) {
      e.printStackTrace();
    }
  }
  /**
   * ドキュメント開始時
   */
  public void startDocument() {
    System.out.println("ドキュメント開始");
  }
  /**
   * 要素の開始タグ読み込み時
   */
  public void startElement(String uri,
                           String localName,
                           String qName,
                           Attributes attributes) {

//    System.out.println("要素開始:" + qName+" localName:"+localName+" uri:"+uri);

	  if(qName == "LUW"){
		//  for(int i=0;i<attributes.getLength();i++)
	   // 	System.out.println(attributes.getQName(i)+"->"+attributes.getValue(i));		  
		  System.out.println(attributes.getValue("LUWLemma")+" ");
		//  attributes.ge);
	  }
	}
  /**
   * テキストデータ読み込み時
   */
  public void characters(char[] ch,
                         int offset,
                         int length) {

    //System.out.println("テキストデータ：" + new String(ch, offset, length));
  }
  /**
   * 要素の終了タグ読み込み時
   */
  public void endElement(String uri,
                         String localName,
                         String qName) {

    //System.out.println("要素終了:" + qName);
  
  }
  /**
   * ドキュメント終了時
   */
  public void endDocument() {
    System.out.println("ドキュメント終了");
  }
}