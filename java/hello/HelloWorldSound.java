package net.kuenishi.hello.java;

import java.io.File;
import javax.sound.sampled.*;


public class HelloWorldSound {
	 private static final int EXTERNAL_BUFFER_SIZE = 1024;//128000
	  public static void main(String[] args) {
	    try {
	    	System.out.println("hoge");
	    	
	      // Fileクラスのインスタンスを生成
	      File soundFile = new File("C:\\tmp\\v.wav");
	      // オーディオ入力ストリームを取得します
	      AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(soundFile);
	      // オーディオ形式を取得します
	      AudioFormat audioFormat = audioInputStream.getFormat();

	      // データラインの情報オブジェクトを生成します
	      DataLine.Info info = new DataLine.Info(SourceDataLine.class,audioFormat);
	      // 指定されたデータライン情報に一致するラインを取得します
	      SourceDataLine line = (SourceDataLine) AudioSystem.getLine(info);
	      // 指定されたオーディオ形式でラインを開きます
	      line.open(audioFormat);
	      // ラインでのデータ入出力を可能にします
	      line.start();

	      int nBytesRead = 0;
	      byte[] abData = new byte[EXTERNAL_BUFFER_SIZE];
	      while (nBytesRead != -1) {
	        // オーディオストリームからデータを読み込みます
	        nBytesRead = audioInputStream.read(abData, 0, abData.length);
	        if (nBytesRead >= 0) {
	          // オーディオデータをミキサーに書き込みます
	          int nBytesWritten = line.write(abData, 0, nBytesRead);
	          System.out.println(abData[0]+"\t"+nBytesWritten);
	        }
	      }
	      // ラインからキューに入っているデータを排出します
	      line.drain();
	      // ラインを閉じます
	      line.close();

	      System.out.println("hogehoge");
	      System.exit(0);
	    } catch (Exception e) {
	    	System.out.println("exception");
	      e.printStackTrace();
	      System.exit(1);
	    }
	  }
}
