//package net.kuenishi.jni;

public class HelloWorld{
    public native void printHello();
    
    static {
	System.loadLibrary("hello");
    }

    public static void main( String[] argv ){
	System.out.println( "testing jni..." );
	new HelloWorld().printHello();
    }
}
