#import <stdio.h>
#import <objc/Object.h>

// compilation in Mac
// $ gcc hello.m -framework Cocoa

// see
// http://wisdom.sakura.ne.jp/programming/objc/objc2.html

@interface Klass : Object
// instance variables here // 
{
  int n;
}
// member methods here
- ( void ) hello  ;
- ( int ) getN;
- ( void ) setN:(int)n;
- (id) free;
@end

@implementation Klass
- ( void ) hello {
  printf("self->n = %d, %s\n", self->n, __func__ );
  printf("ザ・ワールド 時よ止まれッ！\n");
  printf("WRYYYYYYYYYYYYーーーーッ\n");
}
- ( int ) getN {
  return n;
}
- ( void ) setN : (int)n_{
  self->n = n_;
}
- (id) free{
  [super free];
}
@end

int main() {
  id obj = [Klass alloc];
  [obj setN: 256];
  [obj hello];
  [obj free];
  return 0;
}

