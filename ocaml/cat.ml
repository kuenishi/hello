(* $ ocaml cat.ml
 * $ ./a.out some_text_file
 *)

let cat1 filename =
  let fin = open_in filename in
  let cat_sub () =
    while true do
      output_char stdout (input_char fin)
    done
  in
    try cat_sub () with End_of_file -> close_in fin;;

match Array.to_list Sys.argv with
  | [] -> []
  | hd::tl ->  List.map cat1 tl
;;


