#include <caml/mlvalues.h>
#include <caml/memory.h>
#include <caml/threads.h>

#define Nothing ((value) 0)

#include <caml/alloc.h>
#include <caml/fail.h>

#include <stdio.h>

#ifndef __APPLE__
#error "use MacOSX. *BSD and Linux is in progress"
#endif
//__GNUC__ 4
//__amd64 1
//__x86_64 1
//__x86_64__ 1

#define PIN \
  printf("%s:%d entered into %s\n", __FILE__, __LINE__, __func__ );
#define POUT \
  printf("%s:%d get out from %s\n", __FILE__, __LINE__, __func__ );
#define HERE(x) \
  printf("%s:%d in %s -> %p\n", __FILE__, __LINE__, __func__, x );

CAMLprim value hello(value arg){
  CAMLparam1(arg);
  printf("hello %s!\n", String_val(arg));
  CAMLreturn(Val_unit);
}

CAMLprim value env(void){
  CAMLparam0();
  CAMLreturn(caml_copy_string(__VERSION__));
}

CAMLprim value hoge(void){
  CAMLparam0();
  CAMLlocal2(v_res, v_flags);
  int i=10;
  PIN;
  //  printf("len=%d : %p\n", i, v_res);
  v_res = caml_alloc(i, 0);
  while (--i >= 0) {
    value v_ev;
    //    v_flags = caml_copy_int32(23);
    v_ev = caml_alloc_small(2,0); 
    Field(v_ev, 0) = Val_int(i);
    Field(v_ev, 1) = Val_int(23);
    printf("putting (%d,%d)\n", i, 23);
    Store_field(v_res, i, v_ev);
  }
  POUT;
  CAMLreturn(v_res);
}

