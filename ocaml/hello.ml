
external hello : string -> unit = "hello"
external env : unit -> string = "env"

external hoge : unit -> (int * int) array = "hoge"

let _ =
  hello "hoge";
  let l = Array.to_list (hoge()) in
  List.iter (fun (i,j) -> Printf.printf "(%d,%d)\n" i j) l;
  Pervasives.print_string "Hello World!\n";;
