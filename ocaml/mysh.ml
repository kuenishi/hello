(* http://ocaml.jp/um2010
   OCaml Meeting 2010 Golf competition code: 160 stroke.
   http://golf.shinh.org/p.rb?Sort+by+Length+for+OCaml+Golf+Competition *)
let rec(!)=String.length
and g l=match try read_line() with _->""with""->l;|w->g(w::l)in
List.map print_endline(Sort.list(fun l r->(!l= !r)&&l<r|| !l< !r)(g[]))
