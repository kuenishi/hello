(* input max integer which you want prime nums when you exec. *)

let rec is_prime i l =  match l with
    [] -> true  (* prime! *)
  | hd::tl ->
      if (i mod hd) = 0 then false
      else is_prime i tl;;

let rec continue_testing i l m = 
  if i >= m then l
  else if is_prime i l then 
    continue_testing (i+2) (i::l) m
  else
    continue_testing (i+2) l m;;

let main () = 
  let m = int_of_string Sys.argv.(1) in
  let list = continue_testing 3 [] m in
    Printf.printf "prime number from %d to %d is:\n" 3 m;
    Printf.printf " [ %d ,..., %d ]\n" 
      (List.hd (List.rev list)) (List.hd list );
    Printf.printf " with %d elements.\n" (List.length list);;
    
main();;
