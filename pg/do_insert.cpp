//see   http://www.enterprisedb.com/docs/jp/8.3/server/libpq-example.html
// $ gcc -lpq -I /opt/local/include

#include "trans.h"
#include "util.h"

int main(int args, char ** argv){
  int maxN = 65536;
  int M = 0;
  int i, j;
  int n = 0;
  int id;
  my::rand r;
  int N;

  //  int ids[N];
  struct timeval start, end;
  char key[] = "123456789abcdef";
  char value[] = "123456789abcdef"
    "123456789abcdef";
  
  connection c(NULL);
  my::timer tmr;
  extern PGconn * conn;
  conn = c();
  for(  N = 1 ; N <= maxN; N *= 10 ){
    tmr.start();
    char * k, * v;
    //select_test("");
    for( i=0; i<N; ++i ){ //
      c.begin_tx();
      //doc * d = select_doc(id);
      //select_docid(r.str(64));
      //printf("%d/%d \t...%d\n", i, N, id );
      //DEL_DOC(d);
      //    update_test( ids[i], r.str(128) );
      for(j=0;j<20;++j){
	k = r.str(32);
	v = r.str(64);
	id = simple_insert( k,v );
	free(k); free(v);
      }
      //  for( i=0; i<N; ++i )  update_test(i, "hoge");
      // insert_with( r.str(32), r.str(64) ); r.c();
      //      commit_transaction();
      c.end_tx();
    }
    
    tmr.stop();
    double t = tmr.time();
    // t/N tps achieved.
    printf( "%f tps for %d datas in %f secs; %f ms for each data.\n", N/t, N, t, 1000*t/N );

  }
  fin_conn();
  return 0;
}

