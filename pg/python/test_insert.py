#!/usr/bin/python
#ident "$Id: demo1b.py,v 1.2 2001/10/18 03:17:08 ballie01 Exp $"
#-----------------------------------------------------------------------+
# copied from demo1b.py - implement the sample program #1 from the PostgreSQL libpq	|
#	      doumentation using the PgSQL (DB-API 2.0 compliant) module|
#-----------------------------------------------------------------------+
import sys
from pyPgSQL import PgSQL

import string
import random

alphabets = string.digits + string.letters
def randstr(n):
    return ''.join(random.choice(alphabets) for i in xrange(n))

#dbname = 'template1'
dbname = 'kuenishi'
tablename= "tags"

# Make a connection to the database and check to see if it succeeded.
try:
    cnx = PgSQL.connect(database=dbname)
except PgSQL.Error, msg:
    print "Connection to database '%s' failed" % dbname
    print msg,
    sys.exit()

# Create a Cursor object.  This handles the transaction block and the
# declaration of the database cursor.  

cur = cnx.cursor()

# Fetch instances of pg_database, the system catalog of databases.
#"insert into list ( key, value ) values( '%s%d', '%s' )";
N = 65536 #256
try:
    for i in xrange(0,N):
        cur.execute("insert into list ( key, value ) values( '%s', '%s' )"
                   % (randstr(16), randstr(32)) )
#    cur.execute("SELECT * FROM %s" % tablename )
except PgSQL.Error, msg:
    print "insert failed\n%s" % msg,
    sys.exit()

# Close the cursor

cur.close()

# Commit the transaction

cnx.commit()

# That's all folks!

del cnx, cur
