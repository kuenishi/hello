DROP TABLE IF EXISTS tags CASCADE;
DROP TABLE IF EXISTS documents CASCADE;
DROP SEQUENCE IF EXISTS document_seq CASCADE;
DROP SEQUENCE IF EXISTS tag_seq CASCADE;


CREATE SEQUENCE document_seq
  increment    1
  start        1
;
CREATE SEQUENCE tag_seq
  increment    1
  start        1
;

CREATE TABLE documents (
      id INTEGER default nextval('document_seq'),
      key varchar(255) NOT NULL,
      value text,
      constraint document_c primary key(
        id
      )
);

CREATE TABLE tags (
       id INTEGER default nextval('tag_seq'),
       tag varchar(255) NOT NULL,
       document_id INTEGER NOT NULL,
       FOREIGN KEY (document_id)  REFERENCES documents (id),
       primary key(id) 
);
