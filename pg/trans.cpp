//see   http://www.enterprisedb.com/docs/jp/8.3/server/libpq-example.html
// $ gcc -lpq -I /opt/local/include

#include "trans.h"
#include "util.h"

const char *conninfo;
PGconn     *conn;

int check_error(const char* msg, PGresult* res){
  if (PQresultStatus(res) != PGRES_COMMAND_OK)      {
    fprintf(stderr, "%s command failed: %s", msg, PQerrorMessage(conn));
    PQclear(res);
    exit( -1 );
  }
}


int get_max_id(){
  PGresult * res = PQexec(conn, "SELECT MAX(id) FROM documents");
  int M = atoi( PQgetvalue(res, 0, 0) );
  PQclear(res);
  return M;
}
int get_new_id(){
  PGresult * res = PQexec(conn, sel4);
  int M = atoi( PQgetvalue(res, 0, 0) );
  PQclear(res);
  return M;
}

int creat_conn(char* s){
  if( s == NULL )
    //conninfo = "dbname=starfish hostaddr=192.168.0.28 port=5432 user=www";
    //      conninfo = "dbname=gizmo_test hostaddr=192.168.1.43 port=5432 user=gizmo_test";
    conninfo = "host=localhost ";//dbname=kuenishi"; // port=5432 "; //host=localhost ";
  //depending on environment, you have to change this char*
  else
    conninfo = s;
  
  conn = PQconnectdb(conninfo);
  if (PQstatus(conn) != CONNECTION_OK) {    /* バックエンドとの接続確立に成功したかを確認する */
    fprintf(stderr, "Connection to database failed: %s",  PQerrorMessage(conn));
    PQfinish(conn); //exit_nicely(conn);
    return -1;
  }
  PGresult   *res;
}
int begin_transaction(){    
  PGresult   *res;
  /* トランザクションブロックを開始する。 */
  res = PQexec(conn, "BEGIN");
  check_error( "BEGIN", res );
  PQclear(res);
  return 0;
}
int commit_transaction(){   //    トランザクションを終了する; このトランザクションをどういう単位で実行するか検討課題。
    PGresult   *res;
    res = PQexec(conn, "END");
    check_error( "END" , res );
    PQclear(res);
    //    PQfinish(conn);
}

int fin_conn(){
  PQfinish(conn);

}
doc* select_doc(int id){
    PGresult   *res;
    int         nFields, ntuples;
    int         i,   j;
    
    //    sel;
    char esql[strlen(sel) + 32];
    sprintf( esql, sel, id ); //__func__, counter++, s );
    res = PQexec( conn, esql );
    
//     /* まず属性名を表示する。 */
    nFields = PQnfields(res);
    ntuples = PQntuples(res);
     //     for (i = 0; i < nFields; i++)
     //    printf("%-15s", PQfname(res, i));
     //printf("\n\n");
    if( ntuples != 1 ){
      PQclear(res);
      return NULL;
    }
    
    doc * t = ALLOC_NEW_DOC;
    //     /* そして行を表示する。 */
    char * k = PQgetvalue(res,0,1); 
    char * v = PQgetvalue(res,0,2);
    t->key = (char*)malloc(sizeof(char)*(strlen(k)+1));
    t->value = (char*)malloc(sizeof(char)*(strlen(v)+1));
    strcpy( t->key, k );     strcpy( t->value, v );
    //    for (i = 0; i < PQntuples(res); i++)     {
    //      for (j = 0; j < nFields; j++)
    //	printf("%-15s", PQgetvalue(res, i, j));
    //      printf("\n");
    //    }
    PQclear(res);
    return t;
}
int select_docid(const char* key){
  PGresult   *res;
  int         nFields, ntuples;
  puts(key);
  const char* sql = sel;
  char esql[strlen(sql)+32];
  sprintf( esql, sql, "max(id)" ); //__func__, counter++, s );
  res = PQexec( conn, esql );
  check_error(esql, res);
  if( PQntuples(res) != 1 ){
    PQclear(res);
    return -1;
  }
  
  //     /* そして行を表示する。 */
  int ret = atoi(PQgetvalue(res,0,0)); 
    PQclear(res);
    return ret;
}

//insertion test
int simple_insert(char* key, char* value){
  PGresult   *res;
  // insert into list ( key, value ) values( %s, %s )
  //char sql[] = "insert into list ( key, value ) values( '%s', '%s' )";
  const char * sql = ins0;
  char esql[strlen(sql) + strlen(key) + strlen(value)];
  sprintf( esql, sql, key, value ); //__func__, counter++, s );
  res = PQexec( conn, esql );
  check_error( esql, res );
  PQclear(res);
}
int insert_test(char* key, char* value){
  PGresult   *res;
  int id = get_new_id();
  // insert into list ( key, value ) values( %s, %s )
  //char sql[] = "insert into list ( key, value ) values( '%s', '%s' )";
  const char * sql = ins;
  char esql[strlen(sql) + strlen(key) + strlen(value) + 16];
  sprintf( esql, sql, id, key, value ); //__func__, counter++, s );
  res = PQexec( conn, esql );
  check_error( esql, res );
  
  PQclear(res);
  return id;
}

static int lt = 3;
int insert_with(char* key, char* value){
  int id = get_new_id();
  PGresult   *res;
  
  // insert into list ( key, value ) values( %s, %s )
  //char sql[] = "insert into list ( key, value ) values( '%s', '%s' )";
  const char * sql = ins4;
  char esql[strlen(sql) + strlen(key) + strlen(value) + lt * 2 + sizeof(int) * 3];
  char tagk[lt+1];
  char tagv[lt+1];
  strncpy( tagk, key, lt );
  tagk[lt]=tagv[lt]='\0';
  strncpy( tagv, value, lt );
  sprintf( esql, sql, id, key, value , tagk, id, tagv, id ); //__func__, counter++, s );
  res = PQexec( conn, esql );
  check_error( esql, res );
  PQclear(res);

}
int insert_tag(int id){
  PGresult   *res;
  const char * sql = ins6;
  char esql[strlen(sql) + 16 * 2 + lt*2];
  sprintf( esql, sql, "tag", id, "agv", id ); //__func__, counter++, s );
  res = PQexec( conn, esql );
  check_error( esql, res );
  PQclear(res);
}

int update_test(int id, char* value){//int update_test(char * key, char* value){}
  PGresult   *res;
  const char * sql = upd2; //[] = "update list SET value ='%s' where id=%d";
  char esql[strlen(sql) + strlen(value) + 32 ];
  sprintf( esql, sql, value, id );
  res = PQexec( conn, esql );
  check_error( esql, res );
  PQclear(res);
}

int delete_test(int id){
  PGresult   *res;
    // update list ( key, value ) values( %s, %s )
  const char * sql = del2; //[] = "update list SET value ='%s' where id=%d";
  char esql[strlen(sql) + 32 ];
  sprintf( esql, sql, id );
  res = PQexec( conn, esql );
  check_error( esql, res ) ;
  PQclear(res);
}


int comp( const int * l, const int * r ){
  //   return ( *l > *r); //これだとなぜかソートされない
  return //(*r-*l);
    (*l - *r);
}


//#endif
/**

int main(int args, char ** argv){
  int N = 536;
  int M = 0;
  int i;
  int n = 0;
  int id;
  my::rand r;

  int ids[N];
  struct timeval start, end;
  gettimeofday(&start, NULL);
  srand(start.tv_usec * start.tv_sec);
  n = rand() % 345;
    //    update_test(s);
  char key[] = "123456789abcdef";
  char value[] = "123456789abcdef"
    "123456789abcdef";

  if(  begin_transaction(NULL) < 0 ) exit(1);

  PGresult * res = PQexec(conn, "SELECT MAX(id) FROM documents");
  M = atoi( PQgetvalue(res, 0, 0) );
  printf("%d\n", M );
  if( M > 0 ){
    for( i = 0 ; i < N ; ++i ){
      ids[i] = rand() % M;
    }
    qsort( ids , N, sizeof(int) , (int (*)(const void*, const void*))comp);
    printf("0 - %d, %d - %d\n",  ids[0], N/2, ids[N/2] );
  }
  gettimeofday(&start, NULL);

  //select_test("");
  for( i=0; i<N; ++i ){ //
    //insert_test( r.str(64), r.str(128) );
    //update_test(i, r.str(128) );
    //id = rand() % M;
    //    id = i;
    //printf("%d/%d \t...%d\n", i, N, id );
    //update_test( ids[i], r.str(128) );
    r.c();
  //  for( i=0; i<N; ++i )  update_test(i, "hoge");
  }
  commit_transaction();
  gettimeofday(&end, NULL);
  double t = (end.tv_sec - start.tv_sec)+ ((double)(end.tv_usec - start.tv_usec))/1000000.0;
  // t/N tps achieved.
  printf( "%f tps for %d datas in %f secs; %f ms for each data.\n", N/t, N, t, 1000*t/N );
  
  return 0;
}

*/
