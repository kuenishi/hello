import boto, sys
from boto.s3.connection import S3Connection

conn = S3Connection(sys.argv[2], sys.argv[3])
bucket = conn.get_bucket(sys.argv[1])
mpu = bucket.list_multipart_uploads()
for mp in mpu:
    mp.cancel_upload()
    print(unicode(mp))
