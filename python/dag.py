

'''for direct acyclic graph analysis
  1. dict pattern - analyses when printing it <- ima koko
  2. cluster pattern - analyses when modding(creating) it
   2-1. connection oriented cluster pattern - we have connection object
   2-2. node oriented cluster pattern - we have node object
   2-3. very rich pattern - both connection and node(maybe too much)
'''

class DAG:
    def __init__(self):
        self.g = {}
        self.last_add = None

    def add(self, f, to):
        self.g.update( [(f,to)] )
        return self

    def _add(self, dict):
        for (k, v) in dict.iteritems(): add(k,v)

    def _add(self, key, value):pass

    def p_all(self):
        '''resolve and prints DAG'''
        print '----graph:'
        for (k, v) in self.g.iteritems(): print k,'->', v
        print '----roots:'
        for root in self.roots():  print root
        print '----leaves:'
        for leaf in self.leaves(): print leaf

    def leaves(self):
        return filter( lambda x: (x not in self.g.keys()), self.g.values() )

    def roots(self):
        return filter( lambda x: (x not in self.g.values()), self.g.keys() )
        
    def p_g(self):   self.p_all()

if __name__=='__main__':
    d = DAG()
    d.add('hello', 'hge')
    d.add('world', 'hello')
    d.add('hoge', 'hage')
    d.p_g()
