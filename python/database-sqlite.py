#!/usr/bin/env python


#from pysqlite2 import dbapi2 as sqlite
#from sqlite3 import dbapi2 as sqlite

#you'd install python-sqlite
import sqlite

class Database:
    def __init__(self, db = 'db/mails.db'):
        self.connection = sqlite.connect( db )
        self.cursor = self.connection.cursor()
        
    def select_all_mail(self):
        self.cursor.execute('SELECT * FROM mails;')
        result = self.cursor.fetchall()
        mails = []
        for r in result:
            mails.append( Mail(r) )
        return mails
    

def test():
    connection = sqlite.connect('test.db')

    #It's also possible to create a temporary database that exists in memory:
    #memoryConnection = sqlite.connect(':memory:')

    cursor = connection.cursor()
#cursor.execute('CREATE TABLE names (id INTEGER PRIMARY KEY, name VARCHAR(50), email VARCHAR(50))')
#     cursor.execute('INSERT INTO names VALUES (null, "John Doe", "jdoe@jdoe.zz")')
#cursor.execute('INSERT INTO names VALUES (null, "Mary Sue", "msue@msue.yy")')


    cursor.execute('SELECT * FROM SQLITE_MASTER')
    result = cursor.fetchall()


    print cursor.arraysize, " rows selected:", 
    print result
#    for r in result:
#        m = Mail(r)
#        m.p()
 ###       last = m.id
        
   # newm = Mail((last+1, "hoge: hage: content\n hoge"))

   # cursor.execute( newm.insert() )
   # connection.commit()

if __name__ == '__main__':
    test()



