
def somesomedecorator(argv):
    print "somesomedecorator: ", argv
        
def somedecorator(fun):
    print "decorator: \n",
    def fun2(argv):
        print "huga",
        fun(argv)
        print "hogehoge"
    return fun2

@somedecorator
def decoratee(argv):
    print "decoratee: ", argv

if __name__ == "__main__":
    decoratee("suge")
