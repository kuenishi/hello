import string, sys, re
import os, mapreduce

regexp = "" #this will be difficult in distributed environment

def dgrep_map( pair ):
    '''true user map function that converts key/value into key'/value' '''
    filename = pair[0]
    if( os.stat(filename)[3]==1 ):
        try:
            f = open(filename, 'r')
            try:
                tmp_lines = f.readlines()
                lines = []
                for l in range(1, len(tmp_lines)+1):
                    lines.append( (filename+':%d'%l, tmp_lines[l-1].strip() ) )
                return filter( lambda x: regexp.search( x[1]), lines)
                #the point is to use search, not match
            finally:    f.close()
        except IOError: return False

def dgrep_reduce( x, y ): pass 

def dgrep( files ):
    '''input file list and returns a word count list.'''
    inputs = zip( files, [None]*len(files))  #input key/value pairs
    mapreduce.R = 10
    count_lists = mapreduce.do_maps(dgrep_map, inputs)
    return  mapreduce.do_reduces(dgrep_reduce, count_lists )

if __name__=='__main__':
    regexp = re.compile(sys.argv[-1])

    l = dgrep( sys.argv[:-1] )
    for r in xrange(0, mapreduce.R ):
        print 'r=', r, ':'
        for x in l[r]:
            print x


