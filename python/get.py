#!/usr/bin/env python
# -*- coding: utf-8 -*-

#http://d.hatena.ne.jp/snuffkin/20081215/1229296863
import time
import memcache

if __name__ == '__main__':
    # initialize
    mc = memcache.Client(['127.0.0.1:11213'])
    message_num  = 10000

    # get
    start_time = time.time()
    for index in range(message_num):
        message = mc.get(str(index))
    end_time = time.time()

    # print progress time
    progress_time = end_time - start_time
    print 'messages: %s' % message_num
    print 'get time: %s' % progress_time
