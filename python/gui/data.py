# $Id: data.py 73 2007-04-27 16:27:59Z kuenishi $
# module data
#
import unittest
import pickle

class Data:
    def __init__(self):
        self.array={}

    def append(self, key, value):
        self.array.update( {key : value} )
        #self.array[key] = value
        

    def empty(self):
        if len(self.array) > 0:
            return False
        else: return True

    def append_dict(self, tailer):
        if len(tailer) > 0:
            self.array.update( tailer )

    def p(self):
        print self.array

    def save(self, file):
        f = open(file, 'w+')
        pickle.dump(self, f)
        f.close()

    def len(self):
        return len(self.array)

def load_data(file):
    f = open( file, 'r' )
    tmp = pickle.load(f)
    f.close()
    return tmp

#me = Data()
#me.append("k", "v")
#me.append_dict({"ke": "va", "日本語":"どう？"} )

#me = load_data("test.dat")
#print '\nlist:'
#me.p()


class TestSequenceFunctions(unittest.TestCase):
    def setUp(self):
        self.data = load_data( "test.dat" )

    def testPickle(self):
        self.assertEqual( self.data.len(), 3)

    def testAppend(self):
        tmp = self.data.len() + 1
        self.data.append( "for", "test" )
        self.assertEqual( self.data.len(), tmp )

    def testAppendDict(self):
        appendee = {"hoge":"huge", "man":"koo"}
        tmp = self.data.len() + len(appendee)
        self.data.append_dict(appendee)
        self.assertEqual( self.data.len(), tmp)

    def testSave(self):
        filename = "asdfghjk.dat"
        self.data.save(filename)
        tmpdata = load_data(filename)
        self.assertEqual( tmpdata.len(), self.data.len() )
        


print "starting test..."
# simple output
#if __name__ == '__main__':
#    unittest.main()

#detailed output
suite = unittest.makeSuite(TestSequenceFunctions)
unittest.TextTestRunner(verbosity=2).run(suite)
