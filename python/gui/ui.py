#import sys, Tkinter
from Tkinter import *

class MainWindow(Frame):

    def __init__(self, master=None):
        Frame.__init__(self, master)
  
        button = Button(master, text="close", command=sys.exit)
        self.dataf  = DataFrame(self)
        self.dataf.pack()

        button.pack(side= "bottom")
        self.master.title('hropy prototype')
        self.master.geometry('200x200')
        
        self.searchframe = SearchFrame(self)
        self.searchframe.pack(side=TOP)
        
        self.pack()

    def run(self):
        mainloop()

class DataFrame(Frame):
    def __init__(self, master=None):
        Frame.__init__(self, master)
        self.pack()
        
    def set_data(self, data):
        if data.empty: return
        
        for k, v in data.array:
            print k, v
            

class SearchFrame(Frame):
    def __init__(self, master=None):

        Frame.__init__(self, master)
                
        self.input = Entry()
        self.input.pack(side = TOP)
        self.contents = StringVar()
        self.input["textvariable"] = self.contents
        self.input.bind('<Key-Return>',  self.search)

        self.button = Button(master, text="search", command=self.search)
#        self.button.config(bg="#FFF")     
        self.button.pack( side = BOTTOM)
        self.master.config(bd=2,relief="groove")
        
    def search(self, event = None):
        q = self.contents.get()
        
        if len(q) < 1:
            ans = "empty query."
        else:
            ans = "you're searching "+ q
        print ans
