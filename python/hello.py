# $Id: hello.py 69 2007-04-23 16:15:26Z kuenishi $

def hello():
    print "Hello, python!!"

def number():
    a = 1.0j + 3245
    b = 34j - 23
    print (a * b).real
    print abs( a/b )
    a, b = 1, 1
    while b < 10:
        a, b = b, a+b
        print b

def etc():  #引数がなければ、dir() は現在定義している名前を列挙します。
    print dir()

hello();

