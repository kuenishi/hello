#!/usr/bin/env python


from pysqlite2 import dbapi2 as sqlite
connection = sqlite.connect('test.db')

#It's also possible to create a temporary database that exists in memory:
#memoryConnection = sqlite.connect(':memory:')

cursor = connection.cursor()
cursor.execute('CREATE TABLE names (id INTEGER PRIMARY KEY, name VARCHAR(50), email VARCHAR(50))')
cursor.execute('INSERT INTO names VALUES (null, "John Doe", "jdoe@jdoe.zz")')
cursor.execute('INSERT INTO names VALUES (null, "Mary Sue", "msue@msue.yy")')
connection.commit()

cursor.execute('SELECT * FROM names')
print cursor.fetchall()





