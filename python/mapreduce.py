'''two way to do:
 1.make it multithreaded before making it parallel distrebuted
 2.separate its user application code from map/reduce kernel code
 3.use some other tools such as pylinda or parallel python

 if you'd really like to send program, FoA send hoge.py.
 second, in server code;
   file_prefix = "hoge"  # reads from hoge.py
   new_module = __import__(file_prefix)
   new_module.call_some_func()
'''

import string, sys, re
import md5 as hash
import os
import threading

#global variables to be thread_safe or not?
R = 1

intermids = []

def unhex( h ): #http://www.koders.com/python/fidFBDA7DA7132826EC4CD807F0348F14D15523BEE3.aspx?s=smtp+server^5
    depth = 0
    output = 0
    for i in range(0, len(h), 2):
        s = h[i:i+2]
        value = int(s, 16)
        output += output * 16 + value 
    return output

class Map(threading.Thread):
    def __init__( self, map_func, pair ):
        if( len(pair) != 2 ): return False
        threading.Thread.__init__(self)
        self.map_func = map_func
        self.pair = pair
        
    def run(self): self.do_map()

    def do_map(self):
        results = self.map_func(self.pair)
    #combine here
    #results2 = multi_reduce( lambda x, y: x+y, results )
        results2 = results
    #hash_mod_R here
        intermid = {}
        for (k,v) in results2:
            key = unhex( hash.new(k).hexdigest() ) % R
            if( key not in intermid):
                intermid[key] = []
            intermid[key].append( (k,v) )
        for r in intermid:
            intermid[r].sort( lambda x,y: cmp( x[0], y[0]) )
        self.result = intermid

def do_maps( map_func, pairs):
    '''parallel invoke of maps!: currently with threading'''
    tasks = [ Map(map_func, pair) for pair in pairs]
    map( lambda m: m.start() , tasks )
    map( lambda m: m.join() , tasks )
    return [ m.result for m in tasks ]

def do_maps_deprecated( map_func, pairs):
    return [ do_map( map_func, pair ) for pair in pairs] # list as each file's count

def multi_reduce(reduce_func, mid_data):
    mid = {}
    for t in mid_data: 
        mid[t[0]] = reduce_func( mid[t[0]],  t[1] ) if ( t[0] in mid )  else  t[1]
    return mid.items()

def do_reduces(reduce_func, lists):
    result = {}
    for r in xrange(0, R ):
        list = reduce( lambda s, t: s+t, 
                       [(l[r] if r in l else []) for l in lists ] )
        result[r] = multi_reduce( reduce_func, list )
        result[r].sort( lambda x,y: cmp( x[0], y[0]) )
    return result


if __name__=='__main__':
    pass
    #adict = {}
    #adict.update( filter( lambda x: (x[1]>1), l) )

    #http://blog.modp.com/2007/11/sorting-python-dict-by-value.html
    #alist = sorted(adict.items(), key=lambda (k,v): (v,k))
    #n = 10
    #print 'top %s: %s' % (n, alist[-n:]) # top 5
