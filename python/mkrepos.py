from mercurial import hg, ui, commands
import urllib, os, re, Queue, time, tarfile
#
# strongly discouraged to use this code, since this program can be 
# sth like download attack to erlang.org.
#

#import multiprocessing as threading
import threading
import subprocess
from mercurial import commands, hg, ui, error

DEBUG=False
base_url = "http://www.erlang.org/download/"
download_dir = "www.erlang.org/download/"

download_list = (
    "otp_src_R6B-0", #.tar.gz    24-Nov-1999 14:02   6.7M  
    "otp_src_R7B-0", #.tar.gz    28-Sep-2000 16:05   7.3M  
    "otp_src_R7B-1", #.tar.gz    03-Dec-2000 02:04   7.4M  
    "otp_src_R7B-2", #.tar.gz    05-Mar-2001 13:44   8.4M  
    "otp_src_R7B-3", #.tar.gz    01-Jun-2001 16:40  11.7M  
    "otp_src_R7B-4", #.tar.gz    02-Oct-2001 18:03  11.7M  
    "otp_src_R8B-0", #.tar.gz    18-Oct-2001 15:34  10.4M  
    "otp_src_R8B-1", #.tar.gz    12-Apr-2002 17:43  10.4M  
    "otp_src_R8B-2", #.tar.gz    08-Aug-2002 16:15  10.4M  
    "otp_src_R9B-0", #.tar.gz    16-Oct-2002 17:49   7.9M  
    "otp_src_R9B-1", #.tar.gz    05-Mar-2003 15:48   8.1M  
    "otp_src_R9C-0", #.tar.gz    06-Aug-2003 17:32   7.8M  
    "otp_src_R9C-1", #.tar.gz    26-May-2004 13:49   8.3M  
    "otp_src_R9C-2", #.tar.gz    23-Jun-2004 16:00   8.3M  
    "otp_src_R10B-0", #.tar.gz  06-Oct-2004 10:41   4.3M  
    "otp_src_R10B-1a", #.tar.gz  12-Nov-2004 07:27   8.9M  
    "otp_src_R10B-2", #.tar.gz   15-Dec-2004 15:09   9.0M  
    "otp_src_R10B-3", #.tar.gz   10-Feb-2005 10:21   9.5M  
    "otp_src_R10B-4", #.tar.gz   23-Mar-2005 09:00   9.5M  
    "otp_src_R10B-5", #.tar.gz   11-May-2005 10:05   9.5M  
    "otp_src_R10B-6", #.tar.gz   22-Jun-2005 10:15   9.6M  
    "otp_src_R10B-7", #.tar.gz   31-Aug-2005 10:34   9.6M  
    "otp_src_R10B-8", #.tar.gz   26-Oct-2005 09:45   9.5M  
    "otp_src_R10B-9", #.tar.gz   14-Dec-2005 12:07   9.5M  
    "otp_src_R10B-10", #.tar.gz  08-Mar-2006 12:48   9.0M  
    "otp_src_R11B-0", #.tar.gz   17-May-2006 11:47  10.3M  
    "otp_src_R11B-1", #.tar.gz   30-Aug-2006 15:31  10.6M  
    "otp_src_R11B-2", #.tar.gz   08-Nov-2006 15:06  10.7M  
    "otp_src_R11B-3", #.tar.gz   31-Jan-2007 14:36  10.9M  
    "otp_src_R11B-4", #.tar.gz   28-Mar-2007 14:21  11.2M  
    "otp_src_R11B-5", #.tar.gz   13-Jun-2007 15:38  36.6M  
    "otp_src_R12B-0", #.tar.gz   05-Dec-2007 08:53  39.2M  
    "otp_src_R12B-1", #.tar.gz   06-Feb-2008 09:50  39.4M  
    "otp_src_R12B-2", #.tar.gz   09-Apr-2008 10:37  39.3M  
    "otp_src_R12B-3", #.tar.gz   11-Jun-2008 14:40  40.2M  
    "otp_src_R12B-4", #.tar.gz   03-Sep-2008 13:38  42.3M  
    "otp_src_R12B-5", #.tar.gz   06-Nov-2008 11:17  45.3M  
    "otp_src_R13A", #.tar.gz     17-Mar-2009 12:17  51.7M  
    "otp_src_R13B", #.tar.gz     21-Apr-2009 11:13  51.9M  
    "otp_src_R13B01", #.tar.gz   10-Jun-2009 14:03  52.1M  
    "otp_src_R13B02", #.tar.gz   22-Sep-2009 15:30  52.5M  
    )



print_lock = threading.Lock()
def locked_print(str):
    print_lock.acquire()
    print str
    print_lock.release()

class Pipeliner(threading.Thread):
    def __init__(self, src, dest=None):
        threading.Thread.__init__(self)
        self.src = src
        self.dest = dest

    def run(self):
        while(True):# not self.src.empty():
            try:
#                source = self.src.get()
                source = self.src.get_nowait()
            except Queue.Empty:
                time.sleep(0.1)
#                locked_print( "empty... orz" + self.src.qsize().__str__() )
                continue
#            locked_print( "popped: " + source + self.src.qsize().__str__() )
            try:
                source.index("<<EOF>>")
                if self.dest:
                    self.dest.put_nowait("<<EOF>>")
#                    locked_print( "EOF!! " + self.dest.qsize().__str__() )
                return
            except ValueError:
                product = self.filt( source )
                if self.dest:
                    self.dest.put_nowait(product) 
#                    locked_print( "pushed: "+ product  + self.dest.qsize().__str__() )
            self.src.task_done()
#            self.dest.task_done()

        self.finish()

class Downloader(Pipeliner):
    def __init__(self, src, dest):
        Pipeliner.__init__(self,src,dest)

    def filt(self,src):
#        print "downloader:", src
        try:
            n = download_dir+src+".tar.gz"
            os.stat(n)
            locked_print( "we already have "+ n )
        except OSError:
            locked_print( "don't have "+ n+": now downloading")
            urllib.urlretrieve(base_url + src + ".tar.gz", n)
        return src
 #       self.dest.put(download_dir+n+".tar.gz")
            
    def finish(self):
        locked_print( "got all archives." )

class Extracter(Pipeliner):
    def __init__(self, src, dest):
        Pipeliner.__init__(self, src, dest)
        self.basedir = "/tmp"

    def filt(self, src):
        file = download_dir+src+".tar.gz"
        try:
            os.stat(file)
#            print "Extractor: ", file
            tar = tarfile.open(file, "r:gz")
            tar.extractall(self.basedir)
            new_src = tar.getnames()[0].split('/')[0]
            tar.close()
        except OSError:
            locked_print( "error extracting " + file )
        return new_src

    def finish(self):
        locked_print( "all archives extracted." )

def rm_rf(fullpath):
#            print "removing", fullpath
    if os.path.isfile(fullpath): 
        os.remove(fullpath)
    elif os.path.isdir(fullpath):
        for p in os.listdir(fullpath):
            rm_rf(os.path.join(fullpath, p))
        os.rmdir(fullpath)
    else: pass

class Committer(Pipeliner):
    def __init__(self, src, repos_path):
        Pipeliner.__init__(self,src)
#        print "init___", repos_path
        try: os.mkdir(repos_path)
        except OSError: pass
        try:  commands.init(ui.ui(), repos_path)
        except error.RepoError: pass
        self.repos = hg.repository(ui.ui(), repos_path)
        self.repos_path = repos_path
        commands.tip(ui.ui(), self.repos )
        commands.branch(ui.ui(), self.repos, "original-line")
        self.basedir = "/tmp/"

    def filt(self,src):
        locked_print( "Committer: " + src )

        for p in os.listdir(self.repos_path):
            if p[0] is '.': continue #hidden file
            fullpath = os.path.join(self.repos_path+"/"+p)
            rm_rf(fullpath)
        for p in os.listdir( os.path.join(self.basedir, src)):
            path = os.path.join(self.basedir, src)
            os.renames( os.path.join(path, p ), os.path.join(self.repos_path, p) )
        commands.addremove(ui.ui(), self.repos )
        commands.commit(ui.ui(), self.repos , message=src )
        commands.tag(ui.ui(), self.repos, src )
        return src

    def finish(self):
        locked_print( "commiter finished" )

if __name__ == '__main__':
    repos_path = "erlang-otp.hg"
    try:
        os.stat(download_dir) # assume "www.erlang.org" exists
    except OSError:
        os.mkdir(download_dir)
        locked_print( download_dir+ " doesn't exist, created.")
    targetq = Queue.Queue()
    tarfileq = Queue.Queue()
    srcdirq = Queue.Queue()
    if DEBUG:
        rm_rf(repos_path)
        for n in download_list[14:16] :
#            os.remove( os.path.join(download_dir, n) )
            targetq.put(n)
    else:
        for n in download_list:
            targetq.put(n)
    targetq.put("<<EOF>>")
    downloader = Downloader(targetq, tarfileq)
    extracter = Extracter(tarfileq, srcdirq)
    committer = Committer(srcdirq, repos_path)

    downloader.start()
    extracter.start()
    committer.start()
    
    downloader.join()
    extracter.join()
    committer.join()



