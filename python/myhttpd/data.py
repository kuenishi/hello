
import pickle
import unittest

class Settings:
	def __init__(self, filename = None):
		self.array = {}
		if not filename:
			self.makedefault()
		else:
			self.filename_ = filename
			fh = open(filename, 'r')
			self.array = pickle.load(fh)
			fh.close()
		
	def save(self, filename = None):
		if not filename:
			filename = self.filename_
			
		fh = open(filename, 'w+')
		pickle.dump(self.array, fh)
		fh.close()

	def set(self, key, value):
#		self.array.update({key, value})
		self.array[key] = value
		
	def p(self):
		print self.array

	def makedefault(self):
		self.array['port']=8000
		self.array['ip']="127.0.0.1"
		#self.array

	def addr(self):
		return (self.array['ip'], self.array['port'])
		
class SettingsTests(unittest.TestCase):
	def testPicle(self):
		setting = Settings('lmail.conf')
		#setting = Settings()
		setting.p()
		setting.save('tmp.conf')
		tmp = Settings('tmp.conf')
		self.assertEqual(len(setting.array), len(tmp.array))

def test():
	suite = unittest.makeSuite(SettingsTests)
	unittest.TextTestRunner(verbosity=2).run(suite)