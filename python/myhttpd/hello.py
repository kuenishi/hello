# thx to http://omake.accense.com/wiki/Python/Snippet/

import CGIHTTPServer
import BaseHTTPServer
import SimpleHTTPServer

def serveForever(addr, HandlerClass = CGIHTTPServer.CGIHTTPRequestHandler,
        ServerClass = BaseHTTPServer.HTTPServer):
  HandlerClass.protocol_version = 'HTTP/1.0'
  httpd = ServerClass(addr, HandlerClass)
  sa = httpd.socket.getsockname()
  print "Serving HTTP on %s:%d" % (sa[0], sa[1])
  httpd.serve_forever()

if __name__ == '__main__':
  serveForever(('127.0.0.1', 8000))

