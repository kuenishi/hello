import BaseHTTPServer
import SimpleHTTPServer

addr = ('127.0.0.1', 8000)
Server = BaseHTTPServer.HTTPServer
handler = SimpleHTTPServer.SimpleHTTPRequestHandler

httpd = Server(addr, handler)
httpd.serve_forever()

