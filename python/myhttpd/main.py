##
# $Id: main.py 100 2007-05-25 12:16:56Z kuenishi $
##

from servlet import MyHTTPRequestHandler, MyHTTPServlet
from data import Settings

def serveForever(addr, Handler = MyHTTPRequestHandler, Server = MyHTTPServlet):
  Handler.protocol_version = 'HTTP/1.1'
  httpd = Server(addr, Handler)
#  sa = httpd.socket.getsockname()
#  print "Serving HTTP on %s:%d" % (sa[0], sa[1])
  httpd.serve_forever()

if __name__ == '__main__':
	conf = Settings('lmail.conf')
	serveForever( conf.addr() )

