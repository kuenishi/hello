import sys

def is_prime(num, prime_list):
    for p in prime_list:
        if num % p == 0:
            return False
    return True

def gen_primes(end):
    prime_list = [2]
    for i in range(2, end):
        if is_prime(i, prime_list):
            prime_list.append(i)
    return prime_list

if __name__ == '__main__':
    print gen_primes( int(sys.argv[1]) or 100 )
