
# * $Id: loop.py 125 2007-06-08 09:11:01Z kuenishi $
# * 
# * purpose: investigate linux process scheduling

import sys, string

def loop(n):
	cnt = 1 #.0001
	for i in range(0, n):
		cnt += 1
#		cnt *= 1.0000023
	return cnt

if __name__ == "__main__":
	if len(sys.argv) > 1:
		r = string.atoi(sys.argv[1])
		if r > 7:
			print "radius larger than 8 is not recommended."
			print "your input is:", r
#			sys.exit()
		else: pass
		n = 10 ** r
	else:
		n = 10 ** 7

	print "n=", n, "result=", loop(n)
