//#include "Python.h"

#include <sys/time.h>
#include <sys/resource.h>
#include <sys/types.h>
#include <unistd.h>

int chgpriority(pid_t pid, int priority){
  //  printf("pid:%d\n", pid);
  if ( -20 < priority && priority < 20 )
    setpriority(PRIO_PROCESS, pid, priority);
  return getpriority(PRIO_PROCESS, pid);
}

int add(int x, int y){
  return x+y;
}


