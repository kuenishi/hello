//need python-dev
#include <Python.h>

extern int chgpriority(pid_t, int);
extern int add(int, int);

PyObject* linuxctl_chgpriority(PyObject* self, PyObject* args){
  int p, newp, g;
  if( !PyArg_ParseTuple(args, "ii", &p, &newp))
    return NULL;

  g = chgpriority(p, newp);
  return Py_BuildValue("i", g);
}

PyObject* linuxctl_add(PyObject* self, PyObject* args){
  int x, y, g;
  if( !PyArg_ParseTuple(args, "ii", &x, &y))
    return NULL;

  g = add(x , y);
  return Py_BuildValue("i", g);
}

static PyMethodDef linuxctlmethods[] = {
  { "chgpriority", linuxctl_chgpriority, METH_VARARGS},
  { "add", linuxctl_add, METH_VARARGS},
  {NULL},
};

void initlinuxctl(){
  Py_InitModule( "linuxctl", linuxctlmethods);
}

