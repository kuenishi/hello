
# * $Id: process.py 126 2007-06-08 09:50:06Z kuenishi $
# * 
# * purpose: investigate linux process scheduling


import os
import threading
import subprocess, time, sys
import logging
import pf.linuxctl

class Controller:
	def __init__(self, thread_num = 5):
		self.tasks = []
		self.pids  = []
		for i in range(0, thread_num):
			self.tasks.append( Controlled() )

	def start(self):
		for t in self.tasks:
			t.start()
			
		print len(self.tasks), "processes has started.",
		
	def join(self):
		for t in self.tasks:
			t.join()

	def getpids(self):
	
		flag = 0
		while not flag:
			flag = 1
			for t in self.tasks:
				flag = flag and t.mypid
			
		retpids = []
		for t in self.tasks:
			retpids.append( t.mypid )
				
		print retpids

	def setpriority(self, pr):
		for t in self.tasks:
			pf.linuxctl.chgpriority( t.mypid, pr )

	def do(self, func):
		try:
			for t in self.tasks:
				t.func()
		except:
			print "can't do", func

	def monitor(self):
		print os.getloadavg()
		for t in self.tasks:
			print t.runtime(),

	

class Controlled(threading.Thread):
	def __init__(self):
		threading.Thread.__init__(self)
		self.mypid = 0
		self.starttime = 0 #os.times()[4]
		self.endtime   = 0
		
	def run(self):
		#start = os.times()[0]
		self.run_this( ['python', 'loop.py', '7'] )
		self.time = os.times() #[0] - start

	def run_this(self, cmd):
		if len(cmd) < 1: return

		print "starting.",
		sys.stdout.flush()
		p = subprocess.Popen(cmd, stdout=subprocess.PIPE)
		self.mypid = p.pid

		time.sleep(1)
		self.starttime = os.times()[4]
		while( p.poll() == None ):
			sys.stdout.write('.')
			sys.stdout.flush()
			time.sleep(1)
		self.endtime = os.times()[4]
		print "done. result follows:"
		print p.stdout.read()

	def count(self):
		c = 0
		print "start ...",
		for i in range(0,1000000):
			c += i*0.032
		print c, " finished."

	def runtime(self):
		return self.endtime - self.starttime

def run(cmd):
	print cmd


if __name__ == "__main__":
	c = Controller()
	c.start()
	print c.getpids()
	c.setpriority()
	time.sleep(3)
	c.join()
	print c.getpids()
	c.monitor()
