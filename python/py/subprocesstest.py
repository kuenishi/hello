
# * $Id: main.py 101 2007-06-05 02:38:16Z kuenishi $
# * 
# * purpose: investigate linux process scheduling


import os
import subprocess, time, sys


def run(cmd):
	print cmd
	if len(cmd) < 1: sys.exit()

	print "starting.",
	sys.stdout.flush()
	p = subprocess.Popen(cmd, stdout=subprocess.PIPE)
	while( p.poll() == None ):
		sys.stdout.write('.')
		sys.stdout.flush()
		time.sleep(1)
	print "done. result follows:"
	print p.stdout.read()

if __name__ == "__main__":
	
	run( sys.argv[1:])
