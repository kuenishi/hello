#!/usr/bin/env python
# -*- coding: utf-8 -*-

#http://d.hatena.ne.jp/snuffkin/20081215/1229296863
import time
import memcache

def get_message(kbytes):
    return "1234567890" * 100 * kbytes

if __name__ == '__main__':
    # initialize
    mc = memcache.Client(['127.0.0.1:11211'])
    mc.flush_all()

    message_num  = 10000
    message_size = 1
    message      = get_message(message_size)

    # set
    count = 0
    start_time = time.time()
    for index in range(message_num):
        result = mc.set(str(index), message)
        if( result ): count += 1
    end_time = time.time()

    # print progress time
    progress_time = end_time - start_time
    print 'messages: %s / %s' % (count , message_num)
    print 'set time: %s' % progress_time
