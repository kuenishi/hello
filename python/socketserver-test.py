# vim: fileencoding=utf8 sts=4 sw=4
# via: http://d.hatena.ne.jp/Voluntas/20071231/1199109958
# $ echo "hoge" | nc localhost 12345 

import SocketServer

class ExecHandler(SocketServer.StreamRequestHandler):
    def handle(self):
        print "connect from:", self.client_address
        

class EchoHandler(SocketServer.StreamRequestHandler):
    def handle(self):
        print "connect from:", self.client_address
        while True:
            data = self.request.recv(8192)
            if len(data) == 0:
                break
            self.request.send(data)
        self.request.close()

if __name__ == '__main__':
    #server = SocketServer.TCPServer(('', 12345), EchoHandler)
    server = SocketServer.ThreadingTCPServer(('', 12345), EchoHandler)
    #server = SocketServer.ForkingTCPServer(('', 12345), EchoHandler)
    print 'listening:', server.socket.getsockname()
    server.serve_forever()
