#!/usr/bin/env python
# encoding: utf-8

"""Hello Mercurial

A Mercurial Hello World extension. 
"""
# Import basic Mercurial stuff
from mercurial import hg
# and the localizing function
from mercurial.i18n import _

__copyright__ = """ 
  Hello Mercurial - A Mercurial Hello World extension
----------------------------------------------------------------- 
© 2009 - 2009 Copyright by Arne Babenhauserheide

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston,
  MA 02110-1301 USA

""" 

 # Every command function must take a ui and and repo as arguments.
 # 
 # opts is a dict with the command line options.
 # 
 # args contains all simple command line arguments. 
 # They are taken in order from items on the command line 
 # that don't start with a dash.  
 # If no default value is given in the parameter list, they are required.
 # 
 # For experimenting with Mercurial in the python interpreter: 
 # Getting the repo object for the current dir: 
 #    >>> from mercurial import hg, ui
 #    >>> repo = hg.repository(ui.ui(), path = ".")

def hello(ui, repo, *args, **opts): 
	# The docstring below will show in hg help
	"""Say 'Hello Mercurial!'"""
	name = opts["name"]
	if name is None: 
		name = "Mercurial"
	ui.write("Hello " + name + "!\n")
	ui.write( args )
	ui.write( "\n" )
	
		
# Add the hello command to the command line interface
cmdtable = {
	# cmd name	(function call, [options], "usage instructions")
	"hello": 	(hello, 
			[	# An option
				('n', # short form
				'name', # long form
				"Mercurial", # default value. if None, True or False the option is a bool
			        _('the name to use for greeting')) # help text
			], 
			_("hg hello [-n <name>]"))
}
