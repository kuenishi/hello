import string, sys, re
import os, mapreduce

def wordcount_map( pair ): # filename, file itself?
    '''true user map function that converts key/value into key'/value' '''
    filename = pair[0]
    if( os.stat(filename)[3]==1 ):
        try:
            f = open(filename, 'r')
            try:
                list = []
                for line in f.readlines():
                    words = filter( None, #same as lambda x: x,
                                    re.compile(r'\W+').split(line) )#line.split()) # ()[]{}<>=,.
                    list += map(None, words, [1]*len(words))
                return list #filter( lambda x: x, list)
            finally:
                f.close()
        except IOError:
            return False

def wordcount_reduce( v1, v2 ):
    return v1+ v2

def wordcount( files ):
    '''input file list and returns a word count list.'''
    inputs = zip( files, [None]*len(files))  #input key/value pairs

    mapreduce.R = 23

    # do map for each key/value pair
    count_lists = mapreduce.do_maps(wordcount_map, inputs)
    return  mapreduce.do_reduces(wordcount_reduce, count_lists )

if __name__=='__main__':
    l =wordcount( sys.argv )

    for r in xrange(0, mapreduce.R ):
        for (k,v) in l[r]:
            print k,v
#        print 'r=', r, ':', l[r]

