#!/usr/bin/env python
# -*- coding: utf-8 -*-
#目的：ジョブの長さの分布を計る！！→同じジョブでどうなるかやってみるみそ
#TODO: timeitモジュールを使ってプロセスの正確な実行時間を...->計れませんかそうですか
#      それぞれのジョブの実行結果をグローバル変数に保存するか、->singletonで済ませた
#      もしくはsqliteにするか、それともsingletonで賄うか、...
#      sqliteでDBアクセスを模したジョブをつくるべし。
#

import subprocess
import timeit
import time
import threading

class JobResult:
	def __init__(self, name, start, end):
		self.name = name
		self.start = start
		self.end = end
		self.time = end - start

class Stats(object): 
	'''singleton pattern implementation
	reffered from thread  
	http://www.python.jp/pipermail/python-ml-jp/2002-November/002075.html
	, thanks!  usage: inherit Singleton and call Inheretee.instance()'''
	instance_ = None
	
	def __new__(cls):
		if cls.instance_ is None:
			cls.instance_ = object.__new__(cls)
		return cls.instance_
	def instance(cls):
		if not cls.instance_:
			cls.instance_ = Stats()
		return cls.instance_
	instance = classmethod(instance)

	stat = []
	def append(self, result):
		self.stat.append( result )
	def p(self):
		#print self.stat
		for r in self.stat:
			print r.name, '\t', r.start, "-", r.end, r.time
	def output(self):
		if len(self.stat) < 1: return
		sum = 0
		for r in self.stat:
			sum += r.time
		mean = sum/len(self.stat)
		sum = 0
		for r in self.stat:
			sum += (r.time - mean )*(r.time - mean )
		var  = sum/len(self.stat)
		print "meantime:", mean
		import math
		print "stddev:  ",   math.sqrt(var)
#end class

class Job(threading.Thread):
	'''Job: ジョブを実行して時間を計るためのクラス...と思っていたが
	どうなるかはわからない'''
	def __init__(self, name='true', bin='false', *argv):
		threading.Thread.__init__(self)
		self.name = name
		self.bin = bin
		self.argv = argv
		self.finished = threading.Event()
		#print argv

	def run(self):
	        array = [self.bin]
		array.extend(self.argv)
		self.start = time.time()

                #OSXだとなぜかここで落ちる
		self.pobj = subprocess.Popen(array, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
		self.result = self.pobj.stdout.read()
		self.pid = self.pobj.pid
		if self.pobj.poll() is None:
			try: self.pobj.wait()
			except: pass
		self.end = time.time()
		self.finished.set()
		Stats.instance().append(JobResult(self.name, self.start, self.end) )
		self.returncode = self.pobj.returncode
		if self.returncode < 0:
			print self.name, 'has faled with status', self.returncode, "."
                 #print self.name, "has finished:", self.returncode

	def clone(self):
		if not self.finished.isSet():
			return Job(self.name, self.bin, *self.argv)

def inBurst(n, j = None):
	"""starts all jobs in the list at once. all jobs' starting times  are
	thrown to threading library."""
	if j is None:
		j = Job('CPU', 'python', '-c', "import workload; workload.factor(2345)")
	jobs = []
	if n > 1000 or n < 1: n = 1000
	for n in range(1,n):
		jobs.append( j.clone() )

	map( lambda job:(job.start()), jobs)
	map( lambda job:(job.join()),  jobs)
	#for job in jobs:
		#print j.bin, "is finished in",
		#if not j.finished.isSet(): j.join()
		#print j.time(), "sec."


def inQueue(n, j = None):
	"""starts all jobs in the list subsequently. as executing queue get
	empty jack then program puts new job into exec queue."""
	if j is None:
		j = Job('CPU', 'python', '-c', "import workload; workload.factor(2345)")
	jobs = []
	if n > 1000 or n < 1: n = 1000
	for n in range(0,n):
		jobs.append( j.clone() )

	max = 4
	running = []
	finished = []

	while len(jobs) > 0:
		while threading.activeCount() - 1 < max : #len(running) < max:
			if len(jobs) > 0:
				jobs.pop(0).start()
			else: break
		#print len(jobs)
	while threading.activeCount() > 1: pass


if __name__ == '__main__':
	cpu = Job('CPU', 'python', '-c', "import workload; workload.factor(2345)")
	read = Job('update', 'python', '-c', "import workload; workload.useDiskUpdate()")
	update = Job('read', 'python', '-c', "import workload; workload.useDiskRead()")
	#inBurst(100)
	inQueue(1000, cpu)

	Stats.instance().p()
	Stats.instance().output()

