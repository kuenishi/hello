class Singleton_(object): 
	'''singleton pattern implementation
	reffered from thread  
	http://www.python.jp/pipermail/python-ml-jp/2002-November/002075.html
	, thanks!  usage: inherit Singleton and call Inheretee.instance()'''
	instance_ = None
	
	def __new__(cls):
		if cls.instance_ is None:
			cls.stat = []
			cls.instance_ = object.__new__(cls)
		return cls.instance_
	def instance(cls):
		if not cls.instance_:
			cls.instance_ = Hoge()
		return cls.instance_
	instance = classmethod(instance)

class Hoge(Singleton_):
  array = []
  def append(self, v):
    self.array.append(v)
  def p(self):
    for v in self.array:
      print v

Hoge.instance().append('aaaa')
Hoge.instance().append('bbbb')
Hoge.instance().p()
