#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# $Id: workload.py 240 2007-07-31 07:58:01Z kuenishi $
#
#目的：いろんな負荷routineをつくる
#-disk accessが多い負荷routineをつくる
#-CPU usageが多い負荷routineをつくる
#-network usage(lo?)が多い負荷routineをつくる
#-それらを変化させるような何かをつくる
#...実はsubprocess使わなくてもThreadingだけで十分かも？！ →No

import unittest


def smallest_factor(n):
	for i in range(2, n):
		if n % i is 0:return i
	return n
	
def factor(n):
	if n < 1: return None
	import sys
	N = n
	factors = []
	while( N is not 1 ):
		p = smallest_factor( N )
		factors.append(p)
		N = N/p
		sys.stdout.write( str(p)+", " )
		sys.stdout.flush()
	print factors
	return factors

def useCPU():
	return factor(10003034)

import sqlite
def useDiskUpdate():
	connection = sqlite.connect('test.db')
	c = connection.cursor()
	#try:
	import time
	c.execute('UPDATE testoo SET name = "the time is:%s"' % time.ctime())
	connection.commit()
	print "all rows updated at", time.ctime(), "."
	return True
	#except: return False

def useDiskRead():
	try:
		connection = sqlite.connect('test.db')
		cursor = connection.cursor()

		cursor.execute('SELECT * FROM testoo WHERE id < 10')
		result = cursor.fetchall()
		print len(result), " rows selected."
		for r in result: pass
		return True
	except: return False
#	finally: cursor.close()

class RoutineTests(unittest.TestCase):
	def testUpdate(self):
		self.assertTrue( useDiskUpdate() )
	def testRead(self):
		self.assertTrue( useDiskRead() )
	def testFactor0(self):
		self.assertFalse( factor(0) )
	def testFactor(self):
		self.assertEqual( [2,2,2,3], factor(24) )
	def testCPU(self):
		self.assertTrue( useCPU() )

if __name__ == '__main__':
	suite = unittest.makeSuite(RoutineTests)
	unittest.TextTestRunner(verbosity=2).run(suite)
