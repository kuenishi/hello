<?xml version="1.0" encoding="Shift_JIS"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:output method="html" encoding="Shift_JIS"/>

  <xsl:template match="/">
    <xsl:apply-templates/>
  </xsl:template>

  <xsl:template match="tropy">
    <html>
	<head>  
<link type='text/css' rel='Stylesheet' href='default.css' />
	</head>
    <body>
    <h1>keyword list</h1>
    <hr/>
    <dl>
    <!-- xsl:sort select="./key" data-type="string" order="ascending"/ -->
    <xsl:apply-templates/>
    </dl>
    <hr/>
    <address>kota.uenishi@lab.ntt.co.jp</address>
  </body>
    </html>
  </xsl:template>

  <xsl:template match="//item">
        <dt><xsl:value-of select="key"/></dt>
	<dd><xsl:value-of select="value"/></dd>
  </xsl:template>
</xsl:stylesheet>