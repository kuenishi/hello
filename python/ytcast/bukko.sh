#!/bin/sh

##@author kuenishi
##プロトタイプ。引数に動画IDをとる。
##TODO: DSTDIRを外部から設定できるようにすると吉。
TMPDIR=./
DSTDIR=../

YTURL=http://www.youtube.com/watch?v=$1

TMPFILE=${TMPDIR}"$1.flv"
TARGET=${DSTDIR}"$1.m4v"

if ! [ -f ${TMPFILE} ];  then 
    echo "plagging "${TMPFILE}
    ./youtube-dl ${YTURL} -o${TMPFILE}
else
    echo ${TMPFILE} already exists.
fi

if ! [ -f ${TARGET} ]; then
    echo "converting..."
    ffmpeg -y  -i ${TMPFILE} -title "test" \
	-an -vcodec mpeg4 ${TARGET}
else
    echo "${TARGET} already exists."
fi