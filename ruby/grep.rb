#!/usr/bin/env ruby

#simplest grep in ruby
# $Id: grep.rb 402 2007-09-25 18:13:02Z kuenishi $

$KCODE="UTF-8"

result = Array.new
regexp = ARGV[0]
File.open( ARGV[1] ).each do | line |
  result << line  if  /#{regexp}/ =~ line
end
result.each do | match |
  print match
end

