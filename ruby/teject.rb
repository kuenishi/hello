#!/usr/bin/env ruby

# username must be able to exec <eject> command
# %chown username teject.rb
# #crontab -e
# + 0 * * * *  username teject.rb

t = Time.now.hour
t %= 12
print "it's ", t, " o'clock.\n"
t *= 2
while 0 < t
	`eject -T`
	t-=1
end
