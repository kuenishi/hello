#!/usr/bin/env ruby

#require 'rubygems'
require 'memcache'
 
cache = MemCache.new('localhost:11211')
regis = true
if regis then
  cache['number'] = 1234
  cache['string'] = 'foo'
  cache['array'] = [1, 2, 3]
  cache['hash'] = { 1 => 'foo', 2 => 'bar' }
end
start = Time.now
(1..200000).each do |i|
  key = "hoge#{i}===#{Time.now}"
  cache[key] = i
  cache['number'] #=> 1234
  cache['string'] #=> "foo"
  cache['array'] #=> [1, 2, 3]
  cache['hash'] #=> {1=>"foo", 2=>"bar"}
end
fini  = Time.now

p (fini - start)

