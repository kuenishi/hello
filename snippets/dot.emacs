(define-key global-map "\C-o" 'dabbrev-expand)

(setq user-full-name "UENISHI Kota")
(setq user-mail-address "kuenishi@gmail.com")

;;fullscreen,color,alpha
(if window-system
    (progn ()
           (toggle-scroll-bar nil)
           (tool-bar-mode)
           (setq mac-autohide-menubar-on-maximize t)
					;       (mac-toggle-max-window)
           (setq default-frame-alist
                 (append (list
                          '(height . 43)
			  '(width . 120)
                          )))
	   
;           (require 'color-theme)
;           (color-theme-initialize)
;           (color-theme-dark-laptop)
					;http://homepage.mac.com/matsuan_tamachan/software/ColorTheme.html#3
	   ;;(color-theme-matrix) 
	   
           (set-frame-parameter nil 'alpha 95 )
           )
  )

; inhibit startup-splash screen (where a gnu appears)
(setq inhibit-splash-screen t)

(require 'rst)

;(require 'xcscope)
;(global-set-key "\C-csI" 'cscope-index-files) ;でインデックスファイルを作成したら，後は，自由に検索できます．
;(global-set-key "\C-css" 'cscope-find-this-symbol)
;(global-set-key "\C-csd" 'cscope-find-global-definition)
;(global-set-key "\C-cse" 'cscope-find-egrep-pattern)

;; distel; distributed erlang debugging environment
;(add-to-list 'load-path "/opt/local/share/distel/elisp")
;(require 'distel)
;(distel-setup)

;; erlang mode for CarbonEmacs
;
;(setq load-path (cons "/opt/local/lib/erlang/lib/tools-2.6.6.5/emacs/" load-path))
;(setq erlang-root-dir "/opt/local/lib/erlang")
;(setq erlang-path "/opt/local/bin" exec-path)
;(require 'erlang-start)
;(require 'erlang-flymake)

;; http://www.02.246.ne.jp/~torutk/cxx/emacs/mode_extension.html
;; EmacsにおけるC++ファイル拡張子とEmacs
;; C++
; ヘッダファイル(.h)をc++モードで開く
(setq auto-mode-alist
      (append '(("\\.h$" . c++-mode))
              auto-mode-alist))
;;(require 'autoinsert)
;; テンプレート格納用ディレクトリ
;;(setq auto-insert-directory "~/.emacs.d/insert/")
;; ファイル拡張子とテンプレートの対応
;;(setq auto-insert-alist
;;      (append '(
;;		("\\.cpp$" . ["template.cpp" my-template])
;;		("\\.h$" . ["template.h" my-template])
;;		) auto-insert-alist))
;;(add-hook 'find-file-hooks 'auto-insert)

;(require 'navi2ch)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; append-tuareg.el - Tuareg quick installation: Append this file to .emacs.
;(setq auto-mode-alist (cons '("\\.ml\\w?" . tuareg-mode) auto-mode-alist))
;(autoload 'tuareg-mode "tuareg" "Major mode for editing Caml code" t)
;(autoload 'camldebug "camldebug" "Run the Caml debugger" t)

(if (and (boundp 'window-system) window-system)
    (when (string-match "XEmacs" emacs-version)
       	(if (not (and (boundp 'mule-x-win-initted) mule-x-win-initted))
            (require 'sym-lock))
       	(require 'font-lock)))

;; clojure-mode
;(add-to-list 'load-path "/usr/local/src/clojure-mode")
;(require 'clojure-mode)
;; slime
;(eval-after-load "slime" '(progn (slime-setup '(slime-repl))))
;
;(add-to-list 'load-path "/usr/local/src/slime")
;(require 'slime)
;(slime-setup)


(require 'flymake)

(defun flymake-cc-init ()
  (let* ((temp-file   (flymake-init-create-temp-buffer-copy
                       'flymake-create-temp-inplace))
         (local-file  (file-relative-name
                       temp-file
                       (file-name-directory buffer-file-name))))
    (list "g++" (list "-Wall" "-Wextra" "-fsyntax-only" local-file))))

(push '("\\.cpp$" flymake-cc-init) flymake-allowed-file-name-masks)

(add-hook 'c++-mode-hook
          '(lambda ()
             (flymake-mode t)))
