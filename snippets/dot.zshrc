# Lines configured by zsh-newuser-install
umask 022

HISTFILE=~/.histfile
HISTSIZE=1000
SAVEHIST=1000
setopt extended_history
setopt share_history
setopt auto_remove_slash
setopt always_last_prompt
setopt auto_pushd
setopt list_packed

export LANG=ja_JP.UTF-8
export EDITOR=emacs
export OUTPUT_CHARSET=UTF-8
bindkey -e
# End of lines configured by zsh-newuser-install
# The following lines were added by compinstall
zstyle :compinstall filename '/home/kuenishi/.zshrc'

autoload -Uz compinit
compinit
# End of lines added by compinstall


local GREEN=$'%{\e[1;32m%}'
local BLUE=$'%{\e[1;34m%}'
local DEFAULT=$'%{\e[1;m%}'

autoload -U colors; colors

function rprompt-git-current-branch {
        local name st color

        if [[ "$PWD" =~ '/\.git(/.*)?$' ]]; then
                return
        fi
        name=$(basename "`git symbolic-ref HEAD 2> /dev/null`")
        if [[ -z $name ]]; then
                return
        fi
        st=`git status 2> /dev/null`
        if [[ -n `echo "$st" | grep "^nothing to"` ]]; then
                color=${fg[green]}
        elif [[ -n `echo "$st" | grep "^nothing added"` ]]; then
                color=${fg[yellow]}
        elif [[ -n `echo "$st" | grep "^# Untracked"` ]]; then
                color=${fg_bold[red]}
        else
                color=${fg[red]}
        fi

        # %{...%} は囲まれた文字列がエスケープシーケンスであることを明示する
        # これをしないと右プロンプトの位置がずれる
        echo "%{$color%}$name%{$reset_color%} "
}

# プロンプトが表示されるたびにプロンプト文字列を評価、置換する
setopt prompt_subst

RPROMPT='[`rprompt-git-current-branch`%~]'

export HOSTNAME=`hostname`
export PROMPT=$BLUE'${USER}@${HOSTNAME}> %(!.#.$) '$DEFAULT
#export RPROMPT=$GREEN'[${USER}@${HOSTNAME}:%~]'$DEFAULT
setopt PROMPT_SUBST
setopt NO_BEEP
setopt AUTOLIST

#http://0xcc.net/blog/archives/cat_7.html
#case "$TERM" in
#    xterm*|kterm*|rxvt*)
#    PROMPT=$(print "%B%{\e[34m%}%m:%(5~,%-2~/.../%2~,%~)%{\e[33m%}%# %b")
#    PROMPT=$(print "%{\e]2;%n@%m: %~\7%}$PROMPT") # title bar
#    ;;
#    *)
#    PROMPT='%m:%c%# '
#    ;;
#esac

export PATH=$PATH:~/.cabal/bin:/usr/local/depot_tools
export LD_LIBRARY_PATH=/usr/local/lib
export CXX='ccache g++'
export CC='ccache gcc'
