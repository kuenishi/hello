
type json_value = String of string
                  | Number of num
                  | Object of (string * json_value) list
                  | Array  of json_value list
                  | Bool of bool
                  | Null
and num = Int of int
          | Float of float
(* and so on? *)


let rec pp_json = function
  | String(s)-> Printf.sprintf "\"%s\"" s;
  | Number(Int(i)) -> Printf.sprintf "%d" i;
  | Number(Float(v)) -> Printf.sprintf "%f" v;
  | Object(o) -> "{ " ^ (pp_object o) ^ " }";
  | Array(a)  ->
    "[" ^ (String.concat ", " (List.map pp_json a)) ^ "]";
  | Bool(true) -> "true"
  | Bool(false)-> "false";
  | Null -> "null";
and pp_object l =
  String.concat ", " (List.map 
    (fun (k,v) -> Printf.sprintf "%s : %s" k (pp_json v)) l)
;;
