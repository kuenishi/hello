
%{

  let _ = Parsing.set_trace false;;
  let debugprint = ref true;;
  
  let print str = if !debugprint then print_endline str;;
  
  let parse_error s = print ("parse_error->"^s);;

%}

%token TRUE FALSE NULL
%token LBRACE RBRACE COLON COMMA
%token LBRACE2 RBRACE2 EOF
%token <int> INT

%token <string> QUOTE

%start input
%type <Json.json_value list> input

%%

input:
  | exp { $1 }
;

exp:
  | value EOF { [$1] }
  | value exp { $1::$2 }
;
value:
  | QUOTE { Json.String($1) }
  | INT   { Json.Number(Json.Int($1)) }
  | LBRACE pairs RBRACE    { Json.Object($2) }
  | LBRACE2 values RBRACE2 { Json.Array($2)  }
  | TRUE  { Json.Bool(true) }
  | FALSE { Json.Bool(false)}
  | NULL  { Json.Null }
;
pairs:
  | QUOTE COLON value { [($1, $3)] }
  | QUOTE COLON value COMMA pairs
      { ($1, $3)::$5 }
;
values:
  | value { [$1] }
  | value COMMA values { $1::$3 }
;
