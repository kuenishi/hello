
type columns   = All_col | Columns of string list
type condition = And of condition * condition
                 | Or of condition * condition
                 | Not of condition
                 | Gt_f of string * float
                 | Gt_i of string * int
                 | Lt_f of string * float
                 | Lt_i of string * int
                 | Equal_i of string * int
                 | Equal_s of string * string
                 | Regexp  of string * string
  
type query = Select of columns * string * condition list
             (* columns, table, wheres *)

exception Not_implemented_regexp

let pp_cols = function
  | All_col -> "*";
  | Columns(cols) -> (String.concat "," cols);;

let rec pp_cond = function
  | And(c0, c1) -> (pp_cond c0) ^ " and " ^ (pp_cond c1);
  | Or(c0, c1)  -> (pp_cond c0) ^ " or " ^ (pp_cond c1);
  | Not(c)      -> "(NOT "^(pp_cond c)^")";
  | Gt_f(s, v)  -> Printf.sprintf "(%s < %f)" s v;
  | Gt_i(s, i)  -> Printf.sprintf "(%s < %d)" s i;
  | Lt_f(s, v)  -> Printf.sprintf "(%s > %f)" s v;
  | Lt_i(s, i)  -> Printf.sprintf "(%s > %d)" s i;
  | Equal_i(s, i) -> Printf.sprintf "(%s == %d)" s i;
  | Equal_s(s0,s1)-> Printf.sprintf "(%s == '%s')" s0 s1;
  | Regexp(s,r) -> Printf.sprintf "(%s ~ %s)" s r;;

let rec pp_where = function
  | [] -> "";
  | hd::tl -> (pp_cond hd)^" "^(pp_where tl);;

let pp_query = function
  | Select(columns, table, where) ->
    Printf.printf "select: %s from %s where %s\n" (pp_cols columns) table (pp_where where);;

let get_table_name = function
  | Select(_, table, _) -> table;;
