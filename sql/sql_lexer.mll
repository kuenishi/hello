
{

open Sql_parser
let debugprint = ref true;;
let print str =  if !debugprint then print_endline str;;

let quote_buf = String.create 8192;;
let quote_ptr = ref 0;;
let in_quote = ref false;;

}

let digit = ['0'-'9']*
let literal = ['a'-'z']['A'-'Z' 'a'-'z' '_' '0'-'9']*

rule token = parse
  | "select" { SELECT }
  | "SELECT" { SELECT }
  | "from"   { FROM }
  | "FROM"   { FROM }
  | "where"  { WHERE }
  | "WHERE"  { WHERE }
  | ","      { COMMA }
  | "*"      { ASTERISK }
  | "=="     { EQUAL }
  | ';'      { SEMICOLON }
  | "'"      {
    in_quote := true;
    quote_ptr := 0;
    quote lexbuf;
    in_quote := false;
    QUOTE (String.sub quote_buf 0 (!quote_ptr))
  }
  | digit as s   { INT(int_of_string s) }
  | literal as s { LITERAL(s) }
(*  | "\n"         { print "f"; token lexbuf } *)
  | _  { token lexbuf }

and quote = parse
  | "'"  { () }
  | _ as c {
    String.set quote_buf (!quote_ptr) c;
    quote_ptr := (!quote_ptr) + 1;
    quote lexbuf
  }

{}
