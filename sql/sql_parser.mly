
%{

  let _ = Parsing.set_trace false;;
  let debugprint = ref true;;
  
  let print str = if !debugprint then print_endline str;;
  let parse_error s = print ("parse_error->"^s);;

%}

%token SELECT
%token INSERT
%token DELETE
%token UPDATE
%token INTO
%token VALUES
%token SET
%token FROM
%token WHERE
%token LIMIT
%token DOT
%token LBRACE RBRACE
%token COMMA
%token SEMICOLON
%token ASTERISK
%token GT LT EQ NEQ NOT
%token EQUAL

%token <int> INT

%token <string> LITERAL
%token <string> QUOTE

%start input
%type <Sql.query> input

%%

input: | exp { $1 };

exp:
  | SELECT ASTERISK FROM LITERAL SEMICOLON { Sql.Select(Sql.All_col, $4, []) }
  | SELECT columns  FROM LITERAL SEMICOLON { Sql.Select(Sql.Columns($2), $4, []) }

  | SELECT ASTERISK FROM LITERAL where_clause SEMICOLON
      { Sql.Select(Sql.All_col, $4, $5) }
  | SELECT columns  FROM LITERAL where_clause SEMICOLON
      { Sql.Select(Sql.Columns($2), $4, $5) }
;

columns:
  | LITERAL  { [$1] }
  | LITERAL COMMA columns { $1::$3 }

where_clause:
  | WHERE LITERAL EQUAL QUOTE { [ Sql.Equal_s($2, $4)] }
